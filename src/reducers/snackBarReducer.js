const initialState = {
        status: false,//open //close
        message: ""  //show display message
};


export default function snackBarReducer(state=initialState, action){

    switch(action.type.toLowerCase()){/// DURUMLARI KÜCÜK HARFe CEVİRİYOR
    /// DURUMLARI KÜCÜK HARFLER İLE YAZALIM LÜTFEN
        case 'open': 
            return {
                ...state,
                status: true,
                message:action.payload
            }; 
        case 'close':
            return {
                ...state,
                status: false,
                message:""
            }; 
        default:
            return state; 
    }
}