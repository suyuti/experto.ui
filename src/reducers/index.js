import {combineReducers} from 'redux';
import snackBarReducer from './snackBarReducer';

const Reducers = combineReducers({
    snackBarReducer: snackBarReducer
});

export default Reducers;