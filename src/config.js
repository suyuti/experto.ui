
export const API_URL = process.env.REACT_APP_API_URL


export const PermissionList = {
    "MusteriDelete": "Musteri:Delete",
    "ProjeCreate": "Proje:Create",
    "ProjeRead": "Proje:Read",
    "ProjeUpdate": "Proje:Update"
}
