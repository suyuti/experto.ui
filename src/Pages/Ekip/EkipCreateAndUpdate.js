import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views"; 
import { Paper,Button } from "@material-ui/core";  
import { ekipService } from "../../Services";
import EkipFormu from '../../Components/Ekip/EkipFormu'   
import Snackbar from '../../Components/Snackbar';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {SnackbarAction} from '../../actions/snackbarAction';

var self;
class CreateAndUpdate extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            buttonEnable: true,
            message: "",
            value: 0,
            EkipFormu: {
            }
        };

        this.MusteriId=this.props.match.params.mid;
        this.paramsId = this.props.match.params.id;
        if (this.paramsId) {
            ekipService.getById(this.paramsId).then(data => {
                this.setState({ EkipFormu: data.data[0] })
            });
        }
        else {
          if (this.MusteriId) {
            this.state.EkipFormu.MusteriId = this.MusteriId;
          }
      }
    }

    handleTabChange(e, v) {
        self.setState({ value: v });
    }

    handleSwipeViewChange(i) {
        self.setState({ value: i });
    }

    handleChange = (input) => e => {
        let inputValue = e.target.value;
        this.setState(() => ({
            EkipFormu: {
                ...this.state.EkipFormu,
                [input]: inputValue
            }
        }));
    };

    validateInput = () => {
      if (!this.state.EkipFormu.MusteriId || this.state.EkipFormu.MusteriId == " ") {
        return this.props.SnackbarAction('open', "Lütfen Müşteri seçiniz.");
      }
      if (!this.state.EkipFormu.EkipAdi || this.state.EkipFormu.EkipAdi == " ") {
        return this.props.SnackbarAction('open', "Lütfen EkipAdi alanını giriniz");
      }
    }

    createButton = (e) => {
        if(this.validateInput()) return;
        this.setState({buttonEnable:false}); 
        // e.preventDefault();
        ekipService.create(this.state.EkipFormu).then(data => {
            if (data.messages != "") {
              this.props.SnackbarAction('open', data.messages);
            }
            if (data.status === 200) {
                this.setState({buttonEnable:true});
                this.props.history.goBack();
            }
        });
    }

    updateButton = (e) => {
        if(this.validateInput()) return;
        this.setState({ buttonEnable: false });
        ekipService.update(this.state.EkipFormu).then(data => {
            if (data.messages != "") {
              this.props.SnackbarAction('open', data.messages);
            }
            if (data.status === 200) {
                this.setState({ buttonEnable: true });
                this.props.history.goBack();
            }
        });
    }

    render() {
        const { classes, ...props } = this.props;
        let button;
        if (this.state.buttonEnable) {
            if (this.paramsId) {
                button = <Button
                    variant="contained"
                    color="default"
                    onClick={this.updateButton}
                    style={styles.save_button}>GÜNCELLE</Button>
            } else {
                button = <Button
                    variant="contained"
                    color="primary"
                    onClick={this.createButton}
                    style={styles.save_button}>KAYDET</Button>
            }

        } else {
            button = <Button
                variant="contained"
                color="primary" 
                style={styles.save_button}>...</Button>
        }

        if (this.state.result) {            
        }
        
        return (
            <div> 
                <Paper> 
                                    <Tabs
                                        value={this.state.value}
                                        onChange={this.handleTabChange}
                                        indicatorColor="primary"
                                        textColor="primary"
                                        variant="fullWidth"
                                    >
                                        <Tab label='Ekip Bilgileri' />
                                    </Tabs>
                                    <SwipeableViews
                                        style={{margin:"15px"}}
                                        axis="x"
                                        index={this.state.value}
                                        onChangeIndex={this.handleSwipeViewChange}>
                                        <div> 
                                            <EkipFormu
                                                handleChange={this.handleChange}
                                                data={this.state.EkipFormu} />
                                        </div>
                                    </SwipeableViews>
                                    <br />
                                    {button} 
                </Paper>
            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '20px',
        left: '20px'
    }
};

function mapStateToProps(state){
  return {
      snackBarReducer: state.snackBarReducer
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
      SnackbarAction: SnackbarAction
  },dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAndUpdate);
