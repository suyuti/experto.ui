import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add"; 
import { Link } from "react-router-dom";
import { ekipService, musteriService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';
import CircularProgress from '@material-ui/core/CircularProgress';

const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));

class EkipListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            RowStatus: 1,//1 normal liste 2 ekleme 3 düzenleme
            ekipReady: false
        };
    }

    componentDidMount() { 
      this.fetchData();
   }
    fetchData = () => {
      if (this.props.musteriId) {
        musteriService.getEkipler(this.props.musteriId)
          .then(result => {
            if (result.status === 200) {
              if (result.data != null) {
                this.setState({ rows: result.data })
              }
            }
            this.setState({...this.state, ekipReady: true})
          });
      } else {
        ekipService.getAll().then(result => this.setState({ rows: result.data, ekipReady: true }));
      }
    }

    updateRow = (Row) => {
        this.props.history.push('/ekip/update/' + Row.id);
    }

    removeRow = (Row) => {
      // alert('!')
      let confirmMessage = window.confirm(`"${Row.EkipAdi}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        // this.setState({ buttonEnable: false });
        //   ekipService.remove(Row).then(result => {
        //       if (result.status === 200) {
        //           ekipService.getAll().then(result => this.setState({ rows: result.data }));  
        //       }
        //   });
      }
      return
    }
    render() {
        const { classes } = this.props;
        if (this.state.ekipReady === false) {
          return (<div style={{ textAlign: "center" }}><CircularProgress /> </div>)
      }
        var EkipCreate = this.props.musteriId ? "/musteri/" + this.props.musteriId + "/ekip/create" : `/ekip/create`;

        var createbutton = <Fab
          color="primary"
          aria-label="add"
          size='small'
          style={{ marginLeft: 8 }}
          component={AdapterLink}
          to={EkipCreate}
        >
          <AddIcon />
        </Fab>
        return (
            <div>
                <MaterialTable
                  title={
                    <div>{`Ekip  Listesi (${this.state.rows.length}) `}
                      {checkPermission([PermissionData.postEkip]) ? createbutton : ""}
                    </div>
                  }
                    columns={[
                        {title: 'id',field: 'id'},
                        { title: 'ekip Adi', field: 'EkipAdi' },
                        { title: 'Musteri Id', field: 'MusteriId' },
                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize:10,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions: [10, 20, 30]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Ekip Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Ekip sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })
                    ]}
                />
                {checkPermission([PermissionData.postEkip]) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/ekip/create"
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                }
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};


export default EkipListAndSearch;
