import React, { Component } from "react";
import { convertTimestampToDate, convertDateToTimestamp } from '../../command'
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views";
import { Paper, Button } from "@material-ui/core";
import CircularProgress from '@material-ui/core/CircularProgress';
import { isPaketiService, projeService, personelProfilService } from "../../Services";
import IsPaketiFormu from '../../Components/isPaketi/isPaketiFormu'
import Snackbar from '../../Components/Snackbar'




var self;
class CreateAndUpdate extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            buttonEnable: true,
            message: "sadasd",
            value: 0,
            IsPaketiFormu: {
                CalisacakProfilListesi: []
            },
            proje: {},
            personelProfil: []
        };
        this.projeId = props.match.params.pid;
        this.paramsId = this.props.match.params.id;

        if (this.projeId) {
            projeService.getById(this.projeId).then(result => {
                this.setState({ proje: result.data[0] })

            });
        }
        if (this.paramsId) {
            isPaketiService.getById(this.paramsId).then(result => {
                this.setState({ IsPaketiFormu: result.data[0] });
                
            });
            
        }
        personelProfilService.getAll().then(result => {
            this.setState({ personelProfil: result.data })
        });


    }



    handleTabChange(e, v) {
        self.setState({ value: v });
        console.log("handleTabChange");
    }
    handleSwipeViewChange(i) {
        self.setState({ value: i });
        console.log("handleChangeIndex");
    }

    handleChange = (input) => e => {
        //console.log(e.target.value, input);
        let inputValue = e.target.value;
        if (e.target.type == "date") {
            this.setState(() => ({
                IsPaketiFormu: {
                    ...this.state.IsPaketiFormu,
                    [input]: convertDateToTimestamp(inputValue)
                }
            }));
        } else {
            this.setState(() => ({
                IsPaketiFormu: {
                    ...this.state.IsPaketiFormu,
                    [input]: inputValue
                }
            }));
        }
    };

    createButton = (e) => {
        this.setState({ buttonEnable: false });
        //timestimp cevirme
        this.state.IsPaketiFormu.ProjeId = this.projeId;

        isPaketiService.create(this.state.IsPaketiFormu).then(data => {
            console.log(data.status)
            if (data.status = 200) {
                this.setState({ buttonEnable: true });
            }
        });

    }


    updateButton = (e) => {
        console.log('update button');
        this.setState({ buttonEnable: false });
        //timestimp cevirme
        this.state.IsPaketiFormu.ProjeId = this.projeId;

        isPaketiService.update(this.state.IsPaketiFormu).then(data => {
            console.log(data.status)
            if (data.status = 200) {
                this.setState({ buttonEnable: true });
            }
        });
    }

    render() {
        const { classes, ...props } = this.props;
        let button;
        if (this.state.buttonEnable) {
            if (this.paramsId) {
                button = <Button
                    variant="contained"
                    color="default"
                    onClick={this.updateButton}
                    style={styles.save_button}>UPDATE  </Button>
            } else {
                button = <Button
                    variant="contained"
                    color="primary"
                    onClick={this.createButton}
                    style={styles.save_button}>KAYDET </Button>
            }
        } else {
            button = <Button
                variant="contained"
                color="primary"
                style={styles.save_button}>...</Button>

        }
        return (
            <div>
                <Paper>
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleTabChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="fullWidth"
                    >
                        <Tab label='IsPaketiFormu Bilgileri' />
                    </Tabs>
                    <SwipeableViews
                        style={{ margin: "15px" }}
                        axis="x"
                        index={this.state.value}
                        onChangeIndex={this.handleSwipeViewChange}>
                        <div>

                            <IsPaketiFormu
                                handleChange={this.handleChange}
                                data={this.state.IsPaketiFormu}
                                proje={this.state.proje}
                                personelProfil={this.state.personelProfil}
                            />

                        </div>
                    </SwipeableViews>
                    <br />
                    {button}
                </Paper>
            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '20px',
        left: '20px'
    }
};

export default CreateAndUpdate;
