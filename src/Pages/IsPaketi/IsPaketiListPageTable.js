import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import CircularProgress from '@material-ui/core/CircularProgress'; 
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import { authenticationService, projeService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';


const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));


class PersonelListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //role: null,
            rows: [],
            proje:[],
            gecerliMusteri: [],
            RowStatus: 1//1 normal liste 2 ekleme 3 düzenleme
        };
        //console.log(this.state.rows);
        //console.log(can("Musteri", "Delete"));
        this.projeId= props.match.params.pid;

    }

      
    componentDidMount() {
        if( this.projeId){ 
            projeService.getAllIsPaketi(this.projeId).then(result => {
                if (result.status = 200) {
                    this.setState({ loaderShow: false });
                    this.setState({ rows: result.data });
                }
            });
        }  
    }

    updateRow = (Row) => {
       // alert("updateRow  =" + Row.Code);
         var link =`/proje/${this.projeId}/ispaketi/update/${Row.Code}` 
        this.props.history.push(link);
        //alert("You saved " + rowData.id)
    }
 

    removeRow = (Row) => {
      // console.log('remove  button');
      // alert("deleteRow  =" + Row.Code);
      console.log(Row);
      let confirmMessage = window.confirm(`"${Row.FirmaAdi}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        this.setState({ buttonEnable: false });
        projeService.removeIsPaketi(Row).then(result => {
          console.log(result.status)
          if (result.status = 200) {
            projeService.getAllIsPaketi(this.projeId).then(result => this.setState({ rows: result.data }));
          }
        });
      }
    }

 




    render() {
        //const { classes } = this.props;

        if (!this.state.rows.length) {
            return (
                <div style={{textAlign:"center"}}>
                   <CircularProgress   /> 
                </div>
            )
        }
        return (
            <div>
                {(this.state.gecerliMusteri.FirmaAdi) ?
                    <div style={styles.musteriBilgisi}>
                         {this.state.gecerliMusteri.FirmaAdi}  
                    </div>
                    :""
                }

                <MaterialTable
                    title={"İş Paketi  Listesi (" + this.state.rows.length+")"}
                    columns={[
                        {
                            title: 'Code',
                            field: 'Code',
                            cellStyle: {
                                //backgroundColor: '#039be5',
                                //color: '#FFF'
                            },
                            headerStyle: {
                                //backgroundColor: '#039be5',
                            }
                        },
                        { title: 'Adi', field: 'Adi' },
                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'İş Paketi  Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'İş Paketi  sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })


                    ]}
                />
                {checkPermission([PermissionData.postIspaketleri]) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to={"/proje/"+ this.projeId +"/ispaketi/create"}
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                }
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};


export default withStyles(styles)(PersonelListAndSearch);
