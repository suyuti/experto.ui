import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Grid, Button} from '@material-ui/core'
import MaterialTable from 'material-table';
import Chart from 'react-google-charts';
import { projeService, isPaketiService, personelProfilService,musteriService } from '../../Services';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import IsPaketiForm from '../../Components/IsPaketleri/IsPaketiForm';
import IsPaketiError from '../../Components/IsPaketleri/IsPaketiError'; 
import IsPaketiGosterimTablosu from '../../Components/IsPaketleri/IsPaketiGosterimTablosu';

import DialogActions from '@material-ui/core/DialogActions';
import trLocale from "date-fns/locale/tr";
 

import {SnackbarAction} from '../../actions/snackbarAction';
import { datePickerDefaultProps } from '@material-ui/pickers/constants/prop-types';
import { red } from '@material-ui/core/colors';


const charFirstItem = [
    { type: 'string', id: 'Term' },
    { type: 'string', id: 'Name' },
    { type: 'date', id: 'Start' },
    { type: 'date', id: 'End' },
    ];
class IsPaketleriPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            dialogOpen: false,
            profiller: [],
            isPaketleri: [],
            isPaketiFormData: {
                ProjeId:this.props.projeId,
                ProjeAdi: this.props.projeAdi,
                IsPaketiAdi: "",
                Aciklama: "",
                ProfilId: null,
                BaslangicTarihi: new Date(),
                BitisTarihi: new Date()
            },  
            data: [],
            OpenError: false,
            OpenErrorModelData: [],
            read:true
        } 

    }


    updateRow = (Row) => {
    }

    deleteRow = (Row) => {
      let confirmMessage = window.confirm(`"${Row.Adi}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        isPaketiService.remove(Row).then(result => {
          if (result.status === 200) {
            this.updateContent(Row);
          } else {
            this.props.SnackbarAction('open', result.messages);
          }
          if (result.data.hataliOlanlar !== undefined) {
            console.log(result.data.hataliOlanlar);
            this.setState({ OpenErrorModelData: result.data.hataliOlanlar })
            this.setState({ OpenError: true })
          }
        })

        //  isPaketiService.remove(Row).then(result => {if (result.status === 200) {this.updateContent()}})
      }
    }

    componentDidMount() {
        if (this.props.musteriId) {
            musteriService.getPersonelProfil(this.props.musteriId)
                .then(result => {
                    if (result.status === 200) {
                        if (result.data != null) {
                            this.setState({profiller:result.data});
                            this.updateContent()
                        }
                    }
                });
        }  

     //   personelProfilService.getAll().then(result => this.setState({profiller:result.data}))
        
    }

    updateContent=(Row)=> {
        //this.setState({isPaketiFormData:{ProjeAdi: this.props.projeAdi, ProjeId: this.props.projeId}})
        projeService.getIspaketleri(this.props.projeId).then(result => 
            {
            if (result.status === 200) {
                // var _data = [] 
                // result.data.map(r => {
                //     var row = []
                //     row.push('' + r.id)
                //     row.push(r.Adi)
                //     row.push(new Date(r.BaslangicTarihi))
                //     row.push(new Date(r.BitisTarihi))
                //     _data.push(row)
                // }) 
                // var d = []
                // d.push(charFirstItem)
                // //var tmp = charFirstItem; //this.state.data
                // var d = d.concat(_data)

                // this.setState({ 
                //   isPaketleri: this.state.isPaketleri.filter(isPaketi => {
                //     return isPaketi.id !== Row.id
                //   })
                // })
                this.setState({ isPaketleri: result.data});
            }
        }
        )

        this.setState({dialogOpen:false}) 
        
    }

    handleDialogClose=()=> {
        this.setState({dialogOpen:false});
    }

    handleDialogOpen=()=> {
        this.setState(() => ({
            isPaketiFormData: {
                ...this.state.isPaketiFormData, 
                    IsPaketiAdi: "",
                    Aciklama: "",
                    ProfilId: [],
                    BaslangicTarihi: new Date(),
                    BitisTarihi: new Date() ,
                   
            }
        }));  
        this.setState({dialogOpen:true});
    }

    handleChange = (input) => e => {
        var inputValue;
        if (input === 'BaslangicTarihi' || input === 'BitisTarihi') {
            inputValue = e;
        }
        else {
            inputValue = e.target.value;
        }
        this.setState(() => ({
            isPaketiFormData: {
                ...this.state.isPaketiFormData,
                [input]: inputValue
            }
        }));
    };

    handleErrorDialogClose =()=> {
        this.setState({OpenError:false});
    }

    handleIPEkle=()=> {
        
        if(this.state.isPaketiFormData.IsPaketiAdi=="" || !this.state.isPaketiFormData.IsPaketiAdi ){
            this.props.SnackbarAction('open',"IsPaketiAdi kontrol ediniz");  
            return;
        }

        if(this.state.isPaketiFormData.BaslangicTarihi  > this.state.isPaketiFormData.BitisTarihi ){
            this.props.SnackbarAction('open',"Tarihleri kontrol ediniz");  
            return;
        }
        if(this.state.isPaketiFormData.ProfilId == null){
            this.props.SnackbarAction('open',"Çalışacak Profili kontrol ediniz!");  
            return;
        }
        this.setState({read:false});
        isPaketiService.create(this.state.isPaketiFormData).then(result => {
            if (result.messages != "") { 
                this.props.SnackbarAction('open',result.messages);  
            }
            if (result.status === 200) { 
                this.updateContent();  
            }
            if ( result.data.hataliOlanlar !== undefined ) { 
                console.log( result.data.hataliOlanlar );
                this.setState({OpenErrorModelData:result.data.hataliOlanlar})
                this.setState({OpenError:true})  
            } 
            this.setState({read:true});
        })
    }
    _errorDateAlertCheck = () => {
        var BaslangicTarihi = new Date(this.state.isPaketiFormData.BaslangicTarihi);
        var BitisTarihi = new Date(this.state.isPaketiFormData.BitisTarihi);
        var ProjeBaslangicTarihi = new Date(this.props.ProjeForm.ProjeBaslangicTarihi);
        var ProjeBitisTarihi = new Date(this.props.ProjeForm.ProjeBitisTarihi);
        BaslangicTarihi.setDate(18);
        BitisTarihi.setDate(8);
        ProjeBaslangicTarihi.setDate(8);
        ProjeBitisTarihi.setDate(18);

        // if (BaslangicTarihi.setDate(1) > BitisTarihi.setDate(2)) {
        //     return (<div>İŞPAKETİ Başlangıc tarihi bitiş tarihinden Büyük olamaz. <br />  Lütfen kontrol ediniz.</div>)
        // }
        if (BaslangicTarihi >= BitisTarihi) {
            return (<div>İŞPAKETİ Başlangıc tarihi bitiş tarihinden Büyük veya Eşit olamaz. <br />  Lütfen kontrol ediniz.</div>)
        }
        if (BaslangicTarihi < ProjeBaslangicTarihi || BitisTarihi > ProjeBitisTarihi) {
            return (<div>İŞPAKETİ tarihi proje tarihleri içerisinde degildir.  <br />
                Lütfen  {(ProjeBaslangicTarihi.getMonth() + 1) + "." + ProjeBaslangicTarihi.getFullYear()} ve  {(ProjeBitisTarihi.getMonth() + 1) + "." + ProjeBitisTarihi.getFullYear()} tarihleri arasında seciniz.
    </div>)
        }


    }

    render() {
        return(
            <div>
                <Grid container direction='column' spacing={3}>
                    <Grid item>
                        <Button variant="contained" color="secondary" onClick={this.handleDialogOpen}>Ekle</Button>
                    </Grid>
                    <Grid item >
                        <MaterialTable 
                            title = "İş Paketleri"
                            columns = {[
                                {title:'Adi', field:'Adi'},
                                {title:'Başlangıç Tarihi', field:'BaslangicTarihi'},
                                {title:'Bitiş Tarihi', field:'BitisTarihi'}
                            ]}
                            data={this.state.isPaketleri}
                            options={{
                                search:false,
                                showTitle:false,
                                toolbar:false,
                                paging:false,
                                actionsColumnIndex: -1
                            }}
                            actions={[
                                 
                                rowData => ({
                                    icon: 'delete',
                                    tooltip: 'İş Paketini Sil',
                                    onClick: (event, rowData) => { this.deleteRow(rowData) }
                                })
                            ]}
                                />
                    </Grid>
                    <Grid item >
                        <div> 
                        { (this.state.isPaketleri.length>0 && this.props.ProjeForm) &&<IsPaketiGosterimTablosu isPaketleri={this.state.isPaketleri}  proje={this.props.ProjeForm}/> }

                       {/*<Chart
                            chartType="Timeline"
                            height={'500px'}
                            loader={<div>Loading Chart</div>}
                            data= {this.state.data}
                            rootProps={{ 'data-testid': '2' }}
                            />*/} 
                        </div>
                    </Grid>
                </Grid>
            
                <Dialog open={this.state.dialogOpen} onClose={this.handleDialogClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Yeni Is Paketi</DialogTitle>
                    <DialogContent>
                        <IsPaketiForm 
                            data={this.state.isPaketiFormData}     
                            handleChange={this.handleChange} 
                            profiller={this.state.profiller} 
                        ></IsPaketiForm>
                    </DialogContent> 

{this._errorDateAlertCheck() && 
                    <DialogContent>
                        <div className="alert">
                           {this._errorDateAlertCheck() }
                        </div> 
                    </DialogContent>
}

                    <DialogActions >
                        <Button
                            variant="contained"
                            color="primary"
                            style={{ background: "#dc3545" }}
                            onClick={this.handleDialogClose}>Iptal</Button>
                       { this.state.read==true &&
                         <Button
                            variant="contained"
                            color="primary"
                            style={{ background: "#28a745" }}
                            onClick={this.handleIPEkle}>Tamam</Button>  
                       } 
                    </DialogActions>
                </Dialog>


                <Dialog open={this.state.OpenError}  aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">PERSONEL ADAMAY ATAMA HATASI</DialogTitle>
                    <DialogContent>

                        <IsPaketiError data={this.state.OpenErrorModelData}></IsPaketiError>
                            
                                Belirtilen Personellere Adamay ataması yapılamıyor. Lütfen iş paketini degiştiriniz
                             
                    </DialogContent>
                    <DialogActions>
                        <Button color="secondary" onClick={this.handleErrorDialogClose}>Kapat</Button>
                        {/*<Button color="primary" onClick={this.handleIPEkle}>Kabul Et</Button> */}
                    </DialogActions>
                </Dialog>

            </div>)
    }
}
 

function mapStateToProps(state){
    return {
        snackBarReducer: state.snackBarReducer
    }
} 
function mapDispatchToProps(dispatch){
    return bindActionCreators({
        SnackbarAction: SnackbarAction
    },dispatch)
} 
export default connect(mapStateToProps, mapDispatchToProps)(IsPaketleriPage);
