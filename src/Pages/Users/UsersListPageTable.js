import React from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {SnackbarAction} from '../../actions/snackbarAction';
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { Link } from "react-router-dom";
import {  userService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';
import {CircularProgress} from '@material-ui/core'; 


const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));


class PersonelListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            RowStatus: 1,//1 normal liste 2 ekleme 3 düzenleme
            Ready:false,
        };
    }

    componentDidMount() {
        userService.getAll().then(result => this.setState({ rows: result.data,Ready:true }));
    }

    updateRow = (Row) => { 
        this.props.history.push('/users/update/' + Row.id);
    }

    permissionRow = (Row) => { 
        this.props.history.push('/users/update/' + Row.id+"?1");
    }
 

    removeRow = (Row) => {
      let confirmMessage = window.confirm(`"${Row.UserName}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        this.setState({ buttonEnable: false });
        // userService.remove(Row).then(result => {
        //     if (result.status = 200) {
        //         userService.getAll().then(result => this.setState({ rows: result.data }));  
        //     }
        // }); 
        userService.remove(Row.id).then(data => {
          if (data.status == 200) {
            this.setState({
              rows: this.state.rows.filter(function (person) {
                return person.id !== Row.id
              })
            });
          }
          this.props.SnackbarAction('open', data.messages);
        });
      }
    }
    

    render() {
        const { classes } = this.props;
        if (this.state.Ready === false) {
            return  ( <div  style={{textAlign:"center"}}> <CircularProgress />    </div>)
        }
        var createbutton =  <Fab 
                color="primary" 
                aria-label="add" 
                size='small' 
                style={{ marginLeft: 8 }} 
                component={AdapterLink} 
                to="/users/create"
                >
                    <AddIcon />
                </Fab>

        return (
            <div>
                <MaterialTable
                    title={
                        <div>{`Kullanicilar (${this.state.rows.length}) `}
                            {checkPermission([PermissionData.postUsers]) ? createbutton : ""} 
                        </div>
                    }
                    title={"Kullanicilar(" + this.state.rows.length+")"}
                    columns={[
                        {
                            title: 'id',
                            field: 'id',
                            cellStyle: {
                                //backgroundColor: '#039be5',
                                //color: '#FFF'
                            },
                            headerStyle: {
                                //backgroundColor: '#039be5',
                            }
                        },
                        {title:'Kullanici Adi', field:'UserName'},
                        {title:'Adı', field:'FirstName'},
                        {title:'Soyadı', field:'LastName'},
                        {title:'Ekran adi', field:'DisplayName'},
                        {title:'Oluşturma Tarihi', field:'CreatedDate'},
                        {title:'Dashboard', field:'Dashboard'},
                       


                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1
                    }}
                    actions={[
                        rowData => ({
                            icon: 'lock',
                            tooltip: 'Yetkileri Güncelle',
                            onClick: (event, rowData) => this.permissionRow(rowData)
                        }),
                        rowData => ({
                            icon: 'update',
                            tooltip: 'user Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'user sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })


                    ]}
                />
                {/* {checkPermission(PermissionData.postUsers) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/users/create"
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                } */}
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};


function mapStateToProps(state){
    return {
        snackBarReducer: state.snackBarReducer
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        SnackbarAction: SnackbarAction
    },dispatch)
}
 

export default  connect(mapStateToProps, mapDispatchToProps)( PersonelListAndSearch);
