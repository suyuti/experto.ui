import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views"; 
import { Paper,Button } from "@material-ui/core";  
import { userService } from "../../Services";
import UsersFormu from '../../Components/Users/UsersFormu'  
import PermissionFormu from '../../Components/Users/PermissionFormu'  
import PermissionListForm from '../../Components/Users/PermissionListForm'   

import Snackbar from '../../Components/Snackbar' 
import { PermissionList } from "../../config";
 



 

var self;
class CreateAndUpdate extends Component {



    constructor(props) {
        super(props);
        self = this;
        this.state = {
            buttonEnable: true,
            message: "sadasd",
            value: 0,
            UsersFormu: {
            },
            UsersPermission:[]


        };

        this.paramsId = this.props.match.params.id;
        
        console.log(window.location.search.substring(1)); ///tab id
        var tabid=window.location.search.substring(1);
        tabid=parseInt(tabid);
        if (tabid) {
            this.state.value= tabid;
            console.log(this.state.value)
        }  

        if (this.paramsId) {
            userService.getById(this.paramsId).then(data => {
                this.setState({ UsersFormu: data.data[0] })

                /*this.setState(() => ({
                    UsersFormu: {
                        id:data.data[0].id,
                        EgitimDurumAdi: data.data[0].EgitimDurumAdi
                    }
                }));*/

            });
        }


    }

    handleTabChange(e, v) {
        self.setState({ value: v });
        console.log("handleTabChange");
    }
    handleSwipeViewChange(i) {
        self.setState({ value: i });
        console.log("handleChangeIndex");
    }

    handleChange = (input) => e => {
        console.log(e.target.value, input);
        let inputValue = e.target.value;
        this.setState(() => ({
            UsersFormu: {
                ...this.state.UsersFormu,
                [input]: inputValue
            }
        }));
    };
    createButton = (e) => {
       
        console.log(e);
        this.setState({buttonEnable:false}); 

        console.log(this.state.UsersFormu)
        // e.preventDefault();
        userService.create(this.state.UsersFormu).then(result => {
            console.log(result.status)
            if (result.status = 200) {
                window.location.href = `/users/update/${result.data[0].id}`;
                
                this.setState({buttonEnable:true});
            }
        });
         
    }


    updateButton = (e) => {
        console.log('update button');
        this.setState({ buttonEnable: false });
        userService.update(this.state.UsersFormu).then(data => {
            console.log(data.status)
            if (data.status = 200) { 
                userService.setUserRole(this.paramsId,this.state.UsersPermission).then(data => {
                    console.log(data.status)
                    if (data.status = 200) {
                        this.setState({ buttonEnable: true });
                    }

        
                }); 

            }

        });
        


        
    }

    setRole = (value) => {
       console.log(value) 
       this.state.UsersPermission=value;
    }


    render() {
        const { classes, ...props } = this.props;
        let button;
        if (this.state.buttonEnable) {
            if (this.paramsId) {
                button = <Button
                    variant="contained"
                    color="default"
                    onClick={this.updateButton}
                    style={styles.save_button}>UPDATE </Button>
            } else {
                button = <Button
                    variant="contained"
                    color="primary"
                    onClick={this.createButton}
                    style={styles.save_button}>KAYDET </Button>
            }

        } else {
            button = <Button
                variant="contained"
                color="primary" 
                style={styles.save_button}>loading</Button>

        }


        if (this.state.result) {
            
        }
        

        return (
            <div>
            <Snackbar message={this.state.message}/>
                <Paper> 
                                    <Tabs
                                        value={this.state.value}
                                        onChange={this.handleTabChange}
                                        indicatorColor="primary"
                                        textColor="primary"
                                        variant="fullWidth"
                                    >
                                        <Tab label='Users Formu' />
                                       {this.paramsId && <Tab label='Roller' />}
                                        {/* <Tab label='Permission Bilgileri' />
                                         */}

                                    </Tabs>
                                    

                                    <SwipeableViews
                                        style={{margin:"15px"}}
                                        axis="x"
                                        index={this.state.value}
                                        onChangeIndex={this.handleSwipeViewChange}>
                                        <div> 
                                            <UsersFormu
                                                handleChange={this.handleChange}
                                                data={this.state.UsersFormu} />
                                        </div>
                                        {this.paramsId &&  <div> 
                                            <PermissionListForm
                                             User_Id={this.paramsId}
                                             setRole={this.setRole}
                                             />
                                        </div>}
                                        {/* <div> 
                                             
                                                 <PermissionFormu
                                                handleChange={this.handleChange}
                                                data={this.state.UsersFormu} /> 
                                                 
                                        </div> */}
                                    </SwipeableViews>
                                    <br />

                                    {button} 
 
                          
                </Paper>



            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '20px',
        left: '20px'
    }
};

export default CreateAndUpdate;
