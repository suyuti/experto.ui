import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add"; 
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import {  personelProfilService,musteriService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';
import CircularProgress from '@material-ui/core/CircularProgress';  


const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));


class PersonelListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //role: null,
            rows: [],
            RowStatus: 1,//1 normal liste 2 ekleme 3 düzenleme,
            profilReady:false,
        };
    }



    componentDidMount() { 
       this.fetchData();
    }
    fetchData = () => {
        if (this.props.musteriId) {
            musteriService.getPersonelProfil(this.props.musteriId)
                .then(result => {
                    if (result.status === 200) {
                        if (result.data != null) {
                            this.setState({ rows: result.data })
                        }
                    }
                    this.setState({...this.state, profilReady: true })
                });
        } else {
            personelProfilService.getAll().then(result => this.setState({ rows: result.data, profilReady: true }));

        }
    }
    updateRow = (Row) => { 

        this.props.history.push('/personelProfil/update/' + Row.id);
        //alert("You saved " + rowData.id)
    }
 

    removeRow = (Row) => {
      // console.log('remove  button');
      let confirmMessage = window.confirm(`"${Row.ProfilAdi}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        this.setState({ buttonEnable: false });
        personelProfilService.remove(Row).then(result => {
          if (result.status = 200) {
            this.fetchData();
            // this.setState({
            //   ...this.state,
            //   rows: this.state.rows.filter(row => {
            //     return row.id !== Row.id
            //   })
            // })
          }
        });
      }
    }

 




    render() {
        const { classes } = this.props;

        if (this.state.profilReady === false) {
            return (<div style={{textAlign:"center"}}><CircularProgress /> </div>) 
        } 
   var PersonelProfilCreate=this.props.musteriId ? "/musteri/"+  this.props.musteriId +"/personelProfil/create":`/personelProfil/create`
       
     var createbutton =  <Fab 
                color="primary" 
                aria-label="add" 
                size='small' 
                style={{ marginLeft: 8 }} 
                component={AdapterLink} 
                to={PersonelProfilCreate}
                >
                    <AddIcon />
                </Fab>
       
       return (
            <div>
                <MaterialTable
                    title=
                    {<div>{`Personel Profil Listesi (${this.state.rows.length}) `}
                   { checkPermission([PermissionData.postPersonelProfil]) ?createbutton:""}
 
                    </div>
                    }
                     
                    columns={[
                        {
                            title: 'id',
                            field: 'id',
                            cellStyle: {
                                //backgroundColor: '#039be5',
                                //color: '#FFF'
                            },
                            headerStyle: {
                                //backgroundColor: '#039be5',
                            }
                        },
                        { title: 'Profil Adi', field: 'ProfilAdi' },
                       


                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize:10,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions: [10, 20]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Personel Profil  Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Personel Profil sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })


                    ]}
                />
                {/* {checkPermission([PermissionData.postPersonelProfil]) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to={createButtonLink}
                    >
                        <AddIcon />
                    </Fab>
                    : ' ' */} 
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};


export default withStyles(styles)(PersonelListAndSearch);
