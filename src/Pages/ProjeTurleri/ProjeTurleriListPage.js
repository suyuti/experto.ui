import React from "react";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {SnackbarAction} from '../../actions/snackbarAction';

import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add"; 
import {Link } from "react-router-dom";
import {projeTurleriService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';

const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));

class ListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            RowStatus: 1//1 normal liste 2 ekleme 3 düzenleme
        };
    }

    componentDidMount() {
        projeTurleriService.getAll().then(result => this.setState({ rows: result.data }));
    }

    updateRow = (Row) => { 
        this.props.history.push('/projeTurleri/update/' + Row.id);
    }

    removeRow = (Row) => {
      // alert('!')
      let confirmMessage = window.confirm(`"${Row.ProjeTurAdi}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        // checkPermission([PermissionData.deleteProjeTurleriId])
        // { this.props.SnackbarAction('open', "Silme Izniniz Bulunmamaktadır"); return; }
        // this.setState({ buttonEnable: false });
        // projeTurleriService.remove(Row).then(result => {
        //   console.log(result.status)
        //   if (result.status = 200) {
        //     this.props.SnackbarAction('open', result.message);
        //     projeTurleriService.getAll().then(result => this.setState({ rows: result.data }));
        //   }
        // });
      }
      return
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <MaterialTable
                    title={"Proje Turleri (" + this.state.rows.length+")"}
                    columns={[
                        { title: 'id', field: 'id'},
                        { title: 'projeTurleri Adi', field: 'ProjeTurAdi' },
                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize:10,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions: [10, 20, 30]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Görev Durumu Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Görev Durumunu sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })
                    ]}
                />
                {checkPermission([PermissionData.postProjeTurleri]) ? 
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/projeTurleri/create"
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                }
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};

function mapStateToProps(state){
    return {
        snackBarReducer: state.snackBarReducer
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({
        SnackbarAction: SnackbarAction
    },dispatch)
}
 
export default connect(mapStateToProps, mapDispatchToProps)(ListAndSearch);
