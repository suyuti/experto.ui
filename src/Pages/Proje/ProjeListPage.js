import React, {Component} from 'react'
import MaterialTable from 'material-table';
import { projeService,musteriService } from '../../Services';
import Fab from "@material-ui/core/Fab";
import { Link } from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";
import CircularProgress from '@material-ui/core/CircularProgress'; 
import {checkPermission,PermissionData } from '../../command';

const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));
export default class ProjeListPage extends Component {
    constructor(props){
        super(props);
       // alert(this.props.musteriId);
    }
    
    state ={
        projeler:[],
        isReady : false
    }

    updateRow(rowData) { 
      //  alert("updateRow  =" + rowData.id); 
        if (this.props.musteriId) {
            this.props.history.push('/musteri/' + this.props.musteriId + '/proje/update/' + rowData.id);
        }
        else {
            this.props.history.push('/proje/update/' + rowData.id);
        }  
    }

    removeRow(rowData) {
       // alert("!")
        //return
        let confirmMessage = window.confirm(`"${rowData.ProjeAdi}" (${rowData.ProjeKodu}) silinecektir. Emin misiniz?`);
        if (confirmMessage == true) {
            projeService.remove(rowData).then(resp => {
                if (resp.status === 200) {
                    // musteriService.getProjects(this.props.musteriId)
                    // .then(result => {
                    //     if (result.status === 200) {
                    //         if (result.data != null) {
                    //             this.setState({projeler: result.data, isReady: true })
                    //         }
                    //     }
                    // });
                    this.setState({
                      projeler: this.state.projeler.filter(proje => {
                        return proje.id !== rowData.id
                      }),
                      isReady: true
                    })
                }
            })
      }
    }

    componentDidMount() {
        if (this.props.musteriId) {
            musteriService.getProjects(this.props.musteriId)
            .then(result => {
                if (result.status === 200) {
                    if (result.data != null) {
                        this.setState({projeler: result.data, isReady: true })
                    }
                }else if(result.status === 400) {
                    this.setState({projeler: [], isReady: true })
                }
            });

        } else {
            projeService.getAll().then(result => this.setState({ projeler: result.data, isReady: true }))
        }
    }
    render() {
        if (this.state.isReady === false) {
            return <div style={{ textAlign: "center"}}>
                        <CircularProgress   /> 
                    </div>
        }
        return(
            <div style={{position:"relative"}}>
                <MaterialTable
                    title="Projeler"
                    columns={[
                        //{title:'Id', field:"id"},
                        {title:'Proje Adi', field:"ProjeAdi"},
                        {title:'Proje Kodu', field:"ProjeKodu"},
                        {title:'Firma', field:"FirmaAdi"},
                        {title:'Türü', field:"ProjeTurAdi"},
                        {title:'Alanı', field:"ProjeAlanAdi"}
                    ]} 
                    data= {this.state.projeler}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize:20,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions: [20, 50, 100]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Proje Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Proje sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })
                    ]}/>
                                        
    	            {/*checkPermission([PermissionData.postPersonel])*/}
                    { true ? 
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={this.props.musteriId? styles.fabIn :styles.fab}
                        component={AdapterLink}
                        to={(this.props.musteriId)?"/musteri/"+this.props.musteriId+"/proje/create":"/proje/create"}
                    >
                        <AddIcon />
                    </Fab> 
                    : ' '
                }

            </div>
        )
    }
}
const styles = {
    fab: {
        margin: 1, 
        right: "50px",
        bottom: "50px" ,
        position: "fixed"
    },
    fabIn: {
        margin: 13,
        right: 0,
        position: "absolute", 
    }
};
