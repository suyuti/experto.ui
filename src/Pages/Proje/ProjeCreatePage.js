import React, {Component} from 'react'
import { Grid} from '@material-ui/core';
import ProjeCreate from '../../Components/Proje/ProjeCreate';

class ProjeCreatePage extends Component {
    render() {
        return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <ProjeCreate musteriId={this.props.match.params.id} />
                </Grid>
            </Grid>
        </div>)
    }
}

export default ProjeCreatePage;