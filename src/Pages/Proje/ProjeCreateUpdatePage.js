import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { projeService } from '../../Services';
import ProjeForm from '../../Components/Proje/ProjeForm'
import { Button, Tab, Tabs, CircularProgress, Typography } from "@material-ui/core";
import SwipeableViews from "react-swipeable-views";
import PropTypes from "prop-types";
import IsPaketleriPage from '../IsPaketi/IsPaketleriPage'
import {SnackbarAction} from '../../actions/snackbarAction';


function TabContainer({ children, dir }) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {children}
        </Typography>
    );
}
TabContainer.propTypes = {
    children: PropTypes.node.isRequired
};


class ProjeCreateUpdatePage extends Component {
    constructor(props) {
        super(props)
        this.state={
            selectedTab:0,
            ProjeForm:{
                MusteriId:null,
                ProjeBaslangicTarihi:new Date(),
                ProjeBitisTarihi:new Date()
            },
            isCreate: (this.props.match.params.id === undefined),
            isReady: (this.props.match.params.id?false:true)
        }
        this.musteriId=this.props.match.params.mid
        this.onCreate = this.onCreate.bind(this)
        this.onUpdate = this.onUpdate.bind(this)
        this.onTabChange = this.onTabChange.bind(this);
        this.onFormUpdated = this.onFormUpdated.bind(this)
        if (this.props.match.params.id === undefined) {
            this.state.ProjeForm.MusteriId=this.props.match.params.mid;
            this.setState({isReady:true})
        }
    }

    componentDidMount() {
        if (this.state.isCreate === false) {

            projeService.getById(this.props.match.params.id).then(result => this.setState({ProjeForm:result.data[0], isReady:true}))
        }
    }

    onTabChange(e, v) {
        this.setState({ selectedTab: v });
    }

    validateInput = () => {
      if (!this.state.ProjeForm.ProjeAdi) {
        return this.props.SnackbarAction('open', "Lütfen ProjeAdi alanını giriniz");
      }
      if (!this.state.ProjeForm.ProjeKodu) {
        return this.props.SnackbarAction('open', "Lütfen ProjeKodu alanını giriniz");
      }

      if (this.state.ProjeForm.ProjeBaslangicTarihi >= this.state.ProjeForm.ProjeBitisTarihi) {
        return this.props.SnackbarAction('open', "Lütfen Proje tarihlerini kontrol ediniz");
      }
    }

    onCreate() {
        if(this.validateInput()) return;

        // mask string veriyor. burada numaraya ceviriliyor 
        var SunulanButce=String(this.state.ProjeForm.SunulanButce)  
        SunulanButce=SunulanButce.replace(" ","") 
        var Onaylanan=String(this.state.ProjeForm.Onaylanan)  
        Onaylanan=Onaylanan.replace(" ","")   
        this.state.ProjeForm.SunulanButce   = parseInt(SunulanButce)   
        this.state.ProjeForm.Onaylanan  =  parseInt(Onaylanan) 

        projeService.create(this.musteriId,this.state.ProjeForm).then(data => {
            if (data.status == 200) {
              if (data.messages != "") {
                this.props.SnackbarAction('open', data.messages);
              }
                this.props.history.goBack();
            } else{
                this.props.SnackbarAction('open',data.messages); 
            }
        });
        
    }

    onUpdate() {
        // projeService.update(this.state.ProjeForm)
        // this.props.history.goBack();
        if(this.validateInput()) return;
        projeService.update(this.state.ProjeForm).then(resp => {
          if (resp.messages != "") {
            this.props.SnackbarAction('open', resp.messages);
          }
          if (resp.status === 200) {
            this.props.history.goBack();
          }
        })
    }

    onFormUpdated = data => e => {
        let inputValue
        if(e.target==undefined){
             inputValue = e
        }else{
              inputValue = e.target.value; 
        } 
        this.setState(() => ({
            ProjeForm: {
                ...this.state.ProjeForm,
                [data]: inputValue
            }
        }));
    };

    render() {
        if (this.state.isReady === false) {
            return <CircularProgress />
        }
        return(
            <div>
                <Tabs
                    value = {this.state.selectedTab}
                    onChange = {this.onTabChange}
                    indicatorColor = "primary"
                    textColor = "primary"
                >
                    <Tab label="Proje Bilgileri" />
                    {this.props.match.params.id && <Tab label="İş Paketleri" />}
                </Tabs>
                <SwipeableViews
                    axis="x"
                    index={this.state.selectedTab}
                    onChangeIndex={this.handleChangeIndex} >
                    <TabContainer>
                        <ProjeForm 
                            musteriId={this.musteriId} 
                            data={this.state.ProjeForm} 
                            handleChange={this.onFormUpdated} 
                        /></TabContainer>
                    <TabContainer>
                        <IsPaketleriPage  
                            musteriId={this.musteriId} 
                            projeAdi={this.state.ProjeForm.ProjeAdi} 
                            projeId={this.props.match.params.id} 
                            ProjeForm={this.state.ProjeForm}
                        /> </TabContainer>
                </SwipeableViews>
                
                <Button  variant="contained" color="primary"  disabled={!this.state.ProjeForm.MusteriId}
                    onClick={this.state.isCreate?this.onCreate:this.onUpdate}>{this.state.isCreate?"Yeni Olustur":"Guncelle"}</Button>
                
            </div>
        )
    }
}


function mapStateToProps(state){
    return {
        snackBarReducer: state.snackBarReducer
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        SnackbarAction: SnackbarAction
    },dispatch)
}
  
export default connect(mapStateToProps, mapDispatchToProps)(ProjeCreateUpdatePage);
