import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import CircularProgress from '@material-ui/core/CircularProgress'; 
import { Link } from "react-router-dom";
import {  projeService,musteriService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';



const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));


class  ListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            rows: [],
            musteriListesi: [],
            gecerliMusteri: [],
            RowStatus: 1,//1 normal liste 2 ekleme 3 düzenleme
            loaderShow:true
        }; 
        this.paramsMusteriId = props.match.params.mid; 
           
    }

    componentDidMount() {
          

        if( this.paramsMusteriId){ 

            musteriService.getById(this.paramsMusteriId).then(result => {
                if (result.status = 200) {
                    this.setState({ loaderShow: false });
                    this.setState({ gecerliMusteri: result.data[0] });
                    
                }
            });
            musteriService.getProjects(this.paramsMusteriId).then(result => this.setState({ rows: result.data }));
        }else{
            projeService.getAll().then(result => this.setState({ rows: result.data }));
        }

    }

    updateRow = (Row) => {
        this.props.history.push('/proje/update/' + Row.id);
    }
 
    removeRow = (Row) => {
        console.log('remove  button');
        this.setState({ buttonEnable: false });
        projeService.remove(Row).then(result => {
             
            if (data.status == 200) {   
                this.setState({rows: this.state.rows.filter(function(item) { 
                    return item.id !== Row.id
                })});    
            }
              this.props.SnackbarAction('open', data.messages); 

        });


     

    }

    isPaketineGit = (Row) => {

       // this.props.history.push( );
       var  link =`/proje/${Row.Code}/ispaketi`;
        this.props.history.push(link);
    }


 

    render() {
        const { classes } = this.props;

        if (!this.state.rows.length) {
            return (
                <div style={{textAlign:"center"}}>
                   <CircularProgress   /> 
                </div>
            )
        }

        return (
            <div>
                {(this.state.gecerliMusteri.FirmaAdi) ?
                    <div style={styles.musteriBilgisi}>
                         {this.state.gecerliMusteri.FirmaAdi}  
                    </div>
                    :""
                }
                 
                <MaterialTable
                    title={ "Proje  Listesi (" + this.state.rows.length+")"}
                    
                    columns={[
                        {
                            title: 'Code',
                            field: 'Code',
                            cellStyle: {
                                //backgroundColor: '#039be5',
                                //color: '#FFF'
                            },
                            headerStyle: {
                                //backgroundColor: '#039be5',
                            }
                        },
                        { title: 'proje Adi', field: 'ProjeAdi' },
                        { title: 'Musteri Id', field: 'MusteriId' },
                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Proje Durumu Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Projeyi  sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        }),
                        rowData => ({
                            icon: 'assignment',
                            tooltip: 'İş paketine git',
                            onClick: (event, rowData) => this.isPaketineGit(rowData)
                        }),


                    ]}
                />
                {checkPermission([PermissionData.postProje]) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/proje/create"
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                }
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    },
    musteriBilgisi:{
        display: "block",
        background:"#b8b6b638",
        padding: "10px",
        borderTop: "4px solid",
        borderBottom:"1px solid",
        borderRadius: "8px 8px 0 0",
        borderColor: "#3f51b5"
        
    }
};


export default ListAndSearch;
