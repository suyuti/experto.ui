import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
    Grid, 
    Button, 
    Card,
    CardActions,
    CardContent
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    card: {
        minWidth: 275
    },
    title: {
        fontSize: 14
    }
});

class AdminDashboard extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div>
                <Grid container className={classes.root} spacing={2}>
                    <Grid item xs={3}>
                        <Card className={classes.card}>
                            <CardContent>
                                <div
                                    className={classes.title}
                                    color="textSecondary"
                                    gutterBottom
                                >
                                    Kullanici Sayisi
                                </div>
                                <div variant="h5" component="h2">12</div>
                            </CardContent>
                            <CardActions>
                                    <Link to="/admin/user">
                                        <Button size="small">Kullanicilar</Button>
                                    </Link>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item xs={3}>
                        <Card className={classes.card}>
                            <CardContent>
                                <div
                                    className={classes.title}
                                    color="textSecondary"
                                    gutterBottom
                                >
                                    Rol Sayisi
                                </div>
                                <div variant="h5" component="h2">12</div>
                            </CardContent>
                            <CardActions>
                                <Button size="small">Learn More</Button>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(AdminDashboard);
