import React, {Component} from 'react'
import MaterialTable from 'material-table';
import { userService } from '../../Services';
import { Link } from "react-router-dom";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add"; 

import {checkPermission,PermissionData } from '../../command';

const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));
 
class UsersPage extends Component {
    state = {
        users : []
    }
    componentDidMount() {
        userService.getAll().then(users => this.setState({users:users}))
    }
    render() {
        const {classes} = this.props
        return(
            <div>
                <MaterialTable 
                    columns = {[
                        {title:'Kullanici Adi', field:'UserName'},
                        {title:'Adı', field:'FirstName'},
                        {title:'Soyadı', field:'LastName'},
                        {title:'Ekran adi', field:'DisplayName'},
                        {title:'Oluşturma Tarihi', field:'CreatedDate'},
                        {title:'Dashboard', field:'Dashboard'},
                    ]}
                    data = {this.state.users}
                    title = "Kullanicilar"
                />

                {checkPermission([PermissionData.postUsers]) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/user/create"
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                } 
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    } 
};
export default  UsersPage;