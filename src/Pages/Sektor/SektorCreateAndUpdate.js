import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views"; 
import { Paper,Button } from "@material-ui/core";  
import { sektorService } from "../../Services";
import SektorFormu from '../../Components/Sektor/SektorFormu'  
import Snackbar from '../../Components/Snackbar' 
 
var self;
class CreateAndUpdate extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            buttonEnable: true,
            message: "sadasd",
            value: 0,
            SektorFormu: {
            }

        };

        this.paramsId = this.props.match.params.id;
        if (this.paramsId) {
            sektorService.getById(this.paramsId).then(data => {
                this.setState({ SektorFormu: data.data[0] })
            });
        }
        this.handleTabChange = this.handleTabChange.bind(this)
        this.handleSwipeViewChange = this.handleSwipeViewChange.bind(this)
        this.handleChange = this.handleChange.bind(this)
        
    }

    handleTabChange(e, v) {
        this.setState({ value: v });
    }

    handleSwipeViewChange(i) {
        this.setState({ value: i });
    }

    handleChange = (input) => e => {
        let inputValue = e.target.value;
        this.setState(() => ({
            SektorFormu: {
                ...this.state.SektorFormu,
                [input]: inputValue
            }
        }));
    };

    createButton = (e) => {
        this.setState({buttonEnable:false}); 
        // e.preventDefault();
        sektorService.create(this.state.SektorFormu).then(data => {
            if (data.status === 200) {
                this.setState({buttonEnable:true});
                this.props.history.goBack();
            }
        });
    }

    updateButton = (e) => {
        this.setState({ buttonEnable: false });
        sektorService.update(this.state.SektorFormu).then(data => {
            if (data.status === 200) {
                this.setState({ buttonEnable: true });
                this.props.history.goBack();
            }
        });
    }

    render() {
        const { classes, ...props } = this.props;
        let button;
        if (this.state.buttonEnable) {
            if (this.paramsId) {
                button = <Button
                    variant="contained"
                    color="default"
                    onClick={this.updateButton}
                    style={styles.save_button}>GÜNCELLE</Button>
            } else {
                button = <Button
                    variant="contained"
                    color="primary"
                    onClick={this.createButton}
                    style={styles.save_button}>KAYDET</Button>
            }
        } 
        else {
            button = <Button
                variant="contained"
                color="primary" 
                style={styles.save_button}>...</Button>
        }

        if (this.state.result) {
            
        }
        return (
            <div>
                <Snackbar message={this.state.message}/>
                <Paper> 
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleTabChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="fullWidth">
                        <Tab label='Sektor Formu' />
                    </Tabs>
                    <SwipeableViews
                        style={{margin:"15px"}}
                        axis="x"
                        index={this.state.value}
                        onChangeIndex={this.handleSwipeViewChange}>
                        <div> 
                            <SektorFormu
                                handleChange={this.handleChange}
                                data={this.state.SektorFormu} />
                        </div>
                    </SwipeableViews>
                    <br />
                    {button} 
                </Paper>
            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '20px',
        left: '20px'
    }
};

export default CreateAndUpdate;
