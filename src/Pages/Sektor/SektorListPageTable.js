import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { Link } from "react-router-dom";
import {  sektorService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';

const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));

class PersonelListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            RowStatus: 1//1 normal liste 2 ekleme 3 düzenleme
        };
    }

    componentDidMount() {
        sektorService.getAll().then(result => this.setState({ rows: result.data }));
    }

    updateRow = (Row) => {
        this.props.history.push('/sektor/update/' + Row.id);
    }

    removeRow = (Row) => {
      // alert('!')
      let confirmMessage = window.confirm(`"${Row.SektorAdi}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        // this.setState({ buttonEnable: false });
        // sektorService.remove(Row).then(result => {
        //     if (result.status === 200) {
        //         sektorService.getAll().then(result => this.setState({ rows: result.data }));  
        //     }
        // });
      }
      return
    }
    
    render() {
        const { classes } = this.props;
        return (
            <div>
                <MaterialTable
                    title={"Sektor  Listesi (" + this.state.rows.length+")"}
                    columns={[
                        { title: 'id', field: 'id' },
                        { title: 'sektor Adi', field: 'SektorAdi' },
                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize:10,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions: [10, 20, 30]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Sektör Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Sektör sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })
                    ]}
                />
                {checkPermission([PermissionData.postSektor]) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/sektor/create"
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                }
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};

export default PersonelListAndSearch;
