import React, {Component} from 'react'

class HomePage extends Component {
    state ={
        id: 0
    }
    componentDidMount() {
        const { match: { params } } = this.props;
        //const {h} = this.props.match.params
        this.setState({id:params.id})
    }
    render() {
        return (
            <div>Home 
                {this.state.id}

            </div>
        )
    }
} 

export default HomePage;