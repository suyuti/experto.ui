import React from "react";
import {
  Grid,
  Card,
  CardContent,
} from "@material-ui/core";
import Chart from "react-google-charts";

// Reference : https://developers.google.com/chart/interactive/docs/gallery/timeline
const columns = [
  { type: "string", id: "Code" },
  { type: "string", id: "Name" },
  { type: "date", id: "Start" },
  { type: "date", id: "End" }
];

const rows = [
  ["PCODE-1", "Proje 1", new Date(2016, 3, 30), new Date(2017, 2, 4)],
  ["PCODE-2", "Proje 2", new Date(2016, 11, 4), new Date(2019, 2, 4)],
  ["PCODE-3", "Proje 3", new Date(2019, 11, 2), new Date(2021, 1, 1)],
  ["PCODE-5", "Proje 5", new Date(2016, 2, 1), new Date(2017, 2, 8)],
  ["PCODE-6", "Proje 6", new Date(2016, 11, 2), new Date(2017, 2, 8)],
  ["PCODE-7", "Proje 7", new Date(2018, 2, 4), new Date(2021, 6, 6)],
  ["PCODE-8", "Proje 8", new Date(2016, 2, 1), new Date(2017, 2, 8)],
  ["PCODE-9", "Proje 9", new Date(2016, 11, 2), new Date(2017, 2, 8)],
  ["PCODE-10", "Proje 10", new Date(2018, 2, 4), new Date(2021, 6, 6)]
];

// var options = {
//   titleTextStyle: {
//       // color: red,    // any HTML string color ('red', '#cc00cc')
//       fontFamily: "Times New Roman", // i.e. 'Times New Roman'
//       // fontSize: <number>, / v true of false
//   }
// }

const ProjectsTimeline = () => {
  return (
    <Grid container className="mi-card-root">
      <Grid item xs={12}>
        <Card className="card projects-card">
          <h1 className="projects-title">Projeler</h1>
          <CardContent>
            <Chart
              chartType="Timeline"
              data={[columns, ...rows]}
              width="100%"
              height="350px"
            />
          </CardContent>
        </Card>

      </Grid>
    </Grid>
  )
}

export default ProjectsTimeline;