import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
    Grid,
    Button,
    Card,
    CardActions,
    CardContent,
    Divider
} from "@material-ui/core";
import PeopleIcon from '@material-ui/icons/People';

class MusteriPersonelCard extends Component {
    render() {
        return (
            <Grid item xs={12} className="card-root">
                <Card className="card">
                    <CardContent className="card-body">
                        <Grid container>
                            <Grid item xs={12}>
                                <p className="card-category" >Personel</p>
                            </Grid>
                            <Grid item xs={1}></Grid>
                            <Grid item xs={4}>
                                <PeopleIcon className="icon" fontSize={"large"} htmlColor="gray"/>
                            </Grid>
                            <Grid item xs={6} >
                                <p className="numbers" >58<span className="numbers-span" >kişi</span></p>
                            </Grid>
                            <Grid item xs={1}></Grid>
                        </Grid>
                    </CardContent>
                    <Divider className="divider" />
                    <CardActions className="card-footer">
                        <Link to="/dashboard" className="button-detay-link">
                            <center><Button size="small" className="button-detay" > Detay </Button></center>
                        </Link>
                    </CardActions>
                </Card>
            </Grid>
        );
    }
}

export default MusteriPersonelCard;
