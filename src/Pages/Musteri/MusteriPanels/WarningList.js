import React, { Component } from "react";
import {
  Grid,
  Card,
  CardContent,
  Divider,
} from "@material-ui/core";
import WarningIcon from '@material-ui/icons/Warning';

class WarningList extends Component {
  render() {
    return (
      <div>
        <Grid container className="mi-card-root" spacing={2}>
          <Grid item xs={12}>
            <Card className="card uyari-card"> 
              <h1 className="uyari-title">Uyarılar</h1>
              <CardContent >
                <Uyarı tarih={"2017-09-16T12:22:46.587Z"} name={"X şirketinde..."} />
                <Uyarı tarih={"2017-09-16T12:22:46.587Z"} name={"A proje tarihi hatalı!"} />
                <Uyarı tarih={"2017-09-16T12:20:46.587Z"} name={"İş paketleri..."} />
                <Uyarı tarih={"1997-09-16T12:22:46.587Z"} name={"Eksik Proje!"} />
                <Uyarı tarih={"2017-09-16T12:20:46.587Z"} name={"İş paketleri..."} />
                {/* <Uyarı tarih={"1997-09-16T12:22:46.587Z"} name={"Eksik Proje!"} />
                <Uyarı tarih={"2017-09-16T12:20:46.587Z"} name={"İş paketleri..."} />
                <Uyarı tarih={"1997-09-16T12:22:46.587Z"} name={"Eksik Proje!"} />
                <Uyarı tarih={"2017-09-16T12:20:46.587Z"} name={"İş paketleri..."} />
                <Uyarı tarih={"1997-09-16T12:22:46.587Z"} name={"Eksik Proje!"} /> */}
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export const Uyarı = props => {
  let tarih = new Date(props.tarih);
  let saat = tarih.toLocaleTimeString();
  let gun = tarih.toLocaleDateString();
  return (
    <>
      <Grid container>
        <Grid item xs={2} className="text-center">
          <WarningIcon fontSize={"small"} className="uyari-icon" htmlColor="gray" />
        </Grid>
        <Grid item xs={10}>
          <h3>{props.name}</h3>
          <h4>{saat}<small className="uyari-small"> {gun}</small></h4>
        </Grid>
      </Grid>
      <Divider className="uyari-divider" />
    </>
  );
};

export default WarningList;
