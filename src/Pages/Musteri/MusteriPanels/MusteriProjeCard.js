import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Grid,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider
} from "@material-ui/core";
import DescriptionIcon from '@material-ui/icons/Description';

class MusteriProjeCard extends Component {
  render() {
    return (
      <Grid item xs={12} className="card-root">
        <Card className="card">
          <CardContent className="card-body">
            <Grid container>
              <Grid item xs={12}>
                <p className="card-category" >Proje</p>
              </Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={4}>
                <DescriptionIcon className="icon" fontSize={"large"} htmlColor="gray" />
              </Grid>
              <Grid item xs={6} >
                {/* <p className="card-category" >Projeler</p> */}
                <p className="numbers" >151<span className="numbers-span" >adet</span></p>
              </Grid>
              <Grid item xs={1}></Grid>
            </Grid>
          </CardContent>
          <Divider className="divider" />
          <CardActions className="card-footer">
            <Link to="/dashboard" className="button-detay-link">
              <center><Button size="small" className="button-detay" > Detay </Button></center>
            </Link>
          </CardActions>
        </Card>
      </Grid>
    );
  }
}

export default MusteriProjeCard;
