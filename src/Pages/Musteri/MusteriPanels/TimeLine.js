import React, { Component } from "react";
import {
	Grid,
	Card,
	CardContent
} from "@material-ui/core";

import PersonIcon from '@material-ui/icons/Person';
import GroupIcon from '@material-ui/icons/Group';
import AddAlarmIcon from '@material-ui/icons/AddAlarm';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

const datt = [
	{ ts: new Date(Date.now()).toString().split("GMT")[0].trim(), text: 'Logged in' },
	{ ts: new Date("2017-09-17"), text: 'Clicked Home Page' },
	{ ts: new Date("2017-09-18"), text: 'Edited Profile' },
	{ ts: "2017-09-16T12:22:46.587Z", text: 'Registred' },
	{ ts: "2018-01-16T12:21:46.587Z", text: 'Clicked Cart' },
	{ ts: "2018-09-16T12:20:46.587Z", text: 'Clicked Checkout' },
	{ ts: "2017-03-16T12:21:46.587Z", text: 'Clicked Cart' },
	{ ts: "2017-11-16T12:20:46.587Z", text: 'Clicked Checkout' },
	{ ts: "2015-09-16T12:21:46.587Z", text: 'Clicked Cart' },
	{ ts: "2017-09-16T12:20:46.587Z", text: 'Clicked Checkout' },
	{ ts: "2027-09-16T12:21:46.587Z", text: 'Clicked Cart' },
	{ ts: "2017-09-16T12:20:46.587Z", text: 'Clicked Checkout' },
	{ ts: "2017-09-17T12:20:46.587Z", text: 'Edited Profile' },
	{ ts: "2011-09-16T12:22:46.587Z", text: 'Registred' },
	{ ts: "2017-09-17T12:20:46.587Z", text: 'Edited Profile' },
	{ ts: "2017-09-16T12:22:46.587Z", text: 'Registred' },
	{ ts: "2007-09-17T12:20:46.587Z", text: 'Edited Profile' },
	{ ts: "1997-09-16T12:22:46.587Z", text: 'Registred' },
];
let events = datt.sort(function (a, b) {
	a = new Date(a.ts);
	b = new Date(b.ts);
	return a > b ? -1 : a < b ? 1 : 0;
});

class TimeLine extends Component {
	render() {
		const { classes } = this.props;
		return (
			<Grid container className="mi-card-root" spacing={2}>
				<Grid item xs={12}>
					<Card className="card timeline-card" >
							<h1 className="timeline-title">
								Timeline
							</h1>
						<CardContent>
							<VerticalTimeline layout={'1-column'} animate={false}>

								<VerticalTimelineElement
									className="vertical-timeline-element--work"
									contentStyle={{ background: 'gray', color: '#fff' }}
									contentArrowStyle={{ borderRight: '7px solid gray' }}
									date={Date(Date.now()).toString().split("GMT")[0].trim()}
									iconStyle={{ background: '#fff', color: 'gray' }}
									icon={<PersonIcon htmlColor="gray"/>}
								>
									<h3 className="vertical-timeline-element-title">Creative Director</h3>
									{/* <h4 className="vertical-timeline-element-subtitle">Miami, FL</h4> */}
								</VerticalTimelineElement>

								<VerticalTimelineElement
									className="vertical-timeline-element--work"
									contentStyle={{ background: 'rgb(33, 150, 243)', color: '#000' }}
									contentArrowStyle={{ borderRight: '7px solid rgb(33, 150, 243)' }}
									date={Date(Date.now()).toString().split("GMT")[0].trim()}
									iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
									icon={<GroupIcon />}
								>
									<h3 className="vertical-timeline-element-title timeline-card-h3">Creative Director</h3>
									<h4 className="vertical-timeline-element-subtitle">Miami, FL</h4>
								</VerticalTimelineElement>
								<VerticalTimelineElement
									className="vertical-timeline-element--work"
									date={Date(Date.now()).toString().split("GMT")[0].trim()}
									iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
									icon={<ArrowDownwardIcon />}
								>
									<h3 className="vertical-timeline-element-title">Web Designer</h3>
									<h4 className="vertical-timeline-element-subtitle">Los Angeles, CA</h4>
									<p>
										User Experience, Visual Design
																	</p>
								</VerticalTimelineElement>
								<VerticalTimelineElement
									className="vertical-timeline-element--work"
									date={Date(Date.now()).toString().split("GMT")[0].trim()}
									iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
									icon={<AddAlarmIcon />}
								>
									<h3 className="vertical-timeline-element-title">Web Designer</h3>
									<h4 className="vertical-timeline-element-subtitle">San Francisco, CA</h4>
									<p>
										User Experience, Visual Design
																	</p>
								</VerticalTimelineElement>
								<VerticalTimelineElement
									className="vertical-timeline-element--work"
									contentStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
									contentArrowStyle={{ borderRight: '7px solid  rgb(33, 150, 243)' }}
									date={Date(Date.now()).toString().split("GMT")[0].trim()}
									iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
									icon={<PersonIcon />}
								>
									<h3 className="vertical-timeline-element-title">Creative Director</h3>
									<h4 className="vertical-timeline-element-subtitle">Miami, FL</h4>
								</VerticalTimelineElement>
								<VerticalTimelineElement
									className="vertical-timeline-element--education"
									date={Date(Date.now()).toString().split("GMT")[0].trim()}
									iconStyle={{ background: 'rgb(233, 30, 99)', color: '#fff' }}
									icon={<PersonIcon />}
								>
									<h3 className="vertical-timeline-element-title">Agile Development Scrum Master</h3>
									<h4 className="vertical-timeline-element-subtitle">Certification</h4>
									<p>
										Creative Direction, User Experience, Visual Design
																	</p>
								</VerticalTimelineElement>
								<VerticalTimelineElement
									className="vertical-timeline-element--education"
									date="2002 - 2006"
									iconStyle={{ background: 'rgb(233, 30, 99)', color: '#fff' }}
									icon={<PersonIcon />}
								>
									<h3 className="vertical-timeline-element-title">Bachelor of Science in Interactive Digital Media Visual Imaging</h3>
									<h4 className="vertical-timeline-element-subtitle">Bachelor Degree</h4>
									<p>
										Creative Direction, Visual Design
																	</p>
								</VerticalTimelineElement>
								<VerticalTimelineElement
									iconStyle={{ background: 'rgb(16, 204, 82)', color: '#fff' }}
									icon={<PersonIcon />}
								/> </VerticalTimeline>
						</CardContent>
					</Card>
				</Grid>
			</Grid>
		);
	}
}

export default TimeLine;
