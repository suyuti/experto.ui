import React from 'react';
import { Grid } from "@material-ui/core";
import { Link } from "react-router-dom";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';

import BackupIcon from '@material-ui/icons/Backup';
import DialpadIcon from '@material-ui/icons/Dialpad';
import GetAppIcon from '@material-ui/icons/GetApp';
import PublishIcon from '@material-ui/icons/Publish';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const TopPane = () => {
  return (
    <Grid container class="mi-card-root">
      <Grid item xs={12} style={{ marginLeft: 0 }}>
        <ul className="top-pane">
          <li>
            <div className="dropdown">
              <button className="dropbtn"> ADAM AY <ArrowDropDownIcon className="top-pane-list-icon" fontSize={"small"} /></button>
              <div className="dropdown-content">
                <Link className="top-pane-link" to="/dashboard">
                  <DialpadIcon fontSize="small" className="top-pane-icon text-left" /> HESAPLA
                </Link>
                <Link className="top-pane-link" to="/dashboard">
                  <InboxIcon fontSize="small" className="top-pane-icon text-left" /> ÇIKTI
                </Link>
                <Link className="top-pane-link" to="/dashboard">
                  <AccountCircleIcon fontSize="small" className="top-pane-icon text-left" /> ACTION
                </Link>
              </div>
            </div>
          </li>
          <li>
            <div className="dropdown">
              <button className="dropbtn"> PDKS <ArrowDropDownIcon className="top-pane-list-icon" fontSize={"small"} /></button>
              <div className="dropdown-content">
                <Link className="top-pane-link" to="/dashboard">
                  <PublishIcon fontSize="small" className="top-pane-icon text-left" /> DOSYA GİRİŞ
                </Link>
                <Link className="top-pane-link" to="/dashboard">
                  <DialpadIcon fontSize="small" className="top-pane-icon text-left" /> HESAPLA
                </Link>
                <Link className="top-pane-link" to="/dashboard">
                  <InboxIcon fontSize="small" className="top-pane-icon text-left" /> EXCEL
                </Link>
              </div>
            </div>
          </li>
        </ul>
      </Grid>
    </Grid>
  )
}

export default TopPane;