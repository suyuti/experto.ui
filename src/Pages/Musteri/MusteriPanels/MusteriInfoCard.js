import React, { Component } from "react";
import {
  Grid,
  Card,
  CardActions,
  CardContent
} from "@material-ui/core";
import { Link } from "react-router-dom";
import AcUnitIcon from '@material-ui/icons/AcUnit';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';

class MusteriCard extends Component {
  render() {
    return (
      <div>
        <Grid container className="mi-card-root" spacing={2}>
          <Grid item xs={12}>
            <Card className="card mi-card">
              <CardContent className="card-body">
                <span title="ARGE"><AcUnitIcon className="mi-icon" fontSize={"large"} htmlColor="" /></span>
                <h1>Tabanlıoğlu Mimarlık Anonim Şirketi</h1>
                <h2><ArrowRightIcon fontSize={"small"} className="mi-list-icon" /> Sanayi</h2>
                <h2><ArrowRightIcon fontSize={"small"} className="mi-list-icon" />  Redüktör</h2>
                <h2><ArrowRightIcon fontSize={"small"} className="mi-list-icon" />  Kimya</h2>
                <h2><ArrowRightIcon fontSize={"small"} className="mi-list-icon" />  İstanbul</h2>
                <h2><ArrowRightIcon fontSize={"small"} className="mi-list-icon" />
                  <Link className="button-detay-link" to="/dashboard" target="_blank" >http://localhost:3000/dashboard
                  </Link>
                </h2>
                <h2><ArrowRightIcon fontSize={"small"} className="mi-list-icon" />  Tozkoparan, Haldun Taner Sk. 34173 Güngören/İstanbul</h2>
              </CardContent>
              <CardActions className="maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d24083.971803180582!2d28.876303342317424!3d41.01439290038364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cabb30c8f940cd%3A0x6d9910c996630b7a!2sExperto%20AR-GE%20Dan%C4%B1%C5%9Fmanl%C4%B1k%20Hizmetleri!5e0!3m2!1str!2str!4v1573737716327!5m2!1str!2str"
                  frameBorder="0" width="100%" height="125px" style={{ borderRadius: 4 }}></iframe>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default MusteriCard;
