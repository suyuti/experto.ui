import React, { Component } from "react";
import {
  Grid,
  Card,
  CardContent,
  Divider
} from "@material-ui/core";
import LabelImportantIcon from '@material-ui/icons/LabelImportant';

class UrunlerList extends Component {
  render() {
    return (
      <div>
        <Grid container className="mi-card-root" spacing={2}>
          <Grid item xs={12}>
            <Card className="card urun-card"> 
              <h1 className="urun-title">Ürünler</h1>
              <CardContent >
                <Urun not={"bla bla bla bla bla bla bla bla"} name={"Ürün 1"} />
                <Urun not={"bla bla bla bla bla bla bla bla"} name={"Ürün X"} />
                <Urun not={"bla bla bla bla bla bla bla bla"} name={"Ürün X"} />
                <Urun not={"bla bla bla bla bla bla bla bla"} name={"Ürün X"} />
                <Urun not={"bla bla bla bla bla bla bla bla"} name={"Ürün X"} />
                <Urun not={"bla bla bla bla bla bla bla bla"} name={"Ürün X"} />
                {/* <Urun not={"bla bla bla bla bla bla bla bla"} name={"Ürün X"} />
                <Urun not={"bla bla bla bla bla bla bla bla"} name={"Ürün X"} />
                <Urun not={"bla bla bla bla bla bla bla bla"} name={"Ürün X"} /> */}
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export const Urun = props => {
  return (
    <>
      <Grid container>
        <Grid item xs={2} className="text-center">
          <LabelImportantIcon fontSize={"small"} className="urun-icon" htmlColor="gray" />
        </Grid>
        <Grid item xs={10}>
          <h3>{props.name}</h3>
          <small className="urun-small">{props.not}</small>
          {/* <h4>{props.not} <small className="urun-small">{props.not2}</small></h4> */}
        </Grid>
      </Grid>
      <Divider className="urun-divider" />
    </>
  );
};

export default UrunlerList;
