import React, { Component } from "react";
import {
  Grid,
  Card,
  CardContent,
  Divider
} from "@material-ui/core";
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';

class Sorumlular extends Component {
  render() {
    return (
      <div>
        <Grid container className="mi-card-root" spacing={2}>
          <Grid item xs={12}>
            <Card className="card sorumlu-card">
              <h1 className="sorumlu-title">Sorumlular</h1>
              <CardContent>
                <Sorumlu bolum={"Mali"} name={"Mehmet Mehmetoğlu"} />
                <Sorumlu bolum={"Teknik"} name={"Veli Velioğlu"} />
                <Sorumlu bolum={"Proje"} name={"Ahmet Ahmetoğlu"} />
                <Sorumlu bolum={"Sorumlu"} name={"Ali Alioğlu"} />
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export const Sorumlu = props => {
  return (
    <>
      <Grid container>
        <Grid item xs={3} style={{ textAlign: "center" }}>
          {/* <img src={logo1} alt="" style={{ borderRadius: "50%" }} /> */}
          <AccountCircleRoundedIcon fontSize={"large"} className="sorumlu-icon" htmlColor="gray"/>
        </Grid>
        <Grid item xs={9}>
          <h3>{props.name}</h3>
          <small className="sorumlu-small">{props.bolum} Sorumlu</small>
        </Grid>
      </Grid>
      <Divider className="sorumlu-divider" />
    </>
  );
};

export default Sorumlular;
