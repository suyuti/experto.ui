import React, { Component } from 'react';
import { musteriService } from '../../Services/musteri.service';
import Grid from '@material-ui/core/Grid';

import TopPane from "./MusteriPanels/TopPane";
import MusteriInfoCard from "./MusteriPanels/MusteriInfoCard";
import MusteriPersonelCard from "./MusteriPanels/MusteriPersonelCard";
import MusteriProjeCard from "./MusteriPanels/MusteriProjeCard";
import Sorumlular from "./MusteriPanels/Sorumlular";
import TodoList from "./MusteriPanels/TodoList";
import TimeLine from "./MusteriPanels/TimeLine";
import WarningList from "./MusteriPanels/WarningList";
import UrunlerList from "./MusteriPanels/UrunlerList";
import ProjectsTimeline from "./MusteriPanels/ProjectsTimeline";

import "./musteriDashboard.css";

export class MusteriDashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      musteri: [],
      personelSayisi: 0,
      projeler: [],
      ready: true
    }
  }

  // componentDidMount() {
  //   musteriService.getById(this.props.match.params.id).then( result => {
  //     this.setState({ 
  //       musteri: result.data[0]
  //     })
  //     musteriService.getPersoneller(this.state.musteri.id).then( personel => {
  //       this.setState({ personelSayisi: personel.data.length})
  //     });
  //     musteriService.getProjects(this.state.musteri.id).then( proje => {
  //       this.setState({ projeler: proje.data, ready: true})
  //     });
  //   window.scrollTo(0, 0);
  //   });
  // };

  render() {
    if (this.state.ready == true) {
      return (
        <div className={classes.root}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <TopPane />
            </Grid>
            <Grid item md={5} xs={12}>
              <MusteriInfoCard sirket={this.state.musteri} />
            </Grid>
            <Grid item md={7} xs={12}>
              <Grid container spacing={1}>
                <Grid item md={6} sm={6} xs={12}>
                  <MusteriProjeCard projeler={this.state.projeler} />
                </Grid>
                <Grid item md={6} sm={6} xs={12}>
                  <MusteriPersonelCard personelSayisi={this.state.personelSayisi} />
                </Grid>
                <Grid item md={6} xs={12}>
                  <UrunlerList />
                </Grid>
                <Grid item md={6} xs={12}>
                  <WarningList />
                </Grid>
              </Grid>
            </Grid>
            <Grid item md={5} xs={12}>
              <TodoList />
            </Grid>
            <Grid item md={7} xs={12}>
              <Grid container spacing={1}>
                <Grid item md={6} xs={12}>
                  <Sorumlular />
                </Grid>
                <Grid item md={6} xs={12}>
                  <TimeLine />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <ProjectsTimeline />
            </Grid>
          </Grid>
        </div>
      );
    }
    else {
      return (
        <h1> Circular Progress</h1>
      )
    }
  }
}

const classes = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }
});

export default MusteriDashboard;
