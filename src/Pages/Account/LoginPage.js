import React, { Component } from "react";
import { Formik, Field, Form } from "formik";
import  {authenticationService}  from "../../Services";
import * as Yup from "yup";
import { TextField } from "formik-material-ui";
import {  Avatar, Button, CircularProgress, Card, CardActions } from "@material-ui/core";
import LockIcon from "@material-ui/icons/Lock";
import { withStyles } from "@material-ui/core/styles";


const styles = theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        alignItems: 'center',
        justifyContent: 'flex-start',
        background: 'url(https://source.unsplash.com/collection/190727/1600x900)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    card: {
        minWidth: 300,
        marginTop: '6em',
    },
    avatar: {
        margin: '1em',
        display: 'flex',
        justifyContent: 'center',
    },
    icon: {
        backgroundColor: theme.palette.secondary.main,
    },
    hint: {
        marginTop: '1em',
        display: 'flex',
        justifyContent: 'center',
        color: theme.palette.grey[500],
    },
    form: {
        padding: '0 1em 1em 1em',
    },
    input: {
        marginTop: '1em',
    },
    actions: {
        padding: '0 1em 1em 1em',
    },
  });
class LoginPage extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (authenticationService === undefined) {
            alert('x')
        }
        if (authenticationService.currentUserValue) {
            this.props.history.push(
                authenticationService.currentUserValue.Dashboard
            );
        }
    }
    render() {
        const {classes} = this.props
        return (
            <div>
                <Formik
                    initialValues={{
                        username: "",
                        password: ""
                    }}
                    validationSchema={Yup.object().shape({
                        username: Yup.string().required("Username is required"),
                        password: Yup.string().required("Password is required")
                    })}
                    onSubmit={(
                        { username, password },
                        { setStatus, setSubmitting }
                    ) => {
                        setStatus();
                        authenticationService.login(username, password).then(
                            user => {
                                const { from } = this.props.location.state || {
                                    from: { pathname: "/" }
                                };
                                this.props.history.push(
                                    authenticationService.currentUserValue
                                        .Dashboard
                                ); 
                                //this.props.history.push(from);
                            },
                            error => {
                                setSubmitting(false);
                                setStatus(error);
                            }
                        );
                    }}
                    render={({ errors, status, touched, isSubmitting }) => (
                        <div className={classes.root}>
                            <Card className={classes.card}>
                                <div className={classes.avatar}>
                                    <Avatar className={classes.icon}>
                                        <LockIcon />
                                    </Avatar>
                                </div>
                                <Form>
                                    <div className={classes.form}>
                                        <div className={classes.input}>
                                            <Field name="username" type="text" label="Kullanici Adi" fullWidth component={TextField} />
                                        </div>
                                        <div className={classes.input}>
                                            <Field name="password" type="password" label="Şifre" fullWidth component={TextField} />
                                        </div>
                                    </div>
                                    <CardActions className={classes.actions}>
                                        <Button variant="contained" type="submit" color="primary" className={classes.button} fullWidth>
                                            {isSubmitting && (<CircularProgress size={25} thickness={2} />)}
                                            Login
                                        </Button>
                                    </CardActions>
                                </Form>
                            </Card>
                        </div>
                    )}
                />
            </div>
        );
    }
}

export default withStyles(styles)(LoginPage);
