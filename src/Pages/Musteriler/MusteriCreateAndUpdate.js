import React, { Component } from "react";
import MusteriCreate from "../../Components/Musteri/MusteriCreate";
import { Button, Tab, Tabs, Typography } from "@material-ui/core";
import { musteriService, personelService } from "../../Services";
import SwipeableViews from "react-swipeable-views";
import PropTypes from "prop-types";
import MaterialTable from "material-table";
import Paper from "@material-ui/core/Paper";

import ProjeListPage from "../../Pages/Proje/ProjeListPage";
import PersonelPage from "../../Pages/Personel/PersonelListPageTable";

import PersonelProfilList from "../../Pages/PersonelProfil/PersonelProfilListPageTable";
import EkipList from "../../Pages/Ekip/EkipListPageTable";
import { relative } from "path";
import {SnackbarAction} from '../../actions/snackbarAction';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

function TabContainer({ children, dir }) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {children}
        </Typography>
    );
}
TabContainer.propTypes = {
    children: PropTypes.node.isRequired
};


class MusteriCreateAndUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 0,
            MusteriFormu: {},
            isCreate: (this.props.match.params.id === undefined),
            isReady: (this.props.match.params.id ? false : true),
            personeller: [],
            projeler: []
        };
        this.onCreate = this.onCreate.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        this.musteriID = this.props.match.params.id;
        if (this.props.match.params.id !== undefined) {
            musteriService.getById(this.props.match.params.id)
                .then(result => this.setState({ MusteriFormu: result.data[0], isReady: true }))
            musteriService.getPersoneller(this.props.match.params.id)
                .then(result => {
                    if (result.status === 200) {
                        if (result.data != null) {
                            this.setState({ personeller: result.data })
                        }
                    }
                });
            musteriService.getProjects(this.props.match.params.id)
                .then(result => {
                    if (result.status === 200) {
                        if (result.data != null) {
                            this.setState({ projeler: result.data })
                        }
                    }
                });
        }
    }

    onTabChange(e, v) {
        this.setState({ selectedTab: v });
    }

    handleMusteriFormDataChanged = data => e => {
        let inputValue = e.target.value;
        this.setState(() => ({
            MusteriFormu: {
                ...this.state.MusteriFormu,
                [data]: inputValue
            }
        }));
    };

    validateInput = () => {
      if (this.state.MusteriFormu.FirmaAdi == "" || !this.state.MusteriFormu.FirmaAdi) {
        return this.props.SnackbarAction('open', "FirmaAdi kontrol ediniz");
      }
    }

    onCreate() {
      if(this.validateInput()) return;
      musteriService.create(this.state.MusteriFormu).then(resp => {
        if (resp.messages != "") {
          this.props.SnackbarAction('open', resp.messages);
        }
        if (resp.status === 200) {
          this.props.history.goBack();
        }
      })
    }
    onUpdate() {
      if(this.validateInput()) return;
      musteriService.update(this.state.MusteriFormu).then(resp => {
        if (resp.messages != "") {
          this.props.SnackbarAction('open', resp.messages);
        }
        if (resp.status === 200) {
          this.props.history.goBack();
        }
      })
    }

    render() {
        return (
            <div>
                {(this.state.isReady) &&
                    (<div>
                        <Tabs
                            value={this.state.selectedTab}
                            onChange={this.onTabChange}
                            indicatorColor="primary"
                            textColor="primary"
                            variant="scrollable"
                            scrollButtons="auto"
                        >
                            <Tab label="Musteri Bilgileri" />
                            {!this.state.isCreate && <Tab label="Iletisim Bilgileri" />}
                            {!this.state.isCreate && <Tab label="Mali Bilgiler" />}
                            {!this.state.isCreate && <Tab label="Projeler" />}
                            {!this.state.isCreate && <Tab label="Personeller" />}
                            {!this.state.isCreate && <Tab label="p. Profil " />}
                            {!this.state.isCreate && <Tab label="Ekipler " />}
                    </Tabs>

                        <SwipeableViews
                            axis="x"
                            index={this.state.selectedTab}
                            onChangeIndex={this.handleChangeIndex}
                        >
                            <TabContainer  >
                                <Paper style={{ padding: "20px 10px" }}>
                                    <MusteriCreate
                                        data={this.state.MusteriFormu}
                                        handleChange={this.handleMusteriFormDataChanged} />
                              
                                </Paper>

                            </TabContainer>
                            <TabContainer>
                                <div></div>
                            </TabContainer>
                            <TabContainer>
                                <div></div>
                            </TabContainer>
                            <TabContainer>
                                <div>
                                    <ProjeListPage
                                        musteriId={this.musteriID}
                                        history={this.props.history} />
                                </div>
                            </TabContainer>
                            <TabContainer>
                                <div>
                                    <PersonelPage
                                        musteriId={this.musteriID}
                                        history={this.props.history} />


                                </div>
                            </TabContainer>
                            <TabContainer>
                                <div>
                                    <PersonelProfilList
                                        musteriId={this.musteriID}
                                        history={this.props.history} />


                                </div>
                            </TabContainer>
                            <TabContainer>
                                <div>
                                    <EkipList
                                        musteriId={this.musteriID}
                                        history={this.props.history} />
                                </div>
                            </TabContainer>
                        </SwipeableViews>


                        <Button variant="contained" color="primary"
                            onClick={this.state.isCreate ? this.onCreate : this.onUpdate}
                        >{this.state.isCreate ? "Yeni Olustur" : "Guncelle"}</Button>
                    </div>)}
            </div>
        );
    }
}

function mapStateToProps(state){
  return {
      snackBarReducer: state.snackBarReducer
  }
} 
function mapDispatchToProps(dispatch){
  return bindActionCreators({
      SnackbarAction: SnackbarAction
  },dispatch)
} 
export default connect(mapStateToProps, mapDispatchToProps)(MusteriCreateAndUpdate);