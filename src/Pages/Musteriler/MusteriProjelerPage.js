import React, { Component } from "react";
import { musteriService, authenticationService } from "../../Services";
import Fab from "@material-ui/core/Fab";
import { Role } from "../../Helpers";
import { withStyles, Paper, Grid } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import MaterialTable from "material-table";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { Link } from "react-router-dom"; 
import {checkPermission,PermissionData } from '../../command';


const styles = theme => ({
    fab: {
        margin: theme.spacing(1),
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    }
});

const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));
var self;
class MusteriProjelerPage extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            role: null,
            musteri: null,
            projeler: null
        };
    }
    componentDidMount() {
        authenticationService.currentUser.subscribe(x =>
            this.setState({
                currentUser: x,
                //role: x.role
            })
        );
        musteriService.getById(this.props.match.params.id).then(m => {
            this.setState({ musteri: m });
            musteriService.getProjects(m)
                .then(p => this.setState({ projeler: p }));
        });
    }

    render() {
        const { classes, ...props } = this.props;
        return (
            <div>
                <Paper>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <div variant="h5" gutterBottom>
                                {this.state.musteri &&
                                    this.state.musteri.FirmaAdi}{" "}
                                Projeler
                            </div>
                        </Grid>
                        <Grid item xs={12}>
                            {this.state.musteri && (
                                <Table className={classes.table}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Proje Adı</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {this.state.projeler &&
                                            this.state.projeler.map(row => (
                                                <TableRow key={row.ProjeAdi}>
                                                    <TableCell
                                                        component="th"
                                                        scope="row"
                                                    >
                                                        {row.ProjeAdi}
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                    </TableBody>
                                </Table>
                            )}
                        </Grid>
                    </Grid>
                </Paper>
                {checkPermission([PermissionData.postMusteri])?
                    <Fab
                        color="primary"
                        aria-label="add"
                        className={classes.fab}
                        component={AdapterLink}
                        to={"/musteri/" + this.props.match.params.id + "/proje/new"}
                    >
                        <AddIcon />
                    </Fab>
                    : ' '}
            </div>
        );
    }
}

export default withStyles(styles)(MusteriProjelerPage);
