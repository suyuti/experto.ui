import React, {Component} from 'react'
import MaterialTable from 'material-table';
import {musteriService} from '../../Services/musteri.service'
import MusteriDetailPanel from '../../Components/Musteri/MusteriDetailPanel';
import Fab from "@material-ui/core/Fab";
import { Link } from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";
import {CircularProgress} from '@material-ui/core'; 

const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));
class MusteriPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            musteriler:[],
            ready : false
        }
    }

    componentDidMount() {
        musteriService.getAll().then(result => this.setState({musteriler:result.data, ready:true}))
    }

    updateRow(rowData) {
        this.props.history.push('/musteri/' + rowData.id);
    }
    removeRow(rowData) {
      let confirmMessage = window.confirm(`"${rowData.FirmaAdi}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        // musteriService.remove(rowData).then(resp => {
        //   if (resp.status === 200) {
        //     musteriService.getAll().then(result => {
        //       this.setState({ musteriler: result.data })
        //     })
        //   }
        // });
        musteriService.remove(rowData).then(resp => {
          if (resp.status === 200) {
            this.setState({
              ...this.state,
              musteriler: this.state.musteriler.filter(musteri => {
                return musteri.id !== rowData.id
              })
            });
          }
        });
      }
    }

    render() {
        if (this.state.ready === false) {
            return <CircularProgress />
        }
        return(
            <div>
                <MaterialTable 
                    title ={"Firmalar"}
                    columns ={[
                        //{title:'ID', field:'id'},
                        {title:'Firma Adi', field:'FirmaAdi'},
                        {title:'Sektörü', field:'SektorAdi'},
                        {title:'Bölgesi', field:'BolgeAdi'}
                    ]}
                    data= {this.state.musteriler}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        emptyRowsWhenPaging: false,
                        pageSize: 20,
                        pageSizeOptions: [20, 50, 100]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Musteri Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Musteri sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })
                    ]}
                    detailPanel={rowData => { return(<MusteriDetailPanel musteri={rowData} />)}}
                />
                    {true && (
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/musteri/create"
                    >
                        <AddIcon />
                    </Fab>
                )}

            </div>
        )
    }
}
const styles = {
    fab: {
        margin: 1,
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    },};

export default MusteriPage;