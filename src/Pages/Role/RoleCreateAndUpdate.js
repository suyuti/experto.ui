import React, { Component } from "react";
import { Paper, Button } from "@material-ui/core";
import { roleService } from "../../Services";
import Snackbar from '../../Components/Snackbar'
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';



var self;
class CreateAndUpdate extends Component {

    constructor(props) {
        super(props);
        self = this;
        this.state = {
            buttonEnable: true,
            message: "sadasd",
            value: 0,
            RoleFormu: {},
            PermissionList:[],
            rolePermission:[], 

        };

        this.RolesId = this.props.match.params.id; 

        
        
            roleService.getAllPermissionList().then(result => {
                console.log(result)
                this.setState({ PermissionList: result.data })   
            });

        if(this.RolesId ) { 
            roleService.getById(this.RolesId ).then(result => {
                console.log(result) 
                this.setState({ RoleFormu: result.data[0] })  
            });
            roleService.getPermissionByRoleId(this.RolesId ).then(result => {
                console.log(result)
                this.setState({ rolePermission: result.data })  
            }); 
        }   
            



    }

  

    handleChange = (input) => e => {
        console.log(e.target.value, input);
        let inputValue = e.target.value;
        this.setState(() => ({
            RoleFormu: {
                ...this.state.RoleFormu,
                [input]: inputValue
            }
        }));
    };

    createButton = (e) => {
        this.setState({ buttonEnable: false });
        // e.preventDefault();
        var data={
            RoleFormu:this.state.RoleFormu,
            rolePermission:this.state.rolePermission
        }
        roleService.create(data).then(result => {
            console.log(result.status)
            if (result.status === 200) {
                this.setState({ buttonEnable: true });
                this.props.history.goBack();
            }
        });
    }

    updateButton = (e) => {
        this.setState({ buttonEnable: false });
        var data={
            RoleFormu:this.state.RoleFormu,
            rolePermission:this.state.rolePermission
        }

        roleService.update(data).then(result => {
            if (result.status === 200) {
                this.setState({ buttonEnable: true });
                this.props.history.goBack();
            }
        });
    }

    _checkRole =(role)=>{  
        const index = this.state.rolePermission.findIndex(item => item.id === role.id);
        if(index>-1) {
            return true
        }else{
            return false
        }
    } 
    _handleCheckboxChange = (Permission) => {
        const index = this.state.rolePermission.findIndex(item => item.id === Permission.id);
        if(index>-1) {
            const newArray =this.state.rolePermission.filter( item =>  item.id !== Permission.id  )
            this.setState({ 
                rolePermission:newArray
            })  
           // this.props.setRole(newArray); //usteti formdaki UsersPermission alanını  günceller
        }else{
            this.setState({ 
                rolePermission:[
                    ...this.state.rolePermission,
                    Permission
                 ]
            }) 
           //this.props.setRole([ ...this.state.userRole,Permission]); //usteti formdaki UsersPermission alanını  günceller
        }  

    }


    render() { 

        let button;
        if (this.state.buttonEnable) {
            if (this.RolesId) {
                button = <Button
                    variant="contained"
                    color="default"
                    onClick={this.updateButton}
                    style={styles.save_button}>UPDATE</Button>
            } else {
                button = <Button
                    variant="contained"
                    color="primary"
                    onClick={this.createButton}
                    style={styles.save_button}>KAYDET</Button>
            }

        } else {
            button = <Button
                variant="contained"
                color="primary"
                style={styles.save_button}>...</Button>

        }


        if (this.state.PermissionList.length == 0 ) {
            return (<div style={{ textAlign: "center" }}><CircularProgress /> </div>)
        } 
        return (
            <div>
                <Snackbar message={this.state.message} />

                <Paper>

                    <Grid container  style={{margin:8}}>
                        <Grid item xs={12} sm={12} >

                            <TextField
                                id="standard-name"
                                label="Rol Adi"
                                className=''
                                value={this.state.RoleFormu.RoleName}
                                onChange={this.handleChange('RoleName')}
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>


                        {this.state.PermissionList.map((item, i) => {
                            return (
                                <Grid item xs={12} md={4} >
                                <Tooltip title={`${item.Path }`} placement="top">

                                    <FormControlLabel key={i}
                                        control={
                                            <Checkbox
                                                checked={this._checkRole(item)}
                                                onChange={e => this._handleCheckboxChange(item)}
                                                value="checkedB"
                                                color="primary"
                                            />
                                        }
                                        label={`${item.Aciklama }   ` }
                                    />
                                    </Tooltip>
                                </Grid>
                            )
                        })}

                        <Grid item xs={12} sm={12} >
                            {button}
                        </Grid>
                    </Grid>
                </Paper>



            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '0',
        left: '20px'
    }
};

export default CreateAndUpdate;
