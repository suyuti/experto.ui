import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add"; 
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import {   musteriService ,roleService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';
import CircularProgress from '@material-ui/core/CircularProgress';  


const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));


class PersonelListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //role: null,
            rows: [],
            RowStatus: 1,//1 normal liste 2 ekleme 3 düzenleme,
            Ready:false,
        };
    }



    componentDidMount() { 
       this.fetchData();
    }
    fetchData = () => {
        
        roleService.getAll()
                .then(result => {
                    if (result.status === 200) {
                        if (result.data != null) {
                            this.setState({ rows: result.data, Ready: true })
                        }
                    }
                });
       
    }

    updateRow = (Row) => { 

        this.props.history.push('/role/update/' + Row.id);
        //alert("You saved " + rowData.id)
    }
 

    removeRow = (Row) => {
      let confirmMessage = window.confirm(`"${Row.RoleName}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        this.setState({ buttonEnable: false });
        roleService.remove(Row).then(result => {
          if (result.status = 200) {
            this.fetchData();
          }
        });
      }
    }

 




    render() {
        const { classes } = this.props;

        if (this.state.Ready === false) {
            return (<div style={{textAlign:"center"}}><CircularProgress /> </div>) 
        } 
   var PersonelProfilCreate=this.props.musteriId ? "/musteri/"+  this.props.musteriId +"/role/create":`/role/create`
       
     var createbutton =  <Fab 
                color="primary" 
                aria-label="add" 
                size='small' 
                style={{ marginLeft: 8 }} 
                component={AdapterLink} 
                to={PersonelProfilCreate}
                >
                    <AddIcon />
                </Fab>
       
       return (
            <div>
                <MaterialTable
                    title=
                    {<div>{`Role (${this.state.rows.length}) `}
                   { checkPermission([PermissionData.postPersonelProfil]) ?createbutton:""}
 
                    </div>
                    }
                     
                    columns={[
                        {
                            title: 'id',
                            field: 'id',
                            cellStyle: {
                                //backgroundColor: '#039be5',
                                //color: '#FFF'
                            },
                            headerStyle: {
                                //backgroundColor: '#039be5',
                            }
                        },
                        { title: 'Rol Adi', field: 'RoleName' },
                       


                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize:10,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions: [10, 20]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Rol Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Rol sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })


                    ]}
                /> 
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};


export default withStyles(styles)(PersonelListAndSearch);
