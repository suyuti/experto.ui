import React, {Component} from 'react'
import { Grid, Paper, List, ListItemIcon, Checkbox, ListItem, ListItemText, Button } from '@material-ui/core';
import { musteriService } from '../../Services';
import AdamAyTimeLine from "../../Components/AdamAy/AdamAyTimeLineIc"
import CircularProgress from '@material-ui/core/CircularProgress';
import SwipeableViews from "react-swipeable-views";
import AdamAyHesaplamaForm from "../../Components/AdamAy/AdamAyHesaplamaForm" 

export default class AdamAyHesaplamaPage extends Component {
    state = {
        pageIndex:0,
        projeler : [],
        personeller : [],
        checked: [],
        adamAyForm : {
            seciliProjeler:[],
            seciliPersoneller:[]
        },
        musteri:null
    }

    componentDidMount() {
        musteriService.getById(this.props.match.params.id).then(result => this.setState({musteri:result.data[0]}));
        //musteriService.getProjects(this.props.match.params.id).then(result => this.setState({projeler:result.data}));
        //musteriService.getPersoneller(this.props.match.params.id).then(result => this.setState({personeller:result.data}));
    }

    handleToggle = value => () => {
        const currentIndex = this.state.checked.indexOf(value);
        const newChecked = [...this.state.checked];
    
        if (currentIndex === -1) {
          newChecked.push(value);
        } else {
          newChecked.splice(currentIndex, 1);
        }
    
        this.setState({checked:newChecked});
      };

      onHesapla = () => {
          if (this.state.adamAyForm.seciliProjeler.length === 0 ||
                this.state.adamAyForm.seciliPersoneller.length === 0) {
                    alert("Proje ve Personel seçmelisiniz")
                    return
                }
        musteriService.adamAyHesapla(this.props.match.params.id, 
                                    this.state.adamAyForm.seciliProjeler, 
                                    this.state.adamAyForm.seciliPersoneller)
                            .then(result => {
                                console.log(result)
                                    this.setState({pageIndex:1})
        })
        }
        onGeri = () => {
            this.setState({pageIndex:0})
        }

    handleAdamAyFormDataChanged = data => e => {
        let inputValue = e.target.value;
        if (data === 'seciliProjeler') {
            if (e.target.checked) {
                inputValue = this.state.adamAyForm.seciliProjeler
                inputValue.push(e.target.id)
            }
            else {
                inputValue = this.state.adamAyForm.seciliProjeler.filter(v => {return v !== e.target.id})
            }
        }
        else if (data === 'seciliPersoneller') {
            if (e.target.checked) {
                inputValue = this.state.adamAyForm.seciliPersoneller
                inputValue.push(e.target.id)
            }
            else {
                inputValue = this.state.adamAyForm.seciliPersoneller.filter(v => {return v !== e.target.id})
            }
        }
        this.setState(() => ({
            adamAyForm: {
                ...this.state.adamAyForm,
                [data]: inputValue
            }
        }));
    };


    render(){
        return(
            <div>
                <Grid container spacing={1}>
                    <Grid item xs={12}>
                        <h3>Adam Ay Hesaplama1</h3>
                    </Grid>
                    <Grid item xs={12}>
                        {this.state.musteri ? 
                        <AdamAyTimeLine musteri={this.state.musteri} />:<CircularProgress />
                        }
                    </Grid>
                    {false && <Grid item xs={12} container spacing={3}>
                    <Grid item xs = {6}>
                            <Paper>
                                <h4>Projeler</h4>
                                <List>
                                    {this.state.projeler.map(p => {
                                        const labelId = `checkbox-list-label-${p.id}`
                                        return (
                                            <ListItem key={p.id} dense button onClick={this.handleToggle(p)}>
                                                <ListItemIcon>
                                                    <Checkbox 
                                                        edge="start"
                                                        checked={this.state.checked.indexOf(p) !== -1}
                                                        tabIndex = {-1}
                                                        disableRipple
                                                        inputProps={{'aria-labelledby':labelId}} 
                                                    />
                                                </ListItemIcon>
                                                <ListItemText id={labelId} primary={p.ProjeAdi} />
                                            </ListItem>
                                        )
                                    })}
                                </List>
                            </Paper>
                        </Grid>
                        <Grid item xs = {6}>
                            <Paper>
                                <h4>Personeller</h4>
                                <List>
                                    {this.state.personeller.map(p => {
                                        const labelId = `checkbox-list-label-${p.id}`
                                        return (
                                            <ListItem key={p.id} dense button onClick={this.handleToggle(p)}>
                                                <ListItemIcon>
                                                    <Checkbox 
                                                        edge="start"
                                                        checked={this.state.checked.indexOf(p) !== -1}
                                                        tabIndex = {-1}
                                                        disableRipple
                                                        inputProps={{'aria-labelledby':labelId}} 
                                                    />
                                                </ListItemIcon>
                                                <ListItemText id={labelId} primary={p.Adi} />
                                            </ListItem>
                                        )
                                    })}
                                </List>
                            </Paper>
                        </Grid>
                        <Grid item xs={10}></Grid>
                        <Grid item xs={2}>
                            <Button variant="contained" color="primary">
                                Devam
                            </Button>
                        </Grid>
                    </Grid>}
                </Grid>
            </div>

        )
    }
}