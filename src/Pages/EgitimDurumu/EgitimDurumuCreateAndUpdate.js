import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views"; 
import { Paper, Button } from "@material-ui/core"; 
import { Formik, Form } from "formik";
import { egitimDurumuService } from "../../Services";
import EgitimDurumuFormuformu from '../../Components/EgitimDurumu/EgitimDurumuFormu'   
//import { FormControl } from '@material-ui/core';
import Snackbar from '../../Components/Snackbar';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {SnackbarAction} from '../../actions/snackbarAction';


 
var self;
class CreateAndUpdate extends Component {



    constructor(props) {
        super(props);
        self = this;
        this.state = {
            buttonEnable: true,
            message: "sadasd",
            value: 0,
            EgitimDurumu: {
            }

        };

        this.paramsId = this.props.match.params.id;
        console.log("ParamsId", this.paramsId)
        if (this.paramsId) {
            egitimDurumuService.getById(this.paramsId).then(data => {
                console.log(data)
                this.setState({ EgitimDurumu: data.data[0] })

                /*this.setState(() => ({
                    EgitimDurumu: {
                        id:data.data[0].id,
                        EgitimDurumAdi: data.data[0].EgitimDurumAdi
                    }
                }));*/

            });
        }


    }

    handleTabChange(e, v) {
        self.setState({ value: v });
        console.log("handleTabChange");
    }
    handleSwipeViewChange(i) {
        self.setState({ value: i });
        console.log("handleChangeIndex");
    }

    handleChange = (input) => e => {
        // console.log(e.target.value, input);
        let inputValue = e.target.value;
        this.setState(() => ({
            EgitimDurumu: {
                ...this.state.EgitimDurumu,
                [input]: inputValue
            }
        }));
    };

    validateInput = () => {
      if (!this.state.EgitimDurumu.EgitimDurumAdi || this.state.EgitimDurumu.EgitimDurumAdi == " ") {
        return this.props.SnackbarAction('open', "Lütfen EgitimDurumAdi alanını giriniz");
      }
    }

    createButton = (e) => {
        if(this.validateInput()) return;
        this.setState({buttonEnable:false}); 
        // e.preventDefault();
        egitimDurumuService.create(this.state.EgitimDurumu).then(data => {
          if (data.messages != "") {
            this.props.SnackbarAction('open', data.messages);
          }
            if (data.status === 200) {
                this.setState({buttonEnable:true});
                this.props.history.goBack();
            }
        });         
    }

    updateButton = (e) => {
        if(this.validateInput()) return;
        this.setState({ buttonEnable: false });
        egitimDurumuService.update(this.state.EgitimDurumu).then(data => {
          if (data.messages != "") {
            this.props.SnackbarAction('open', data.messages);
          }
            if (data.status === 200) {
                this.setState({ buttonEnable: true });
                this.props.history.goBack();
            }
        });
    }

    render() {
        const { classes, ...props } = this.props;
        let button;
        if (this.state.buttonEnable) {
            if (this.paramsId) {
                button = <Button
                    variant="contained"
                    color="default"
                    onClick={this.updateButton}
                    style={styles.save_button}>UPDATE</Button>
            } else {
                button = <Button
                    variant="contained"
                    color="primary"
                    onClick={this.createButton}
                    style={styles.save_button}>KAYDET</Button>
            }

        } else {
            button = <Button
                variant="contained"
                color="primary" 
                style={styles.save_button}>...</Button>

        }


        if (this.state.result) {
            
        }
        

        return (
            <div> 
                <Paper style={styles.padding20}>  
                                    <Tabs
                                        value={this.state.value}
                                        onChange={this.handleTabChange}
                                        indicatorColor="primary"
                                        textColor="primary"
                                        variant="fullWidth"
                                    >
                                        <Tab label='Egitim Durumuları' />
                                    </Tabs>
                                    <SwipeableViews 
                                        
                                        axis="x"
                                        index={this.state.value}
                                        onChangeIndex={this.handleSwipeViewChange}>
                                        <div>
                                           
                                            <EgitimDurumuFormuformu
                                                handleChange={this.handleChange}
                                                data={this.state.EgitimDurumu} />
                                        </div>
                                    </SwipeableViews>
                                    <br />

                                    {button}  
                </Paper>



            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '20px',
        left: '20px'
    },padding20:{ 
        padding: "20px" 

    }

};

function mapStateToProps(state){
  return {
      snackBarReducer: state.snackBarReducer
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
      SnackbarAction: SnackbarAction
  },dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAndUpdate);
