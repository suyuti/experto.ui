import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add"; 
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import {  egitimDurumuService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';


const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));


class PersonelListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //role: null,
            rows: [],
            RowStatus: 1//1 normal liste 2 ekleme 3 düzenleme
        };  
    }

    componentDidMount() {
       /* authenticationService.currentUser.subscribe(x =>
            this.setState({
                currentUser: x,
                //role: x.role
            })
        );*/
        /*this.setState({rows:[
            { Adi: 'Mehmet', Soyadi: 'Baran', EgitimDurumu: 'teknik lise' , ProfilAdi:'xx' },
            { Adi: 'Zerya Betül', Soyadi: 'Baran', EgitimDurumu: 'üni' , ProfilAdi: 'xx' },
          ]})*/
        egitimDurumuService.getAll().then(result => { 
          this.setState({ rows: result.data })  
        });
    }

    updateRow = (Row) => {
        // alert("updateRow  =" + Row.id);
        this.props.history.push('/egitimDurumu/update/' + Row.id);
        //alert("You saved " + rowData.id)
    }

  //   removeRow = (Row) => {
  //       // console.log('remove  button');
  //       // alert("deleteRow  =" + Row.id);
  //   let confirmMessage = window.confirm(`"${Row.EgitimDurumAdi}" silinecektir. Emin misiniz?`);
  //   if (confirmMessage == true) {
  //     this.setState({ buttonEnable: false });
  //     egitimDurumuService.remove(Row).then(result => {
  //       console.log(result.status)
  //       if (result.status = 200) {
  //         this.setState({ buttonEnable: true });
  //         egitimDurumuService.getAll().then(result => this.setState({ rows: result.data }));
  //       }
  //     });
  //   }
  // }



    render() {
        const { classes } = this.props;
        return (
            <div>
                <MaterialTable
                    title={"Egitim Durumu Listesi (" + this.state.rows.length +")"}
                    columns={[
                        {
                            title: 'id',
                            field: 'id',
                            cellStyle: {
                                //backgroundColor: '#039be5',
                                //color: '#FFF'
                            },
                            headerStyle: {
                                //backgroundColor: '#039be5',
                            }
                        },
                        { title: 'Egitim Durum Adi', field: 'EgitimDurumAdi' },
                       


                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize:15,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions: [5, 10, 15]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Egitim Durumu Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        // rowData => ({
                        //     icon: 'delete',
                        //     tooltip: 'Egitim Durumunu sil',
                        //     onClick: (event, rowData) => { this.removeRow(rowData) }
                        // })


                    ]}
                />
                {checkPermission([PermissionData.postEgitimDurumu]) ? 
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/egitimDurumu/create"
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                }
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};


export default withStyles(styles)(PersonelListAndSearch);
