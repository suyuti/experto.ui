import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { SnackbarAction } from '../../actions/snackbarAction';
import FileBase64 from 'react-file-base64';
import { pdksService, musteriService } from "../../Services"
import { Grid, MenuItem, Button, TextField } from "@material-ui/core";
import CircularProgress from '@material-ui/core/CircularProgress';
import { ExportCSV } from '../../Components/ExportCSV'
import PdksListAndUpdate from "../../Components/Pdks/PdksListAndUpdate";
import PdksSonuc from '../../Compone../../Components/Pdks/PdksSonuc';
import '../../App.css';
import command from '../../command';
import moment from 'moment';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';



// import {DropzoneDialog} from 'material-ui-dropzone'
// <DropzoneDialog
//                     open={this.state.open}
//                     onSave={this._handleSave}
//                     acceptedFiles={['image/jpeg', 'image/png', 'image/bmp']}
//                     showPreviews={true}
//                     maxFileSize={5000000}
//                     onClose={this._handleClose}
//                 />

class PdksImportPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            file_loaded: true,
            files: [],
            Musteri: [],
            excelData: [],
            data: [],
            openPopupModel: false,
            personelList: [],
            array: [],
            errorPersonel: {
                pdksVarDbYok: [],
                dbVarPdksYok: [],
                eslesmis: []
            },
            popupData: {},
            popupValue: "ismail",
            ModelSelectType: null,
            showHidePdksListAndUpdate: true,
            hesaplananPDKS: [],
            typeList: [
                { id: 0, name: "NORMAL ÇALIŞMA  " },
                { id: 18, name: "Dış Görev" },
                { id: 1, name: "HAFTA TATİLİ   " },
                { id: 2, name: "GENEL TATİLLER   " },
                { id: 3, name: "YILLIK ÜCRETLİ İZİNLER  " },
                { id: 4, name: "ÜCRET FARKI " },
                { id: 5, name: " PRİM - İKRAMİYE  " },
                { id: 6, name: "BAYRAM - YEMEK - KİRA " },
                { id: 7, name: "İZİN HARÇLIĞI" },
                { id: 8, name: "KIDEM ZAMMI " },
                { id: 9, name: "ÇOCUK ZAMMI " },
                { id: 10, name: "YAKACAK YARDIMI " },
                { id: 11, name: "FAZLA MESAİ ÜCRETİ" },
                { id: 12, name: "DOĞUM İZNİ " },
                { id: 13, name: "SÜT İZNİ   " },
                { id: 14, name: "EVLİLİK İZNİ  " },
                { id: 15, name: "ÖLÜM İZNİ " },
                { id: 16, name: "MAZERET İZNİ  " },
                { id: 17, name: "RAPORLU OLAN SÜRELER" }
            ]

        }

    }





    componentWillMount() {
        musteriService.getAll().then(result => { this.setState({ Musteri: result.data }); });

    }

    componentDidMount() {
    }



    _handleCreate = () => {
        this.setState({
            data: [],
            array: [],
        });
        var data = {
            files: this.state.files,
            MusteriId: this.state.MusteriId
        }
        pdksService.checkPdks(data).then(result => {

            if (result.status == 200) {
                this.setState({
                    errorPersonel: {
                        pdksVarDbYok: [],
                        dbVarPdksYok: [],
                        eslesmis: []
                    }
                });
                this.setState({ data: result.data, personelList: [...new Set(result.data.map(item => item.SadelestirilmisAdSoyad))] });

                this._renderResultRowsHeaderMounth2();
                this._renderResultRowsContentMounth2();
                this.setState({ showHidePdksListAndUpdate: true })
            } else {
                this.props.SnackbarAction('open', result.messages);

                if (result.data.pdksVarDbYok !== undefined || result.data.dbVarPdksYok !== undefined) {
                    this.setState({
                        errorPersonel: {
                            pdksVarDbYok: result.data.pdksVarDbYok,
                            dbVarPdksYok: result.data.dbVarPdksYok,
                            eslesmis: result.data.eslesmis
                        }
                    });
                    //this.setState({ errorPersonel: result.data });    
                }
            }
        });

        // this.setState({
        //     data:  [{"Aciklama":"13 temmuz yıllık izin ","AdSoyad":"Demet  DARCAN CİNKAYA ","SadelestirilmisAdSoyad":"demetdarcancinkaya","Tarih":"2019-07-01T00:00:00.000Z","Giris":"7:53","Cikis":"18:03","Type":0},
        //     {"Aciklama":"13 temmuz yıllık izin ","AdSoyad":"Demet  DARCAN CİNKAYA ","SadelestirilmisAdSoyad":"demetdarcancinkaya","Tarih":"2019-07-02T00:00:00.000Z","Giris":"7:51","Cikis":"18:04","Type":0},
        //     {"Aciklama":"13 temmuz yıllık izin ","AdSoyad":"Demet  DARCAN CİNKAYA ","SadelestirilmisAdSoyad":"demetdarcancinkaya","Tarih":"2019-07-03T00:00:00.000Z","Giris":"7:50","Cikis":"18:03","Type":0}] 
        // });

        // pdksService.create(data).then(result => {
        //     this.setState({ data: result.data })

        //     var tempExcelData = result.data.map((item) => {
        //         var temp = {
        //             'PERSONEL': item.AdSoyad,
        //             'ÇALIŞMA': item.Sure,
        //             'HAFTA TATİLİ': item.cumartesiHakki,
        //             'RESMİ TATİL': item.ResmiTatil,
        //             'YILLIK İZİN': item.YillikIzin,
        //             'DIŞ GÖREV': item.DisGorev,
        //             'AR-GE GÜNÜ': item.CalistigiGun 
        //         }

        //         return temp;
        //     })
        //     this.setState({ excelData: tempExcelData })
        //     console.log(tempExcelData)
        //     console.log(result)
        // });
    }

    _handleOpen = () => {
        var fileUpload = document.getElementById("file_upload").getElementsByTagName("input")[0];
        //fileUpload.setAttribute("accept","image/png, image/jpeg");
        fileUpload.setAttribute("accept", ".xlsx, .xls,.png");

        fileUpload.click();
        this.setState({
            file_loaded: false,
        });
    }
    _getFiles = (files) => {
        console.log(files)
        this.setState({ showHidePdksListAndUpdate: false })
        this.setState({ data: [] })
        this.setState({ array: [] })


        this.setState({
            files: files,
            file_loaded: true
        })
    }

    _handleChange = (input) => e => {
        let inputValue = e.target.value;

        this.setState(() => ({
            [input]: inputValue
        }));
    };


    _ModelHandleChange = (input) => e => {
        let inputValue = e.target.value;

        this.setState(() => ({
            [input]: inputValue
        }));
    };

    _openModel = (rowIndex, elementIndex) => {
        var popupData = {
            rowIndex: rowIndex,
            elementIndex: elementIndex
        }


        this.setState({ ModelSelectType: null })
        this.setState({ popupData: popupData })
        this.setState({ openPopupModel: true })
    }

    _closeModel = () => {
        this.setState({
            errorPersonel: {
                pdksVarDbYok: [],
                dbVarPdksYok: [],
                eslesmis: []
            }
        });
        this.setState({ openPopupModel: false })
        this.setState({ popupData: {} })
    }

    _saveModel = () => {
        this.setState({ openPopupModel: false })
        this.setState({ popupData: {} })


        this.state.array[this.state.popupData.rowIndex][this.state.popupData.elementIndex] = this.state.ModelSelectType;



        var date = this.state.array[this.state.popupData.rowIndex][0]
        date = moment(date, "DD.MM.YYYY").format("DD.MM.YYYY");
        var findIndex = this.state.data.findIndex((item) => {
            return item.SadelestirilmisAdSoyad == this.state.array[0][this.state.popupData.elementIndex][1]
                && moment(item.Tarih).format("DD.MM.YYYY") == date
        })
        if (findIndex > -1) {
            // this.state.data[findIndex]
            this.state.data[findIndex].Type = this.state.ModelSelectType
            console.log(this.state.data[findIndex])
        } else {
            var finditem = this.state.data.filter((item) => {
                return item.SadelestirilmisAdSoyad == this.state.array[0][this.state.popupData.elementIndex][1]
            })

            var pushData = {
                Aciklama: "13 temmuz yıllık izin ",
                AdSoyad: finditem[0].AdSoyad,
                Cikis: "00:00",
                Giris: "00:00",
                Tarih: `${this.state.array[this.state.popupData.rowIndex][0]}    `,
                Type: this.state.ModelSelectType,
                SadelestirilmisAdSoyad: finditem[0].SadelestirilmisAdSoyad
            }
            this.state.data.push(pushData);
            console.log("Eklenem Data")
            console.log(pushData)

        }

    }

    _getName(SadelestirilmisAdSoyad) {
        var findItem = this.state.data.filter((item) => {
            return item.SadelestirilmisAdSoyad == SadelestirilmisAdSoyad
        })
        return findItem[0].AdSoyad
    }
    _getSaat(SadelestirilmisAdSoyad, gun, ay, yil) {
        if (gun == 4) {
            console.log("degug")
        }
        var date = moment(`${gun}.${ay}.${yil}`, "DD.MM.YYYY").format("DD.MM.YYYY");
        var findItem = this.state.data.filter((item) => {
            var dateItem = moment(item.Tarih).format("DD.MM.YYYY");
            //console.log(dateItem==date,dateItem,date );
            if (item.SadelestirilmisAdSoyad == SadelestirilmisAdSoyad && dateItem == date) {
                return true
            }
        })
        if (findItem.length > 0) {///bulunduysa
            if (findItem[0].Type == 0) {
                var toplam = "00:00";
                for (let x = 0; x < findItem.length; x++) {
                    toplam = command.gunSaatTopla(command.gunSaatCıkart(findItem[x].Giris, findItem[x].Cikis), toplam)
                }

                return toplam
            } else {
                return findItem[0].Type
            }
        } else {
            return ""
        }

    }
    // _renderResultRowsHeaderMounth() {
    //     var tempAy = new Date(this.state.data[0].Tarih);
    //     var ayinGunSayisi = tempAy.monthDays();
    //     this.state.array[0] = new Array();
    //     this.state.array[0][0] = new Array();
    //     this.state.array[0][0][0] = "PERSONEL";
    //     for (let index = 1; index <= ayinGunSayisi; index++) {
    //         this.state.array[0][index] = `${index}.${tempAy.getMonth() + 1}.${tempAy.getFullYear()}`
    //     }
    // }

    // _renderResultRowsContentMounth() {
    //     var tempFirstElementDate = new Date(this.state.data[0].Tarih);
    //     var tempAy = tempFirstElementDate.getMonth() + 1;
    //     var tempYil = tempFirstElementDate.getFullYear()
    //     var ayinGunSayisi = tempFirstElementDate.monthDays();
    //     for (let indexPersonel = 0; indexPersonel < this.state.personelList.length; indexPersonel++) {
    //         const PersonelSadelestirilmisAdSoyad = this.state.personelList[indexPersonel];

    //         this.state.array[indexPersonel + 1] = new Array();
    //         this.state.array[indexPersonel + 1][0] = new Array();
    //         this.state.array[indexPersonel + 1][0][0] = this._getName(PersonelSadelestirilmisAdSoyad);
    //         this.state.array[indexPersonel + 1][0][1] = PersonelSadelestirilmisAdSoyad
    //         for (let indexGun = 1; indexGun <= ayinGunSayisi; indexGun++) {
    //             this.state.array[indexPersonel + 1][indexGun] = this._getSaat(PersonelSadelestirilmisAdSoyad, indexGun, tempAy, tempYil)
    //         }
    //     }
    //     console.log(this.state.array);
    //     this.setState({});
    // }

    _renderResultRowsHeaderMounth2() {
        var tempAy = new Date(this.state.data[0].Tarih);
        var ayinGunSayisi = tempAy.monthDays();
        this.state.array[0] = new Array();
        this.state.array[0][0] = new Array();
        this.state.array[0][0][0] = "PERSONEL";
        for (let index = 0; index < this.state.personelList.length; index++) {
            const PersonelSadelestirilmisAdSoyad = this.state.personelList[index];

            this.state.array[0][index + 1] = new Array();
            this.state.array[0][index + 1][0] = this._getName(PersonelSadelestirilmisAdSoyad);
            this.state.array[0][index + 1][1] = PersonelSadelestirilmisAdSoyad

        }
    }

    _renderResultRowsContentMounth2() {
        var tempFirstElementDate = new Date(this.state.data[0].Tarih);
        var tempAy = tempFirstElementDate.getMonth() + 1;
        var tempYil = tempFirstElementDate.getFullYear()
        var ayinGunSayisi = tempFirstElementDate.monthDays();

        for (let indexGun = 1; indexGun <= ayinGunSayisi; indexGun++) {
            this.state.array[indexGun] = new Array();
            this.state.array[indexGun][0] = `${indexGun}.${tempAy}.${tempYil}`

            for (let indexP = 0; indexP < this.state.personelList.length; indexP++) {
                const PersonelSadelestirilmisAdSoyad = this.state.personelList[indexP];
                this.state.array[indexGun][indexP + 1] = this._getSaat(PersonelSadelestirilmisAdSoyad, indexGun, tempAy, tempYil, this.state.array[indexGun][indexP + 1])
            }
        }
        console.log(this.state.array);
        this.setState({});
    }


    _hesapla = () => {
        this.setState({ showHidePdksListAndUpdate: false })
        console.log(this.state.data);
        this.setState({ hesaplananPDKS: [] });
        pdksService.hesapla(this.state.data).then(result => {
            console.log("hesaplanan veri");
            console.log(result.data);
            this.setState({ hesaplananPDKS: result.data });
        });

    }


    _getTypeName = (value) => {
        var pattSaat = /[0-9]*:[0-9]*/i;

        if (pattSaat.test(value)) {
            return value
        }
        var findeType = this.state.typeList.filter(item => { return item.id === value })
        if (findeType.length > 0)
            return findeType[0].name
        else return ""

    }

    _pdksIndir = () => {
        var temp = JSON.parse(JSON.stringify(this.state.array))
        for (let index = 0; index < temp.length; index++) {
            const element = temp[index];

            for (let x = 0; x < element.length; x++) {

                if (index == 0) {
                    temp[index][x] = temp[index][x][0]
                } else if (x !== 0) {
                    temp[index][x] = this._getTypeName(temp[index][x])
                }
            }
        }
        return temp
    }

    render() {


        return (
            <div>
                <Grid container >
                    <Grid item xs={12} container style={{ background: "white" }}>
                        <Grid item xs={12} md={6}>
                            <TextField
                                fullWidth
                                id="personel-firma"
                                select label="Calistigi firma"
                                value={this.state.MusteriId}
                                onChange={this._handleChange('MusteriId')}
                                SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                                {this.state.Musteri.map(m => (
                                    <MenuItem key={m.id} value={m.id}> {m.FirmaAdi} </MenuItem>
                                ))}
                            </TextField>
                        </Grid>

                        <Grid item xs={12} md={6} style={{ display: "flex", alignItems: "center" }}>

                            <Button onClick={this._handleOpen}>
                                {this.state.files.name ? this.state.files.name : "Dosya Yükle  "}
                                {/* {this.state.file_loaded == true && this.state.files !== {} ? "" : <CircularProgress />} */}
                            </Button>
                            <div id="file_upload" style={{ display: "none" }}>
                                <FileBase64
                                    //multiple={ true } 
                                    onDone={(e) => this._getFiles(e)}
                                />
                            </div>
                            {this.state.files.length !== 0 &&
                                <Button
                                    style={{ marginLeft: "auto" }}
                                    color="primary"
                                    variant="contained"
                                    onClick={this._handleCreate}>  KONTROL İÇİN GÖNDER
                            </Button>}
                        </Grid>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <hr></hr>

                    </Grid>
                    {this.state.array.length > 0 ?
                        <Grid item xs={12} md={12} style={{ background: "white", padding: 5 }}>
                            <text>Personel Pdks:</text>
                            <ExportCSV csvData={this._pdksIndir()} fileName="PDKS_Personel" butonismi="indir" />
                            <Button onClick={() => this.setState({ showHidePdksListAndUpdate: true })} color="primary">  Göster </Button>
                            <Button onClick={() => { this.setState({ showHidePdksListAndUpdate: false }) }} color="primary">  Gizle </Button>
                            <Button onClick={this._hesapla} variant="contained" color="primary">  HESAPLA </Button>

                        </Grid>
                        : ""}

                    {(this.state.array.length > 0 && this.state.showHidePdksListAndUpdate) &&
                        <Grid item xs={12} md={12}>
                            <PdksListAndUpdate array={this.state.array} openModel={this._openModel}
                                typeList={this.state.typeList} />
                        </Grid>
                    }

                    {this.state.hesaplananPDKS.length > 0 &&
                        <Grid item xs={12} md={12}>
                            <PdksSonuc data={this.state.hesaplananPDKS} />
                        </Grid>
                    }


                    <Grid item xs={12} md={12}>
                        <hr></hr>

                    </Grid>
                    {/* <Grid item xs={12} md={12} style={{ background: "white",padding: 5  }}>
                    <Button onClick={this._hesapla} color="primary">  gönder </Button>

                        
                    </Grid> */}

                </Grid>

                {this.state.popupData.rowIndex ?

                    <Dialog open={this.state.openPopupModel} onClose={this._closeModel} aria-labelledby="form-dialog-title">
                        <DialogTitle id="form-dialog-title">
                            {this.state.array[0][this.state.popupData.elementIndex][0]}<br></br>
                            {this.state.array[this.state.popupData.rowIndex][this.state.popupData.elementIndex]}
                        </DialogTitle>
                        <DialogContent>

                            <TextField fullWidth id="type" select label="Çalışma Durumu" value={this.state.ModelSelectType} onChange={this._ModelHandleChange('ModelSelectType')}
                                SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                                {this.state.typeList.map(item => (
                                    <MenuItem key={item.id} value={item.id}> {item.name} </MenuItem>
                                ))}
                            </TextField>

                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this._closeModel} color="primary">
                                İptal
                                </Button>
                            <Button onClick={this._saveModel} color="primary">
                                Degiştir
                                </Button>
                        </DialogActions>
                    </Dialog> : ""
                }

                <Dialog open={this.state.errorPersonel.pdksVarDbYok.length > 0 || this.state.errorPersonel.dbVarPdksYok.length > 0} onClose={this._closeModel} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">
                        Eşleşmeyen Personeller Var Lütfen Personel bilgilerini güncelleyiniz
                        </DialogTitle>
                    <DialogContent>
                        <Grid container>
                            <Grid item xs={12}  >
                                Esleşen Personel Sayısı ={this.state.errorPersonel.eslesmis.length}
                            </Grid>
                            <Grid item xs={6}  >

                                <List>
                                    <ListItem style={{ background: "cornflowerblue", color: "white", borderRadius: 15, width: "88%" }}>
                                        {"pdks Var EXPERTO Yok".toUpperCase()}
                                    </ListItem>
                                    {this.state.errorPersonel.pdksVarDbYok.map((item, index) => (
                                        <ListItem>
                                            <ListItemText key={item.SadelestirilmisAdSoyad + index} primary={item.AdSoyad} secondary={item.SadelestirilmisAdSoyad} />
                                        </ListItem>
                                    ))}
                                </List>
                            </Grid>
                            <Grid item xs={6} >

                                <List>
                                    <ListItem style={{ background: "cornflowerblue", color: "white", borderRadius: 15, width: "88%" }}>
                                        {"EXPERTO Var Pdks Yok".toUpperCase()}
                                    </ListItem>
                                    {this.state.errorPersonel.dbVarPdksYok.map((item, index) => (
                                        <ListItem>
                                            <ListItemText key={item.SadelestirilmisAdSoyad + index} primary={`${item.Adi} ${item.Soyadi}`} secondary={item.SadelestirilmisAdSoyad} />
                                        </ListItem>
                                    ))}
                                </List>
                            </Grid>
                        </Grid>



                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this._closeModel} color="primary">
                            Kapat
                                </Button>
                    </DialogActions>
                </Dialog>

            </div>
        );
    }
}

const styles = {
    rotate: {
        height: "100px",
        maxWidth: "100px",
        whiteSpace: "nowrap",


    },
    transform: {
        writingMode: "tb",
        transform: "rotate(180deg)"
    }


};

function mapStateToProps(state) {
    return {
        snackBarReducer: state.snackBarReducer
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        SnackbarAction: SnackbarAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PdksImportPage);


