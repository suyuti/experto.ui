import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views"; 
import { Paper,Button } from "@material-ui/core";  
import { departmanService } from "../../Services";
import DepartmanFormu from '../../Components/Departman/DepartmanFormu'   
import Snackbar from '../../Components/Snackbar';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {SnackbarAction} from '../../actions/snackbarAction';
 
var self;
class CreateAndUpdate extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            buttonEnable: true,
            message: "",
            value: 0,
            DepartmanFormu: {
            }
        };

        this.paramsId = this.props.match.params.id;
        if (this.paramsId) {
            departmanService.getById(this.paramsId).then(data => {
                this.setState({ DepartmanFormu: data.data[0] })
            });
        }
    }

    handleTabChange(e, v) {
        self.setState({ value: v });
    }

    handleSwipeViewChange(i) {
        self.setState({ value: i });
    }

    handleChange = (input) => e => {
        let inputValue = e.target.value;
        this.setState(() => ({
            DepartmanFormu: {
                ...this.state.DepartmanFormu,
                [input]: inputValue
            }
        }));
    };

    validateInput = () => {
      if (!this.state.DepartmanFormu.DepartmanAdi || this.state.DepartmanFormu.DepartmanAdi == " ") {
        return this.props.SnackbarAction('open', "Lütfen DepartmanAdi alanını giriniz");
      }
      if (!this.state.DepartmanFormu.MusteriId || this.state.DepartmanFormu.MusteriId == " ") {
        return this.props.SnackbarAction('open', "Lütfen MusteriId alanını giriniz");
      }
    }

    createButton = (e) => {
        if(this.validateInput()) return;
        this.setState({buttonEnable:false}); 
        // e.preventDefault();
        departmanService.create(this.state.DepartmanFormu).then(data => {
            console.log(data.status)
            if (data.messages != "") {
              this.props.SnackbarAction('open', data.messages);
            }
            if (data.status === 200) {
                this.setState({buttonEnable:true});
                this.props.history.goBack();
            }
        });
    }

    updateButton = (e) => {
        if(this.validateInput()) return;
        this.setState({ buttonEnable: false });
        departmanService.update(this.state.DepartmanFormu).then(data => {
          if (data.messages != "") {
            this.props.SnackbarAction('open', data.messages);
          }
            if (data.status === 200) {
                this.setState({ buttonEnable: true });
                this.props.history.goBack();
            }
        });
    }

    render() {
        const { classes, ...props } = this.props;
        let button;
        if (this.state.buttonEnable) {
            if (this.paramsId) {
                button = <Button
                    variant="contained"
                    color="default"
                    onClick={this.updateButton}
                    style={styles.save_button}>GÜNCELLE</Button>
            } else {
                button = <Button
                    variant="contained"
                    color="primary"
                    onClick={this.createButton}
                    style={styles.save_button}>KAYDET</Button>
            }
        } 
        else {
            button = <Button
                variant="contained"
                color="primary" 
                style={styles.save_button}>...</Button>
        }

        if (this.state.result) {
        }

        return (
            <div>
                <Paper> 
                                    <Tabs
                                        value={this.state.value}
                                        onChange={this.handleTabChange}
                                        indicatorColor="primary"
                                        textColor="primary"
                                        variant="fullWidth"
                                    >
                                        <Tab label='Departman Bilgileri' />
                                    </Tabs>
                                    <SwipeableViews
                                        style={{margin:"15px"}}
                                        axis="x"
                                        index={this.state.value}
                                        onChangeIndex={this.handleSwipeViewChange}>
                                        <div> 
                                            <DepartmanFormu
                                                handleChange={this.handleChange}
                                                data={this.state.DepartmanFormu} />
                                        </div>
                                    </SwipeableViews>
                                    <br />
                                    {button} 
                </Paper>
            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '20px',
        left: '20px'
    }
};

function mapStateToProps(state){
  return {
      snackBarReducer: state.snackBarReducer
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
      SnackbarAction: SnackbarAction
  },dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAndUpdate);