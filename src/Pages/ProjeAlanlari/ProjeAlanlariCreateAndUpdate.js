import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views";
import { Paper, Button } from "@material-ui/core";
import { projeAlanlariService } from "../../Services";
import ProjeAlanlariFormu from '../../Components/ProjeAlanlari/ProjeAlanlariFormu'
import Snackbar from '../../Components/Snackbar';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {SnackbarAction} from '../../actions/snackbarAction';






var self;
class CreateAndUpdate extends Component {



    constructor(props) {
        super(props);
        self = this;
        this.state = {
            buttonEnable: true,
            message: "sadasd",
            value: 0,
            ProjeAlanlariFormu: {
            }

        };

        this.paramsId = this.props.match.params.id;
        if (this.paramsId) {
            projeAlanlariService.getById(this.paramsId).then(result => {
                this.setState({ ProjeAlanlariFormu: result.data[0] })
 
            });
        }


    }

    handleTabChange(e, v) {
        self.setState({ value: v });
        console.log("handleTabChange");
    }
    handleSwipeViewChange(i) {
        self.setState({ value: i });
        console.log("handleChangeIndex");
    }

    handleChange = (input) => e => {
        // console.log(e.target.value, input);
        let inputValue = e.target.value;
        this.setState(() => ({
            ProjeAlanlariFormu: {
                ...this.state.ProjeAlanlariFormu,
                [input]: inputValue
            }
        }));
    };

    validateInput = () => {
      if (!this.state.ProjeAlanlariFormu.ProjeAlanAdi || this.state.ProjeAlanlariFormu.ProjeAlanAdi == " ") {
        return this.props.SnackbarAction('open', "Lütfen ProjeAlanAdi alanını giriniz");
      }
    }

    createButton = (e) => {
        if(this.validateInput()) return;
        this.setState({ buttonEnable: false });
        // e.preventDefault();
        projeAlanlariService.create(this.state.ProjeAlanlariFormu).then(result => {
          if (result.messages != "") {
            this.props.SnackbarAction('open', result.messages);
          }
            if (result.status === 200) {
                this.setState({ buttonEnable: true });
                this.props.history.goBack();
            }
        });
    }

    updateButton = (e) => {
        if(this.validateInput()) return;
        this.setState({ buttonEnable: false });
        projeAlanlariService.update(this.state.ProjeAlanlariFormu).then(result => {
            // console.log(result.status)
            if (result.messages != "") {
              this.props.SnackbarAction('open', result.messages);
            }
            if (result.status === 200) {
                this.setState({ buttonEnable: true });
                this.props.history.goBack();
            }
        });
    }

    render() {
        const { classes, ...props } = this.props;
        let button;
        if (this.state.buttonEnable) {
            if (this.paramsId) {
                button = <Button
                    variant="contained"
                    color="default"
                    onClick={this.updateButton}
                    style={styles.save_button}>UPDATE</Button>
            } else {
                button = <Button
                    variant="contained"
                    color="primary"
                    onClick={this.createButton}
                    style={styles.save_button}>KAYDET</Button>
            }

        } else {
            button = <Button
                variant="contained"
                color="primary"
                style={styles.save_button}>...</Button>

        }


        if (this.state.result) {

        }


        return (
            <div>
                <Paper>
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleTabChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="fullWidth"
                    >
                        <Tab label='ProjeAlanlariFormu Bilgileri' />
                    </Tabs>
                    <SwipeableViews
                        style={{ margin: "15px" }}
                        axis="x"
                        index={this.state.value}
                        onChangeIndex={this.handleSwipeViewChange}>
                        <div>
                            <ProjeAlanlariFormu
                                handleChange={this.handleChange}
                                data={this.state.ProjeAlanlariFormu} />
                        </div>
                    </SwipeableViews>
                    <br />

                    {button}


                </Paper>



            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '20px',
        left: '20px'
    }
};

function mapStateToProps(state){
  return {
      snackBarReducer: state.snackBarReducer
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
      SnackbarAction: SnackbarAction
  },dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAndUpdate);