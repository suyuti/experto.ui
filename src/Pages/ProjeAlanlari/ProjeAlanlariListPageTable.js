import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add"; 
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import { authenticationService, projeAlanlariService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';


const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));


class PersonelListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //role: null,
            rows: [],
            RowStatus: 1//1 normal liste 2 ekleme 3 düzenleme
        };
        console.log(this.state.rows); 
    }

    componentDidMount() {
       /* authenticationService.currentUser.subscribe(x =>
            this.setState({
                currentUser: x,
                //role: x.role
            })
        );*/
        
        projeAlanlariService.getAll().then(result => this.setState({ rows: result.data }));
    }

    updateRow = (Row) => {
        // alert("updateRow  =" + Row.id);
        this.props.history.push('/projeAlanlari/update/' + Row.id);
        //alert("You saved " + rowData.id)
    }
 

  removeRow = (Row) => {
    // console.log('remove  button');
    // alert("deleteRow  =" + Row.id);
    let confirmMessage = window.confirm(`"${Row.ProjeAlanAdi}" silinecektir. Emin misiniz?`);
    if (confirmMessage == true) {
      this.setState({ buttonEnable: false });
      projeAlanlariService.remove(Row).then(result => {
        console.log(result.status)
        if (result.status = 200) {
          projeAlanlariService.getAll().then(result => this.setState({ rows: result.data }));
        }

      });
    }
  }

 




    render() {
        const { classes } = this.props;
        return (
            <div>
                <MaterialTable
                    title={"proje Alanlari (" + this.state.rows.length+")"}
                    columns={[
                        {
                            title: 'id',
                            field: 'id',
                            cellStyle: {
                                //backgroundColor: '#039be5',
                                //color: '#FFF'
                            },
                            headerStyle: {
                                //backgroundColor: '#039be5',
                            }
                        },
                        { title: 'proje Alanlari Adi', field: 'ProjeAlanAdi' },
                       


                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize:10,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions: [10, 20, 30]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Görev Durumu Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Görev Durumunu sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })


                    ]}
                />
                {checkPermission([PermissionData.postProjeAlanlari]) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/projeAlanlari/create"
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                }
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};


export default withStyles(styles)(PersonelListAndSearch);
