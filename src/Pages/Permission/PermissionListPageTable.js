import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import CircularProgress from '@material-ui/core/CircularProgress'; 
import { Link } from "react-router-dom";
import {  permissionService } from "../../Services";
import {checkPermission,PermissionData } from '../../command';


const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));


class ListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            RowStatus: 1//1 normal liste 2 ekleme 3 düzenleme
        };
    }

    componentDidMount() {
        permissionService.getAll().then(result => this.setState({ rows: result.data }));
    }

    updateRow = (Row) => {
        alert("UpdateRow  =" + Row.id);
        this.props.history.push('/permission/update/' + Row.id);
    }
 

    removeRow = (Row) => {
        alert("deleteRow  =" + Row.id);
        this.setState({ buttonEnable: false });
        permissionService.remove(Row).then(result => {
            if (result.status = 200) {
                permissionService.getAll().then(result => this.setState({ rows: result.data }));  
            }

        });
    }
    

    render() {
        const { classes } = this.props;

        if (!this.state.rows.length) {
            return (
                <div style={{textAlign:"center"}}>
                   <CircularProgress   /> 
                </div>
            )
        }

        return (
            <div>
                <MaterialTable
                    title={"Pemission  Listesi (" + this.state.rows.length+")"}
                    columns={[
                        {
                            title: 'id',
                            field: 'id',
                            cellStyle: {
                                //backgroundColor: '#039be5',
                                //color: '#FFF'
                            },
                            headerStyle: {
                                //backgroundColor: '#039be5',
                            }
                        },
                        { title: 'Key', field: 'PermissionKey' },
                        { title: 'Path', field: 'Path' },
                        { title: 'Method', field: 'Method' },
                        { title: 'Aciklama', field: 'Aciklama' }
                       


                    ]}
                    data={this.state.rows}
                    options={{
                        search: true,
                        actionsColumnIndex: -1
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Pemission  Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Pemission  sil',
                            onClick: (event, rowData) => { this.removeRow(rowData) }
                        })


                    ]}
                />
                {checkPermission(PermissionData.postPermission) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={styles.fab}
                        component={AdapterLink}
                        to="/permission/create"
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                }
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};


export default ListAndSearch;
