import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views"; 
import { Paper,Button } from "@material-ui/core";   
import CircularProgress from '@material-ui/core/CircularProgress'; 
import { permissionService } from "../../Services";
import PermissionFormu from '../../Components/Permission/PermissionFormu'  
import Snackbar from '../../Components/Snackbar' 
 



 

var self;
class CreateAndUpdate extends Component {



    constructor(props) {
        super(props);
        self = this;
        this.state = {
            buttonEnable: true,
            message: "sadasd",
            value: 0,
            PermissionFormu: {
            }

        };

        this.paramsId = this.props.match.params.id;
        if (this.paramsId) {
            permissionService.getById(this.paramsId).then(data => {
                this.setState({ PermissionFormu: data.data[0] })

                /*this.setState(() => ({
                    PermissionFormu: {
                        id:data.data[0].id,
                        EgitimDurumAdi: data.data[0].EgitimDurumAdi
                    }
                }));*/

            });
        }


    }

    handleTabChange(e, v) {
        self.setState({ value: v });
        console.log("handleTabChange");
    }
    handleSwipeViewChange(i) {
        self.setState({ value: i });
        console.log("handleChangeIndex");
    }

    handleChange = (input) => e => {
        console.log(e.target.value, input);
        let inputValue = e.target.value;
        this.setState(() => ({
            PermissionFormu: {
                ...this.state.PermissionFormu,
                [input]: inputValue
            }
        }));
    };
    createButton = (e) => {
       
        console.log(e);
        this.setState({buttonEnable:false}); 

        console.log(this.state.PermissionFormu)
        // e.preventDefault();
        permissionService.create(this.state.PermissionFormu).then(data => {
            console.log(data.status)
            if (data.status = 200) {
                this.setState({buttonEnable:true});
            }
        });
         
    }


    updateButton = (e) => {
        console.log('update button');
        this.setState({ buttonEnable: false });
        permissionService.update(this.state.PermissionFormu).then(data => {
            console.log(data.status)
            if (data.status = 200) {
                this.setState({ buttonEnable: true });
            }

        });
    }




    render() {
        const { classes, ...props } = this.props;
        let button;
        if (this.state.buttonEnable) {
            if (this.paramsId) {
                button = <Button
                    variant="contained"
                    color="default"
                    onClick={this.updateButton}
                    style={styles.save_button}>UPDATE </Button>
            } else {
                button = <Button
                    variant="contained"
                    color="primary"
                    onClick={this.createButton}
                    style={styles.save_button}>KAYDET </Button>
            }

        } else {
            button = <Button
                variant="contained"
                color="primary" 
                style={styles.save_button}>loading</Button>

        }


        if (this.state.result) {
            
        }


        if (this.paramsId &&  !Object.getOwnPropertyNames(this.state.PermissionFormu).length >= 1) {
            return (
                <div style={{textAlign:"center"}}>
                   <CircularProgress   /> 
                </div>
            )
        }

        

        return (
            <div>
            <Snackbar message={this.state.message}/>
                <Paper> 
                                    <Tabs
                                        value={this.state.value}
                                        onChange={this.handleTabChange}
                                        indicatorColor="primary"
                                        textColor="primary"
                                        variant="fullWidth"
                                    >
                                        <Tab label='Permission Formu' />
                                    </Tabs>
                                    <SwipeableViews
                                        style={{margin:"15px"}}
                                        axis="x"
                                        index={this.state.value}
                                        onChangeIndex={this.handleSwipeViewChange}>
                                        <div> 
                                            <PermissionFormu
                                                handleChange={this.handleChange}
                                                data={this.state.PermissionFormu} />
                                        </div>
                                    </SwipeableViews>
                                    <br />

                                    {button} 
 
                          
                </Paper>



            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '20px',
        left: '20px'
    }
};

export default CreateAndUpdate;
