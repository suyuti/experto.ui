import React, { Component } from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {convertTimestampToDate,convertDateToTimestamp} from '../../command'
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views"; 
import { Paper, withStyles, Grid, Button, LinearProgress } from "@material-ui/core";
import PropTypes, { string } from "prop-types"; 
import { personelService } from "../../Services";
import Personelbilgiformu from '../../Components/Personel/Personelbilgiformu'
import Personelbilgiformu2 from '../../Components/Personel/Personelbilgiformu2'
import {SnackbarAction} from '../../actions/snackbarAction';
import CircularProgress from '@material-ui/core/CircularProgress'; 


var self;
class PersonelCreate extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            value: 0,
            personel: {
                MusteriId:this.props.match.params.mid,
                IseBaslamaTarihi:new Date(),
                ProfilId: []
            },
            isCreate: (this.props.match.params.id === undefined),
            isReady: (this.props.match.params.id?false:true),
        };

        this.musteriId=this.props.match.params.mid;

        
        if (this.props.match.params.id !== undefined) {
            personelService.getById(this.props.match.params.id).then(result => {
         
                this.setState({ personel: result.data[0], isReady:true});
            });
        }
    }

    handleTabChange(e, v) {
        self.setState({ value: v });
    }

    handleSwipeViewChange(i) {
        self.setState({ value: i });
    }

    // handleChange = (input) => e => { 
    //      let inputValue = e.target.value;
    //     if (e.target.type === "date") {
    //         this.setState(() => ({
    //             personel: {
    //                 ...this.state.personel,
    //                 [input]: convertDateToTimestamp(inputValue)
    //             }
    //         }));
    //     } 
    //     else {
    //         this.setState(() => ({
    //             personel: {
    //                 ...this.state.personel,
    //                 [input]: inputValue
    //             }
    //         }));
    //     }
    // };

    handleChange = (input) => e => {
        var inputValue;
        if (input === 'IseBaslamaTarihi' ||  input === 'IstenAyrilmaTarihi' ) {
            inputValue = e;
        }
        else {
            inputValue = e.target.value;
        }
        this.setState(() => ({
            personel: {
                ...this.state.personel,
                [input]: inputValue
            }
        }));
        this.musteriId = input === "MusteriId" ? inputValue : null;
    };

    validateInput = () => {
      if(!this.state.personel.TcNo){
        return this.props.SnackbarAction('open', "Lütfen Bir TC Numarası Giriniz");  
      }
      if(!this.state.personel.Adi){
        return this.props.SnackbarAction('open', "Lütfen Personel Adını Giriniz");  
      }
    }


    createButton = (e) => {
        if(this.validateInput()) return;
        //this.state.personel.CreatedAt = Date.now();
        //tokenda backend den cekip kim oluşturduysa onun bilgisi alınacak şimdilik localstorage den alınıyor
        this.state.personel.CreatedBy = localStorage.getItem('currentUser').id; 
        personelService.create(this.state.personel).then(data => 
            {  
              if (data.status == 200) {
                  this.props.history.goBack();
              }
              this.props.SnackbarAction('open', data.messages); 
            });
           // this.props.SnackbarAction('open',result); 
    }

    updateButton = (e) => {
        if(this.validateInput()) return;
        //tokenda backend den cekip kim oluşturduysa onun bilgisi alınacak şimdilik localstorage den alınıyor
        this.state.personel.UpdatedAt = Date.now();
        //tokenda backend den cekip kim oluşturduysa onun bilgisi alınacak şimdilik localstorage den alınıyor
        this.state.personel.UpdatedBy = localStorage.getItem('currentUser').id;
      
        personelService.update(this.state.personel).then(result => {
            // this.props.history.goBack();
            if (result.messages != "") {
              this.props.SnackbarAction('open', result.messages);
            }
            if (result.status === 200) {
              this.setState({ buttonEnable: true });
              this.props.history.goBack();
            }
        })
    }

    render() {
        const { classes, ...props } = this.props;
        if (this.state.isReady === false) return (<CircularProgress />)
        let button;
        if (this.props.match.params.id) {
            button = <Button
                variant="contained"
                color="default"
                onClick={this.updateButton}
                style={styles.save_button}
            >GÜNCELLE</Button>
        } else {
            button = <Button
                fullWidth
                variant="contained"
                color="primary"
                onClick={this.createButton}
                style={styles.save_button}
            >KAYDET</Button>
        }


        return (
            <div>
                <Paper>
                    <Grid
                        container
                        direction="row"
                        justify="flex-end"
                        alignItems="flex-start"
                    >
                       {button}
                    </Grid>

                    <Tabs
                        value={this.state.value}
                        onChange={this.handleTabChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="fullWidth"
                    >
                        <Tab label='Personel Bilgileri' />
                        <Tab label='Firma Bilgileri' />

                    </Tabs>
                    
                    <SwipeableViews
                        axis="x"
                        index={this.state.value}
                        onChangeIndex={this.handleSwipeViewChange}>
                        <div>{(this.state.isReady) && 
                            (<Personelbilgiformu
                                handleChange={this.handleChange}
                                personel={this.state.personel}
                                button=""
                            />)}
                        </div>
                        <div>
                            <Personelbilgiformu2
                                handleChange={this.handleChange}
                                personel={this.state.personel}
                                button=""
                                musteriId={this.musteriId}
                                params ={this.props.match.params.mid}
                            />
                        </div>
                    </SwipeableViews>
                </Paper>
            </div>
        );
    }
}

const styles = {
    save_button: {
        marginTop: '20px',
        marginRight: '20px'
    }
};
 

function mapStateToProps(state){
    return {
        snackBarReducer: state.snackBarReducer
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        SnackbarAction: SnackbarAction
    },dispatch)
}
  
export default connect(mapStateToProps, mapDispatchToProps)(PersonelCreate);
