import React from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import MaterialTable from 'material-table';
import { Link } from "react-router-dom"; 
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import {CircularProgress} from '@material-ui/core'; 
import { personelService, personelProfilService ,musteriService} from "../../Services";
import {SnackbarAction} from '../../actions/snackbarAction';

import {checkPermission,PermissionData } from '../../command';

const AdapterLink = React.forwardRef((props, ref) => (
    <Link innerRef={ref} {...props} />
));

class PersonelListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Personeller: [],
            ProfilFilterLookup: {},
            personelReady : false,
            profilReady:false,
            RowStatus: 1//1 normal liste 2 ekleme 3 düzenleme
        };


        

    }

    componentDidMount() { 
        if (this.props.musteriId) {
            musteriService.getPersonelProfil(this.props.musteriId).then(result => {
                var x = result.data.reduce(function(map, obj) {
                    map[obj.id] = obj.ProfilAdi;
                    return map;
                }, {});  
                this.setState({ProfilFilterLookup: x, profilReady:true})
            })

            musteriService.getPersoneller(this.props.musteriId)
            .then(result => { 
                if (result.status === 200) {
                    if (result.data != null) { 
                        var ProfileIdDuzetme = result.data.map((item) => {
                            if (item.ProfileId == undefined || item.ProfileId == null) {
                                item.ProfileId = -1;
                            }
                            return item
                        })
                        this.setState({ Personeller: ProfileIdDuzetme, personelReady: true }) 
 
                    }
                }else if(result.status === 400) {
                    this.setState({Personeller: [] , personelReady:true})
                }
            });

        } else {
            personelProfilService.getAll().then(result => {
                var x = result.data.reduce(function(map, obj) {
                    map[obj.id] = obj.ProfilAdi;
                    return map;
                }, {}); 
                this.setState({ProfilFilterLookup: x, profilReady:true})
            })
            personelService.getAll().then(result => {
                var ProfileIdDuzetme = result.data.map((item) => {
                    if (item.ProfileId == undefined || item.ProfileId == null) {
                        item.ProfileId = -1;
                    }
                    return item
                })
                this.setState({ Personeller: ProfileIdDuzetme, personelReady: true }) 
            });

        }
        
        
    }

    updateRow = (Row) => {
        if (this.props.musteriId) {
            this.props.history.push('/musteri/' + this.props.musteriId + '/personel/update/' + Row.id);
        }
        else {
            this.props.history.push('/personel/update/' + Row.id);
        }        
    }

    deleteRow = (personel) => {
      let confirmMessage = window.confirm(`"${personel.Adi} ${personel.Soyadi}" silinecektir. Emin misiniz?`);
      if (confirmMessage == true) {
        personelService.remove(personel).then(data => {
          if (data.status == 200) {
            this.setState({
              ...this.state,
              Personeller: this.state.Personeller.filter(function (person) {
                return person.id !== personel.id
              })
            });
          }
          this.props.SnackbarAction('open', data.messages);
        });
      }
    }

    render() {
        const { classes ,snackBarReducer} = this.props; 
        if (this.state.personelReady === false || this.state.profilReady === false) {
            return <CircularProgress />
        }
        return (
            <div style={{position:"relative", paddingBottom: "60px"}}>
                <MaterialTable
                    title={"Personel Listesi (" + this.state.Personeller.length + ")"}
                    columns={[
                        { title: 'Adi', field: 'Adi'},
                        { title: 'Soyadi', field: 'Soyadi' },
                        { title: 'Firması', field: 'FirmaAdi' },
                        { title: 'Egitim Durumu', field: 'EgitimDurumAdi' }, 
                        {
                            title: 'Profili',
                            field: 'ProfileId',
                            lookup:this.state.ProfilFilterLookup,  
                        },
                        {
                            title: 'Durumu',
                            field: 'Durumu',
                            lookup: { 0: 'Pasif', 1: 'Aktif' }, 
                        },
                    ]}
                    data={this.state.Personeller}
                    options={{
                        filtering: true,
                        search: true,
                        actionsColumnIndex: -1,
                        pageSize:20,
                        emptyRowsWhenPaging: false,
                        pageSizeOptions: [20, 50, 100]
                    }}
                    actions={[
                        rowData => ({
                            icon: 'update',
                            tooltip: 'Personel Güncelle',
                            onClick: (event, rowData) => this.updateRow(rowData)
                        }),
                        rowData => ({
                            icon: 'delete',
                            tooltip: 'Personel Sil',
                            onClick: (event, rowData) => { this.deleteRow(rowData) }
                        })
                    ]}
                />
                {checkPermission([PermissionData.postPersonel]) ?
                    <Fab
                        color="primary"
                        aria-label="add"
                        style={this.props.musteriId? styles.fabIn :styles.fab}
                        component={AdapterLink}
                        to={(this.props.musteriId)?"/musteri/"+this.props.musteriId+"/personel/create": '/personel/create'}
                    >
                        <AddIcon />
                    </Fab>
                    : ' '
                }
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    },
    fabIn: {
        margin: 13,
        right: 0,
        position: "absolute", 
    }
};


function mapStateToProps(state){
    return {
        snackBarReducer: state.snackBarReducer
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        SnackbarAction: SnackbarAction
    },dispatch)
}
 



export default connect(mapStateToProps, mapDispatchToProps)(PersonelListAndSearch);
