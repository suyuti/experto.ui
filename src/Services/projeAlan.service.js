import { API_URL } from '../config'
import { http } from './axios'

function getAll() {
    return http.get("/projeAlanlari")
}

function getById(id) {
    return http.get(API_URL + `/projeAlanlari/${id}`)
}

function create(projeAlan) {
    return http.post(API_URL + '/projeAlanlari', projeAlan)
}

function update(projeAlan) {
    return http.put(API_URL + `/projeAlanlari/${projeAlan.id}`, projeAlan)
}

function remove(projeAlan) {
    return http.delete(API_URL + `/projeAlanlari/${projeAlan.id}`)
}

export const projeAlanService = {
    getAll,
    getById,
    create,
    update,
    remove
};
