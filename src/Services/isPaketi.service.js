import { API_URL } from '../config'
import { http } from './axios'

function getAll() {
    return http.get("/ispaketleri")
}

function getById(Code) {
    return http.get(API_URL + `/ispaketleri/${Code}`)
}

function create(ispaketleri) {
    return http.post(API_URL +`/proje/${ispaketleri.ProjeId}/ispaketi`, ispaketleri)
}

function update(ispaketleri) {
    return http.put(API_URL + `/ispaketleri/${ispaketleri.id}`, ispaketleri)
}

function remove(ispaketleri) {
    return http.post(API_URL +`/proje/${ispaketleri.ProjeId}/ispaketi/delete`, ispaketleri)
}

export const isPaketiService = {
    getAll,
    getById,
    create,
    update,
    remove
};


