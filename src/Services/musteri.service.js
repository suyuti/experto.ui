import { API_URL } from '../config'
import { http } from './axios'

function getAll() {
    return http.get(API_URL + "/musteri")
}

function getById(id) {
    return http.get(API_URL + `/musteri/${id}`)
}

function create(musteri) {
    return http.post(API_URL + '/musteri', musteri)
}

function update(musteri) {
    return http.put(API_URL + `/musteri/${musteri.id}`, musteri)
}

function remove(musteri) {
    return http.delete(API_URL + `/musteri/${musteri.id}`)
}

function getProjects(musteriCode) {
    return http.get(API_URL + `/musteri/${musteriCode}/proje`)
}

function addProje(musteriId, proje) {
    return http.post(API_URL + `/musteri/${musteriId}/proje`, proje)
}

function getPersoneller(musteriId) {
    return http.get(API_URL + `/musteri/${musteriId}/personel`)
}

function adamAyHesapla(musteriId, startDate, endDate) { 
    var adamAyParams = {
        startDate: startDate,
        endDate: endDate  
    }
    return http.post(API_URL + `/musteri/${musteriId}/adamay`, adamAyParams)
}
function adamAyUpdate(adamay) { 
    if(adamay.id){
        return http.put(API_URL + `/musteri/adamay/${adamay.id}/update`, adamay)
    }
}
function getPersonelProfil(musteriId) {  
    return http.get(API_URL + `/musteri/${musteriId}/personelProfil`) 
}
function getEkipler(musteriId) {  
    return http.get(API_URL + `/musteri/${musteriId}/ekip`) 
}


export const musteriService = {
    getAll,
    getById,
    create,
    update,
    remove,
    getProjects,
    addProje,
    getPersoneller,
    adamAyHesapla,
    adamAyUpdate,
    getPersonelProfil,
    getEkipler
};
