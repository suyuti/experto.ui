import { API_URL } from '../config'
import { http } from './axios'

function getAll() {
    return http.get(API_URL +"/personel")
}

function getById(id) {
    return http.get(API_URL + `/personel/${id}`)
}

function create(personel) {
    console.log(API_URL + '/personel')
    return http.post(API_URL + '/personel', personel)
}

function update(personel) {
    return http.put(API_URL + `/personel/${personel.id}`, personel)
}

function remove(personel) {
    return http.delete(API_URL + `/personel/${personel.id}`)
}

export const personelService = {
    getAll,
    getById,
    create,
    update,
    remove
};
