// import { authHeader, handleResponse } from '../Helpers';
// import { API_URL } from '../config' 
 

// function getAll() {
//     const requestOptions = { method: 'GET', headers: authHeader() };
//     return fetch(API_URL + "/users", requestOptions).then(handleResponse);
// }

// function getById(id) {
//     const requestOptions = { method: 'GET', headers: authHeader() };
//     return fetch(API_URL+ `/users/${id}`, requestOptions).then(handleResponse);
// }

// function getRole() {
//     const requestOptions = { method: 'GET', headers: authHeader() };
//     return fetch(API_URL+ `/roles`, requestOptions).then(handleResponse);
// }
// function getUserRole(id) {
//     const requestOptions = { method: 'GET', headers: authHeader() };
//     return fetch(API_URL+ `/users/${id}/role`, requestOptions).then(handleResponse);
// }

 

import { http } from './axios'

function getAll() {
    return http.get("/users")
}
function create(data) {
    return http.post("/users",data)
}
function getById(id) {
    return http.get(`/users/${id}`)
}

function update(user) {
    return http.put(`/users/${user.id}`, user)
}
function remove(id) {
    return http.delete(`/users/${id}`)
}

function getRole() {
    return http.get('/roles')
}

function getUserRole(id) {
    return http.get(`/users/${id}/role`)
}

function setUserRole(id,roleList) {
    return http.put(`/users/${id}/role`,roleList)
}
 

export const userService = {
    getAll,
    getById,
    getRole,
    getUserRole,
    setUserRole,
    update,
    create,
    remove
};