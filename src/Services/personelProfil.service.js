
import { API_URL } from '../config'
import { http } from './axios'

function getAll() {
    return http.get("/personelProfil")
}

function getById(id) {
    return http.get(API_URL + `/personelProfil/${id}`)
}

function create(personelProfil) {
    return http.post(API_URL + '/personelProfil', personelProfil)
}

function update(personelProfil) {
    return http.put(API_URL + `/personelProfil/${personelProfil.id}`, personelProfil)
}

function remove(personelProfil) {
    return http.delete(API_URL + `/personelProfil/${personelProfil.id}`)
}

export const personelProfilService = {
    getAll,
    getById,
    create,
    update,
    remove
};

