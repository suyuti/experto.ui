import { API_URL } from '../config'
import { http } from './axios'

function getAll() {
    return http.get("/proje")
}

function getById(id) {
    return http.get(`/proje/${id}`)
}

function create(Mid,proje) {
    return http.post(API_URL + `/musteri/${Mid?Mid : proje.MusteriId}/proje`, proje)
}

function update(proje) {
    return http.put(API_URL + `/proje/${proje.id}`, proje)
}

function remove(proje) {
    return http.delete( API_URL + `/proje/${proje.id}`)
}

function getIspaketleri(id) {

    return http.get( API_URL + `/proje/${id}/ispaketi`)
}

function removeIsPaketi(proje) {

    return http.delete(API_URL + `/ispaketleri/${proje.id}`)
}




export const projeService = {
    getAll,
    getById,
    create,
    update,
    remove,
    getIspaketleri,
    removeIsPaketi
};
