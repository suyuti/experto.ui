
import { http } from './axios'

function getAll() {
    return http.get("/projeTurleri")
}

function getById(id) {
    return http.get(`/projeTurleri/${id}`)
}

function create(projeTurleri) {
    return http.post('/projeTurleri', projeTurleri)
}

function update(projeTurleri) {
    return http.put(`/projeTurleri/${projeTurleri.id}`, projeTurleri)
}

function remove(projeTurleri) {
    return http.delete( `/projeTurleri/${projeTurleri.id}`)
}

export const projeTurleriService = {
    getAll,
    getById,
    create,
    update,
    remove
};

