
import { http } from './axios'

function getAll() {
    return http.get("/egitimDurumu")
}

function getById(id) {
    return http.get(`/egitimDurumu/${id}`)
}

function create(egitimDurumu) {
    return http.post('/egitimDurumu', egitimDurumu)
}

function update(egitimDurumu) {
    return http.put(`/egitimDurumu/${egitimDurumu.id}`, egitimDurumu)
}

function remove(egitimDurumu) {
    return http.delete( `/egitimDurumu/${egitimDurumu.id}`)
}

export const egitimDurumuService = {
    getAll,
    getById,
    create,
    update,
    remove
};

