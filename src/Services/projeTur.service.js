import { API_URL } from '../config'
import { http } from './axios'

function getAll() {
    return http.get("/projeTurleri")
}

function getById(id) {
    return http.get(API_URL + `/projeTurleri/${id}`)
}

function create(projetur) {
    return http.post(API_URL + '/projeTurleri', projetur)
}

function update(projetur) {
    return http.put(API_URL + `/projeTurleri/${projetur.id}`, projetur)
}

function remove(projetur) {
    return http.delete(API_URL + `/projeTurleri/${projetur.id}`)
}

export const projeTurService = {
    getAll,
    getById,
    create,
    update,
    remove
};
