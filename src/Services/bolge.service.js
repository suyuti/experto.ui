import { http } from './axios'

function getAll() {
    return http.get("/bolge")
}

function getById(id) {
    return http.get(`/bolge/${id}`)
}

function create(bolge) {
    return http.post('/bolge', bolge)
}

function update(bolge) {
    return http.put(`/bolge/${bolge.id}`, bolge)
}

function remove(bolge) {
    return http.delete( `/bolge/${bolge.id}`)
}

export const bolgeService = {
    getAll,
    getById,
    create,
    update,
    remove
};

