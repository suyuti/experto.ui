import { http } from './axios'

function getAll() {
    return http.get("/ekip")
}

function getById(id) {
    return http.get(`/ekip/${id}`)
}

function create(ekip) {
    return http.post('/ekip', ekip)
}

function update(ekip) {
    return http.put(`/ekip/${ekip.id}`, ekip)
}

function remove(ekip) {
    return http.delete( `/ekip/${ekip.id}`)
}

export const ekipService = {
    getAll,
    getById,
    create,
    update,
    remove
};
