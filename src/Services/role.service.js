import { http } from './axios'

function getAll() {
    return http.get("/roles")
}

function getById(id) {
    return http.get(`/roles/${id}`)
}

function create(roleAndPermission) {
    return http.post('/roles', roleAndPermission)
}

function update(roleAndPermission) {
    return http.put(`/roles/${roleAndPermission.RoleFormu.id}`, roleAndPermission)
}

function remove(role) {
    return http.delete( `/roles/${role.id}`)
}
function getAllPermissionList() {
    return http.get( `/permission`)
}

function getPermissionByRoleId(id) {
    return http.get( `/roles/${id}/permission`)
}
export const roleService = {
    getAll,
    getById,
    create,
    update,
    remove,
    getAllPermissionList,
    getPermissionByRoleId
};
