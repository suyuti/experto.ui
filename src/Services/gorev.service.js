import { API_URL } from '../config'
import { http } from './axios'

function getAll() {
    return http.get("/gorev")
}

function getById(id) {
    return http.get(API_URL + `/gorev/${id}`)
}

function create(gorev) {
    return http.post(API_URL + '/gorev', gorev)
}

function update(gorev) {
    return http.put(API_URL + `/gorev/${gorev.id}`, gorev)
}

function remove(gorev) {
    return http.delete(API_URL + `/gorev/${gorev.id}`)
}

export const gorevService = {
    getAll,
    getById,
    create,
    update,
    remove
};


