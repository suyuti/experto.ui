
import { http } from './axios'

function getAll() {
    return http.get("/sektor")
}

function getById(id) {
    return http.get(`/sektor/${id}`)
}

function create(sektor) {
    return http.post('/sektor', sektor)
}

function update(sektor) {
    return http.put(`/sektor/${sektor.id}`, sektor)
}

function remove(sektor) {
    return http.delete( `/sektor/${sektor.id}`)
}

export const sektorService = {
    getAll,
    getById,
    create,
    update,
    remove
};

