
import { http } from './axios'

function getAll() {
    return http.get("/departman")
}

function getById(id) {
    return http.get(`/departman/${id}`)
}

function create(departman) {
    return http.post('/departman', departman)
}

function update(departman) {
    return http.put(`/departman/${departman.id}`, departman)
}

function remove(departman) {
    return http.delete( `/departman/${departman.id}`)
}

export const departmanService = {
    getAll,
    getById,
    create,
    update,
    remove
};


 