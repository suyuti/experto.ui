import { API_URL } from '../config'
import { http } from './axios'

function getAll() {
    return http.get("/permission")
}

function getById(id) {
    return http.get(API_URL + `/permission/${id}`)
}

function create(permission) {
    return http.post(API_URL + '/permission', permission)
}

function update(permission) {
    return http.put(API_URL + `/permission/${permission.id}`, permission)
}

function remove(permission) {
    return http.delete(API_URL + `/permission/${permission.id}`)
}

export const permissionService = {
    getAll,
    getById,
    create,
    update,
    remove
};


