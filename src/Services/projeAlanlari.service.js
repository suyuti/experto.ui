
import { http } from './axios'

function getAll() {
    return http.get("/projeAlanlari")
}

function getById(id) {
    return http.get(`/projeAlanlari/${id}`)
}

function create(projeAlanlari) {
    return http.post('/projeAlanlari', projeAlanlari)
}

function update(projeAlanlari) {
    return http.put(`/projeAlanlari/${projeAlanlari.id}`, projeAlanlari)
}

function remove(projeAlanlari) {
    return http.delete( `/projeAlanlari/${projeAlanlari.id}`)
}

export const projeAlanlariService = {
    getAll,
    getById,
    create,
    update,
    remove
};

