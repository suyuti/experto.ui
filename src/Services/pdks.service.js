
import { http } from './axios'

function getAll() {
    return http.get("/pdks")
}

function getById(id) {
    return http.get(`/pdks/${id}`)
}

function create(pdks) {
    return http.post('/pdks', pdks)
}
function checkPdks(pdks) {
    return http.post('/pdks/check', pdks)
}
function hesapla(pdks) {
    return http.post('/pdks/hesapla', pdks)
}
function update(pdks) {
    return http.put(`/pdks/${pdks.id}`, pdks)
}

function remove(pdks) {
    return http.delete( `/pdks/${pdks.id}`)
}



export const pdksService = {
    getAll,
    getById,
    create,
    update,
    remove,
    checkPdks,
    hesapla
};

