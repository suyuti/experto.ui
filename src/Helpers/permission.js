import {authenticationService} from '../Services'

// can("Musteri", "update")
export function can(resource, action) {
    console.log(authenticationService.currentUserValue.Permissions)
    //return true
    return authenticationService.currentUserValue.Permissions.includes(resource+":"+action);
}

