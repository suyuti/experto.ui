import React, { Component } from "react";
import { authenticationService } from "./Services";
import "./App.css";
import MainLayout from "./Layout/MainLayout";
import CssBaseline from "@material-ui/core/CssBaseline";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

import {PermissionData } from './command';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomePage from "./Pages/Home";
import DashboardPage from "./Pages/Dashboard/Dashboard";
import { history, Role } from "./Helpers";
import LoginPage from "./Pages/Account/LoginPage";
import { PrivateRoute } from "./Components/PrivateRoute";
import MusteriPage from "./Pages/Musteriler/MusteriPage";
import MusteriEdit from "./Components/Musteri/MusteriEdit";
import MusteriProjelerPage from "./Pages/Musteriler/MusteriProjelerPage";
import ProjeCreatePage from "./Pages/Proje/ProjeCreatePage";
import AdminDashboard from "./Pages/Admin/AdminDashboard"
import UsersPage from "./Pages/Admin/UsersPage";

import PersonelPage from "./Pages/Personel/PersonelListPageTable";

import EgitimDurumuListPage from "./Pages/EgitimDurumu/EgitimDurumuListPageTable";
import EgitimDurumuCreateAndUpdate from "./Pages/EgitimDurumu/EgitimDurumuCreateAndUpdate";

import GorevListPage from "./Pages/Gorev/GorevListPageTable";
import GorevCreateAndUpdate from "./Pages/Gorev/GorevCreateAndUpdate";

import PersonelProfilListPageTable from "./Pages/PersonelProfil/PersonelProfilListPageTable";
import personelProfilCreateAndUpdate from "./Pages/PersonelProfil/PersonelProfilCreateAndUpdate";

import MusteriCreateAndUpdate from "./Pages/Musteriler/MusteriCreateAndUpdate";
import AdamAyHesaplamaPage from "./Pages/AdamAy/AdamAyHesaplamaPage";
import ProjeListPage from "./Pages/Proje/ProjeListPage";

import ProjeTurleriListPage from "./Pages/ProjeTurleri/ProjeTurleriListPage";
import ProjeTurleriCreateAndUpdate from "./Pages/ProjeTurleri/ProjeTurleriCreateAndUpdate";

import ProjeAlanlariListPage from "./Pages/ProjeAlanlari/ProjeAlanlariListPageTable";
import ProjeAlanlariCreateAndUpdate from "./Pages/ProjeAlanlari/ProjeAlanlariCreateAndUpdate";

import DepartmanListPage from "./Pages/Departman/DepartmanListPageTable";
import DepartmanCreateAndUpdate from "./Pages/Departman/DepartmanCreateAndUpdate";
import ProjeCreateUpdatePage from './Pages/Proje/ProjeCreateUpdatePage' 

import EkipListPage from "./Pages/Ekip/EkipListPageTable";
import EkipCreateAndUpdate from "./Pages/Ekip/EkipCreateAndUpdate";

import BolgeListPage from "./Pages/Bolge/BolgeListPageTable";
import BolgeCreateAndUpdate from "./Pages/Bolge/BolgeCreateAndUpdate";

import SektorListPage from "./Pages/Sektor/SektorListPageTable";
import SektorCreateAndUpdate from "./Pages/Sektor/SektorCreateAndUpdate";

//import ListAndSearch from "./Pages/Proje/ProjeListPageTable";
//import ProjeCreateAndUpdate from "./Pages/Proje/ProjeCreateAndUpdate";

import IsPaketiListPage from "./Pages/IsPaketi/IsPaketiListPageTable";
import IsPaketiCreateAndUpdate from "./Pages/IsPaketi/IsPaketiCreateAndUpdate";

import PersonelCreateAndUpdate from "./Pages/Personel/PersonelCreateAndUpdate";

import UsersListPage from "./Pages/Users/UsersListPageTable";
import UsersCreateAndUpdate from "./Pages/Users/UsersCreateAndUpdate";
import PermissionListPage from "./Pages/Permission/PermissionListPageTable";
import PermissionCreateAndUpdate from "./Pages/Permission/PermissionCreateAndUpdate";

import RoleListPage from "./Pages/Role/RoleListPage";
import RoleCreateAndUpdate from "./Pages/Role/RoleCreateAndUpdate";
 

import KullanicilarListPage from "./Pages/Users/UsersListPageTable";

import PdksImportPage from "./Pages/Pdks/PdksImportPage";
 



const DashboardRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <MainLayout>
          <Component {...matchProps} />
        </MainLayout>
      )}
    />
  );
};
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: null,
      isAdmin: false
    };
  }

  componentDidMount() {
    authenticationService.currentUser.subscribe(x =>
      this.setState({
        currentUser: x,
        isAdmin: x && x.role === Role.Admin
      })
    );
  }

  logout() {
    authenticationService.logout();
    history.push("/login");
  }

  render() {
    const { settings } = this.props;
    const { currentUser, isAdmin } = this.state;

    return (
      <div  >
        <CssBaseline />
        <div style={{ height: "100vh" }}>
          <Router history={history}>
            <Switch>

              {/*<DashboardRoute exact path="/" component={HomePage} >*/}
              <PrivateRoute
                path="/dashboard"
                permissionKeys={[]}
                component={DashboardPage}
              />
              <PrivateRoute
                path="/musteri/create"
                permissionKeys={[PermissionData.postMusteri]}
                roles={[Role.User, Role.Admin]}
                component={MusteriCreateAndUpdate}
              /> 
              <PrivateRoute
                path="/musteri/:mid/personelProfil/create"
                permissionKeys={[PermissionData.postPersonelProfil]}
                component={personelProfilCreateAndUpdate}
              />
              <PrivateRoute
                path="/musteri/:mid/ekip/create"
                permissionKeys={[PermissionData.postEkip]}
                component={EkipCreateAndUpdate}
              />
              <PrivateRoute
                path="/musteri/:mid/proje/create"
                permissionKeys={[PermissionData.postMusteriIdProje]}
                component={ProjeCreateUpdatePage}
              />
              <PrivateRoute
                path="/musteri/:mid/proje/update/:id"
                permissionKeys={[PermissionData.getProjeId]}
                component={ProjeCreateUpdatePage}
              />
              <PrivateRoute
                path="/musteri/:id/proje"
                permissionKeys={[PermissionData.getMusteriIdProje]}
                component={MusteriProjelerPage}
              />
             

              <PrivateRoute
                path="/musteri/:mid/personel/create"
                permissionKeys={[PermissionData.postPersonel]}
                component={PersonelCreateAndUpdate}
              />
              <PrivateRoute
                path="/musteri/:mid/personel/update/:id"
                permissionKeys={[PermissionData.putPersonelId]}
                component={PersonelCreateAndUpdate}
              />

              


              <PrivateRoute
                path="/musteri/:id/adamay"
                permissionKeys={[PermissionData.getMusteriIdAdamay]}
                component={AdamAyHesaplamaPage}
              />
              <PrivateRoute
                permissionKeys={[PermissionData.getMusteriId]}
                path="/musteri/:id"
                component={MusteriCreateAndUpdate}
              />
              <PrivateRoute
                path="/musteri"
                permissionKeys={[PermissionData.getMusteri]}
                component={MusteriPage}
              /> 
              <PrivateRoute
                path="/admin/user"
                permissionKeys={[PermissionData.getUsers]}
                component={UsersPage}
              />
              <PrivateRoute
                path="/admin"
                permissionKeys={[]}
                component={AdminDashboard}
              />
              <PrivateRoute
                path="/personel/create"
                permissionKeys={[PermissionData.postPersonel]}
                component={PersonelCreateAndUpdate}
              />
              <PrivateRoute
                path="/personel/update/:id"
                permissionKeys={[PermissionData.putPersonelId]}
                component={PersonelCreateAndUpdate}
              />
              <PrivateRoute
                path="/personel"
                permissionKeys={[PermissionData.getPersonel]}
                component={PersonelPage}
              />



              <PrivateRoute
                path="/egitimDurumu/create"
                permissionKeys={[PermissionData.postEgitimDurumu]}
                component={EgitimDurumuCreateAndUpdate}
              />
              <PrivateRoute
                path="/egitimDurumu/update/:id"
                permissionKeys={[PermissionData.putEgitimDurumuId]}
                component={EgitimDurumuCreateAndUpdate}
              />
              <PrivateRoute
                path="/egitimDurumu"
                permissionKeys={[PermissionData.getEgitimDurumu]}
                component={EgitimDurumuListPage}
              />



              <PrivateRoute
                path="/gorev/create"
                permissionKeys={[PermissionData.postGorev]}
                component={GorevCreateAndUpdate}
              />
              <PrivateRoute
                path="/gorev/update/:id"
                permissionKeys={[PermissionData.putGorevId]}
                component={GorevCreateAndUpdate}
              />
              <PrivateRoute
                path="/gorev"
                permissionKeys={[PermissionData.getGorev]}
                component={GorevListPage}
              />

              
              <PrivateRoute
                path="/personelProfil/create"
                permissionKeys={[PermissionData.postPersonelProfil]}
                component={personelProfilCreateAndUpdate}
              />
              <PrivateRoute
                path="/personelProfil/update/:id"
                permissionKeys={[PermissionData.putPersonelProfilId]}
                component={personelProfilCreateAndUpdate}
              /> 
              <PrivateRoute
                path="/personelProfil"
                permissionKeys={[PermissionData.getPersonelProfil]}
                component={PersonelProfilListPageTable}
              />   
              <PrivateRoute
                path="/projeTurleri/create"
                permissionKeys={[PermissionData.postProjeTurleri]}
                component={ProjeTurleriCreateAndUpdate}
              />
              <PrivateRoute
                path="/projeTurleri/update/:id"
                permissionKeys={[PermissionData.putProjeTurleriId]}
                component={ProjeTurleriCreateAndUpdate}
              />
              <PrivateRoute
                path="/projeTurleri"
                permissionKeys={[PermissionData.getProjeTurleri]}
                component={ProjeTurleriListPage}
              />
              <PrivateRoute
                path="/projeAlanlari/create"
                permissionKeys={[PermissionData.postProjeAlanlari]}
                component={ProjeAlanlariCreateAndUpdate}
              />
              <PrivateRoute
                path="/projeAlanlari/update/:id"
                permissionKeys={[PermissionData.putProjeAlanlariId]}
                component={ProjeAlanlariCreateAndUpdate}
              />
              <PrivateRoute
                path="/projeAlanlari"
                permissionKeys={[PermissionData.getProjeAlanlari]}
                component={ProjeAlanlariListPage}
              />

              <PrivateRoute
                path="/departman/create"
                permissionKeys={[PermissionData.postDepartman]}
                component={DepartmanCreateAndUpdate}
              />
              <PrivateRoute
                path="/departman/update/:id"
                permissionKeys={[PermissionData.putDepartmanId]}
                component={DepartmanCreateAndUpdate}
              />
              <PrivateRoute
                path="/departman"
                permissionKeys={[PermissionData.getDepartman]}
                component={DepartmanListPage}
              />


              <PrivateRoute
                path="/ekip/create"
                permissionKeys={[PermissionData.postEkip]}
                component={EkipCreateAndUpdate}
              />
              <PrivateRoute
                path="/ekip/update/:id"
                permissionKeys={[PermissionData.putEkipId]}
                component={EkipCreateAndUpdate}
              />
              <PrivateRoute
                path="/ekip"
                permissionKeys={[PermissionData.getEkip]}
                component={EkipListPage}
              />



              <PrivateRoute
                path="/bolge/create"
                permissionKeys={[PermissionData.postBolge]}
                component={BolgeCreateAndUpdate}
              />
              <PrivateRoute
                path="/bolge/update/:id"
                permissionKeys={[PermissionData.putBolgeId]}
                component={BolgeCreateAndUpdate}
              />
              <PrivateRoute
                path="/bolge"
                permissionKeys={[PermissionData.getBolge]}
                component={BolgeListPage}
              />



              <PrivateRoute
                path="/sektor/create"
                permissionKeys={[PermissionData.postSektor]}
                component={SektorCreateAndUpdate}
              />
              <PrivateRoute
                path="/sektor/update/:id"
                permissionKeys={[PermissionData.putSektorId]}
                component={SektorCreateAndUpdate}
              />
              <PrivateRoute
                path="/sektor"
                permissionKeys={[PermissionData.getSektor]}
                component={SektorListPage}
              />

              <PrivateRoute
                path="/proje/musteri/:mid"
                permissionKeys={[PermissionData.getMusteriIdProje]}
                component={ProjeListPage}
              />

              

              <PrivateRoute
                path="/proje/:pid/ispaketi/create"
                permissionKeys={[PermissionData.postIspaketleri]}
                component={IsPaketiCreateAndUpdate}
              />
              <PrivateRoute
                path="/proje/:pid/ispaketi/update/:id"
                permissionKeys={[PermissionData.putIspaketleriId]}
                component={IsPaketiCreateAndUpdate}
              />
              
              
              <PrivateRoute
                path="/proje/:pid/ispaketi"
                permissionKeys={[PermissionData.getIspaketleri]}
                component={IsPaketiListPage}
              />
              <PrivateRoute
                path="/proje/create"
                permissionKeys={[]}
                component={ProjeCreateUpdatePage}
              />
              <PrivateRoute
                path="/proje/update/:id"
                permissionKeys={[PermissionData.getProjeId]}
                component={ProjeCreateUpdatePage}
              />
              <PrivateRoute
                path="/proje"
                permissionKeys={[PermissionData.getProje]}
                component={ProjeListPage}
              />

              <PrivateRoute
                path="/users/create"
                permissionKeys={[PermissionData.postUsers]}
                component={UsersCreateAndUpdate}
              />
              <PrivateRoute
                path="/users/update/:id"
                permissionKeys={[PermissionData.putUsersId]}
                component={UsersCreateAndUpdate}
              />
              <PrivateRoute
                path="/users"
                permissionKeys={[PermissionData.getUsers]}
                component={UsersListPage}
              />
           
 

              <PrivateRoute
                path="/permission/create"
                permissionKeys={[]}
                component={PermissionCreateAndUpdate}
              />
              <PrivateRoute
                path="/permission/update/:id"
                permissionKeys={[]}
                component={PermissionCreateAndUpdate}
              />
              <PrivateRoute
                path="/permission"
                permissionKeys={[]}
                component={PermissionListPage}
              />


              <PrivateRoute
                path="/role/create"
                permissionKeys={[]}
                component={RoleCreateAndUpdate}
              />
              <PrivateRoute
                path="/role/update/:id"
                permissionKeys={[]}
                component={RoleCreateAndUpdate}
              />
              <PrivateRoute
                path="/role"
                permissionKeys={[]}
                component={RoleListPage}
              />
              
              <PrivateRoute
                path="/pdks"
                permissionKeys={[]}
                component={PdksImportPage}
              />


              <Route component={LoginPage} />
              
            </Switch>

          </Router>
          
        </div>
      </div>
    );
  }
}



export default App;
