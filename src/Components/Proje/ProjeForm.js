import React, {Component} from 'react'
import { Grid, TextField, MenuItem } from '@material-ui/core';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
  } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import trLocale from "date-fns/locale/tr";
import {projeTurService, projeAlanService, musteriService} from '../../Services'
import InputMask from 'react-input-mask'
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';


export default class ProjeForm extends Component {
    state = {
        projeTurleri : [],
        projeAlanlari : [],
        musteriler: [],
        getMusteri:{},
        ekipler: [],
    }

    componentDidMount() {
        projeTurService.getAll().then(result => this.setState({projeTurleri:result.data}))
        projeAlanService.getAll().then(result => this.setState({projeAlanlari:result.data}))
        musteriService.getAll().then(result => {this.setState({musteriler:result.data});this._findMusteri()})
        musteriService.getEkipler(this.props.musteriId).then(result => {this.setState({ekipler:result.data}); })
    }

    _findMusteri = () => {
        let musteri = this.state.musteriler.filter(m => {
            if (m.id == this.props.musteriId) {
                // alert(JSON.stringify(m));
                return m;
            }
        })
        this.setState({ getMusteri: musteri[0] });
        console.log(this.state.getMusteri)
    } 

    render() {
        const {data, handleChange} = this.props; 


        let SunulanButceStyle = {}

        var SunulanButce=String(data.SunulanButce)  
        SunulanButce=SunulanButce.replace(" ","") 
        var Onaylanan=String(data.Onaylanan)  
        Onaylanan=Onaylanan.replace(" ","")  
        if(parseInt(SunulanButce)  <= parseInt(Onaylanan)){
            SunulanButceStyle={
                border:"4px groove red"
            }
        } 
        return(
            <div>
                <MuiPickersUtilsProvider locale={trLocale} utils={DateFnsUtils}>
                <Grid container spacing={3}>
                    <Grid item xs={6}><TextField fullWidth id="proje-adi" label="Proje Adı" defaultValue={data.ProjeAdi} onChange = {handleChange('ProjeAdi')}/></Grid>
                    <Grid item xs={6}><TextField fullWidth id="proje-kodu" label="Proje Kodu" defaultValue={data.ProjeKodu} onChange = {handleChange('ProjeKodu')}/></Grid>

                    <Grid item xs={3}>
                         {this.props.musteriId? 
                            <div>
                            <strong style={{fontSize:18}}>Firma:  </strong>
                            {this.state.getMusteri.FirmaAdi}
                        </div>
                         :           
                        <TextField fullWidth id="proje-musteri" select label="Musteri" value={data.MusteriId} onChange={handleChange('MusteriId')}
                            SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                            {this.state.musteriler.map(m => (
                                <MenuItem key={m.id} value={m.id}> {m.FirmaAdi} </MenuItem>
                            ))}
                        </TextField>
                        } 
                    </Grid>
                    <Grid item xs={3}>
                        <TextField fullWidth id="proje-turu" select label="Proje Turu" value={data.ProjeTuru} onChange={handleChange('ProjeTuru')}
                            SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                            {this.state.projeTurleri.map(tur => (
                                <MenuItem key={tur.id} value={tur.id}> {tur.ProjeTurAdi} </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={3}>
                        <TextField fullWidth id="proje-alani" select label="Proje Alani" value={data.ProjeAlani} onChange={handleChange('ProjeAlani')}
                            SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                            {this.state.projeAlanlari.map(tur => (
                                <MenuItem key={tur.id} value={tur.id}> {tur.ProjeAlanAdi} </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={3}>
                        <TextField fullWidth id="ekipler" select label="Ekipler" value={data.EkipId} onChange={handleChange('EkipId')}
                            SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                            {this.state.ekipler.map(ekip => (
                                <MenuItem key={ekip.id} value={ekip.id}> {ekip.EkipAdi} </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                     
                    <Grid item xs={3}> 
                        <KeyboardDatePicker
                            margin="normal"
                            id="proje-baslangic-tarihi"
                            label="Proje Baslangic Tarihi"
                            format="MM/dd/yyyy"
                            value={data.ProjeBaslangicTarihi}
                            onChange={handleChange('ProjeBaslangicTarihi')}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                            />
                    </Grid>
                    <Grid item xs={3}>
                    
                        <KeyboardDatePicker
                            margin="normal"
                            id="proje-bitis-tarihi"
                            label="Proje Bitis Tarihi"
                            format="MM/dd/yyyy"
                            value={data.ProjeBitisTarihi}
                            onChange={handleChange('ProjeBitisTarihi')}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                            />
                    </Grid>
                    <Grid item xs={3}> 
                        <KeyboardDatePicker
                            margin="normal"
                            id="proje-yazim-bas-tarihi"
                            label="Proje Yazim Baslangic Tarihi"
                            format="MM/dd/yyyy"
                            value={data.ProjeYazimBaslangicTarihi}
                            onChange={handleChange('ProjeYazimBaslangicTarihi')}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                            />
                    </Grid>
                    <Grid item xs={3}></Grid>

                    <Grid item xs={6}>  
                        <InputMask
                            id="proje-sunulan-butce"  
                            style={{ padding: 10,paddingRight:5, width: "100%",textAlign:"right" ,  ...SunulanButceStyle }}
                            placeholder="Sunulan Butce"
                            mask="999 999 999 999 999 999"
                            maskChar={null}
                            value={data.SunulanButce} 
                            onChange={handleChange('SunulanButce')}
                        /> 
                        </Grid>
                    <Grid item xs={6}> 
                        <InputMask
                            id="proje-onaylanan-butce"   
                            style={{ padding: 10,paddingRight:5, width: "100%",textAlign:"right" }}
                            placeholder="Onaylanan Butce" 
                            mask="999 999 999 999 999 999"
                            maskChar={null}
                            value={data.Onaylanan} 
                            onChange={handleChange('Onaylanan')}
                        /> 
                        </Grid>

                </Grid>
                </MuiPickersUtilsProvider>
            </div>)
    }
}