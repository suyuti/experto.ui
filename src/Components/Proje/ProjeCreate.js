import React, {Component} from 'react'
import { Paper, Grid } from '@material-ui/core';
import { Formik, Form, Field } from "formik";
import { TextField } from "material-ui-formik-components/TextField";
import {  MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';

const DatePickerField = ({ field, form, ...other }) => {
    const currentError = form.errors[field.name];
    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <KeyboardDatePicker
                clearable
                disablePast
                name={field.name}
                value={field.value}
                format="DD/MM/YYYY"
                helperText={currentError}
                error={Boolean(currentError)}
                onError={(_, error) => form.setFieldError(field.name, error)}
                onChange={date => date && form.setFieldValue(field.name, date, true)}
                {...other}
                animateYearScrolling      />
      </MuiPickersUtilsProvider>
    );
  };

class ProjeCreate extends Component {
    render() {
        return(
            <div>
                <Paper>
                    <Formik
                        render={({submitForm, values, status, errors, isSubmitting}) => (
                            <Form>
                                <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <Field name="ProjeAdi" type="text" label="Proje Adi" component={TextField}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field name="ProjeKodu" type="text" label="Proje Kodu" component={TextField}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field name="ProjeTuru" type="text" label="Proje Turu" component={TextField}/>
                                    </Grid>
                                    <Grid item container justify="center" xs={12} spacing={3}>
                                        <Field name="ProjeBaslangicTarihi" label="Proje Başlangıç Tarihi" component={DatePickerField} />
                                        <Field name="ProjeBitisTarihi" label="Proje Bitiş Tarihi" component={DatePickerField} />
                                    </Grid>                                    
                                    <Grid item xs={12}>
                                        <Field name="ProjeYazimBaslangicTarihi" type="text" label="Proje Yazım Başlangıç Tarihi" component={TextField}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field name="ProjeAlani" type="text" label="Proje Alanı" component={TextField}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field name="SunulanProjeButcesi" type="text" label="Sunulan Proje Bütçesi" component={TextField}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field name="OnaylananProjeButcesi" type="text" label="Onaylanan Proje Bütçesi" component={TextField}/>
                                    </Grid>
                                </Grid>
                            </Form>
                        )}
                    ></Formik>
                </Paper>
            </div>
        )
    }
}

export default ProjeCreate;