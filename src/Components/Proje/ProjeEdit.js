import React, { Component } from "react";
import {
    Paper,
    Tabs,
    Tab,
 
    Button
} from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import SwipeableViews from "react-swipeable-views";
import { musteriService } from "../../Services";
import {
    fieldToTextField,
    TextField,

    Switch,
  } from 'formik-material-ui';
function TabContainer({ children, dir }) {
    return (
        <div style={{ padding: 8 * 3 }}>
            {children}
        </div>
    );
}

var self;
class ProjeEdit extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            value: 0,
        };
    }

    handleTabChange(e, v) {
        self.setState({ value: v });
    }
    handleSwipeViewChange(i) {
        self.setState({ value: i });
    }
    render() {
        const {classes, ...props} = this.props
        return (
            <div>
                <Paper>
                    <Formik
                        onSubmit={(values, { setSubmitting }) => {
                            //console.log(self.props.musteriId)
                            console.log(values)
                            musteriService
                                .addProje(this.props.musteriId, values)
                                .then(setSubmitting(false));
                        }}
                        render={({
                            submitForm,
                            values,
                            status,
                            errors,
                            isSubmitting
                        }) => (
                            <Form>
                                <Tabs
                                    value={this.state.value}
                                    onChange={this.handleTabChange}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    variant="fullWidth"
                                >
                                    <Tab label="Proje Bilgileri" />
                                    <Tab label="Calisan Personeller" />
                                </Tabs>
                                <SwipeableViews
                                    axis="x"
                                    index={this.state.value}
                                    onChangeIndex={this.handleSwipeViewChange}
                                >
                                    <TabContainer>
                                        <Field
                                            name="ProjeAdi"
                                            type="text"
                                            label="Proje Adi"
                                            component={TextField}
                                        />
                                        <Field
                                            name="ProjeKodu"
                                            type="text"
                                            label="Proje Kodu"
                                            component={TextField}
                                        />
                                    </TabContainer>
                                    <TabContainer />
                                </SwipeableViews>
                                <br />
                                <Button
                                    variant="contained"
                                    color="primary"
                                    disabled={isSubmitting}
                                    onClick={submitForm}
                                >
                                    Kaydet
                                </Button>
                            </Form>
                        )}
                    />
                </Paper>
            </div>
        );
    }
}

export default ProjeEdit;
