import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';

class ProjeTurCreatePage extends Component {
    render() {
        const { data, handleChange } = this.props;
        return ( 
                <div >
                    <TextField
                        id="standard-name"
                        label="Proje Tür Adı"
                        className=''
                        value={data.ProjeTurAdi}
                        onChange={handleChange('ProjeTurAdi')}
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </div>  
            );
    }
}

export default ProjeTurCreatePage;
