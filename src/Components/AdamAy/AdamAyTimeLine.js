// import React, { Component } from 'react'
// import Timeline from 'react-timelines'
// import InputLabel from '@material-ui/core/InputLabel';
// import MenuItem from '@material-ui/core/MenuItem'; 
// import FormControl from '@material-ui/core/FormControl';
// import Select from '@material-ui/core/Select';

// import OranEditForm from './OranEditForm'

// import 'react-timelines/lib/css/style.css'

// import { START_YEAR, NUM_OF_YEARS, NUM_OF_TRACKS } from './Timeline/constants'

// import { buildTimebar, buildTrack, _buildTrack, buildElements } from './Timeline/builders'

// import { fill, getCurrentDate } from './Timeline/utils'
// import { musteriService } from '../../Services';
// import CircularProgress from '@material-ui/core/CircularProgress';
// const now = new Date()

// const timebar = buildTimebar()

// // eslint-disable-next-line no-alert
// const clickElement = element => {!element.toplamMi && alert(`Clicked element\n${JSON.stringify(element, null, 2)}`)}

// const MIN_ZOOM = 1
// const MAX_ZOOM = 3

// class AdamAyTimeLine extends Component {
//   constructor(props) {
//     super(props)
//     this.state = {
//       showOranEditForm: false,
//       selectedOranValue: 0,
//       open: false,
//       ready:false,
//       zoom: 2,
//       tracksById:{},
//       musteri: props.musteri,
//       gosterimSekli:1,//  1: projepersonel    2:personelproje
//       tracks:  []//Object.values(tracksById),
      
//     }
//   }

//   //const tracksById = 



//   convertData(data) {
//     var obj = data[0]
//     var tracks = []
//     var tracksById ={}
//     for (var k in obj.personeller) {
//       var p = obj.personeller[k]
//       const t = buildTrack(p)
//       tracks.push(t)
//       tracksById[t.id] = t
//     }
// //    obj.personeller.map(p => {
// //        const t = buildTrack(p)
// //        tracks.push(t)
// //        tracksById[t.id] = t
// //    }) 
//     this.setState({tracks:tracks, tracksById:tracksById})
//   }

//   _convertPersonelProje = (aylikSonuclar) => {
//     var byPersonel = aylikSonuclar.reduce((a, b) => {
//       b.isPaketleri.forEach(ip => {
//         var x = a[ip.personelId] || (a[ip.personelId] = {})
//         x.personelId = ip.personelId
//         x.personelAdi = ip.personelAdi
//         x.oranlar = x.oranlar || []
//         var y = x.oranlar.filter(f => (f.ay === b.ay && f.yil === b.yil))
//         if (y.length > 0) {
//           var totalVal = parseFloat(y[0].oran)
//           totalVal += parseFloat(ip.oran)
//           y[0].oran = totalVal.toFixed(2)
//         }
//         else {
//           x.oranlar.push({ ay: b.ay, yil: b.yil, oran: ip.oran })
//         }

//         x.projeler = x.projeler || []
//         var projePart = x.projeler.filter(p => (p.id === ip.projeId))
//         if (projePart.length > 0) {
//           projePart[0].oranlar.push({ ay: b.ay, yil: b.yil, oran: ip.oran })
//         }
//         else {
//           x.projeler.push({ id: ip.projeId, projeAdi: 'Proje-' + ip.projeId, oranlar: [{ ay: b.ay, yil: b.yil, oran: ip.oran }] })
//         }

//       });
//       return a
//     }, {})
//     return [{personeller: byPersonel}];
//   }

//   _convertProjePersonel = (aylikSonuclar) => {
//     var byPersonel = aylikSonuclar.reduce((a, b) => {
//       b.isPaketleri.forEach( (ip, index)=> {
//         var x = a[ip.projeId] || (a[ip.projeId] = {})
//         x.personelId = ip.projeId
//         x.personelAdi = ip.MusteriAdi || 'Proje-' + ip.projeId;
//         x.oranlar = x.oranlar || []
//         x.projeler = x.projeler || []


//         var y = x.oranlar.filter(f => (f.ay === b.ay && f.yil === b.yil))
//         if (y.length > 0) {
//           var totalVal = parseFloat(y[0].oran)
//           totalVal += parseFloat(ip.oran)
//           y[0].oran = totalVal.toFixed(2)
//         }
//         else {
//           x.oranlar.push({ ay: b.ay, yil: b.yil, oran: ip.oran })
//         }

    
//         var projePart = x.projeler.filter(p => (p.id === ip.personelId))
//         if (projePart.length > 0) {
//           projePart[0].oranlar.push({ ay: b.ay, yil: b.yil, oran: ip.oran })
//         }
//         else {
//           x.projeler.push({ id: ip.personelId, projeAdi: ip.personelAdi, oranlar: [{ ay: b.ay, yil: b.yil, oran: ip.oran }] })
//         }

//       });
//       return a
//     }, {})
//     return [{personeller: byPersonel}];
//   }

//   componentDidMount() {
//       musteriService.adamAyHesapla(this.state.musteri.id).then(result => {

//           this.setState({resultData:result.data})
//           if (this.state.gosterimSekli==1) {
//             var gosterilecekData=this._convertPersonelProje(this.state.resultData[0].aylikSonuclar)
//           }else if (this.state.gosterimSekli==2) {
//             var gosterilecekData=this._convertProjePersonel(this.state.resultData[0].aylikSonuclar)
//           }
          
//           this.convertData(gosterilecekData)
//           this.setState({ready:true})

//           })
//   }

//   handleToggleOpen = () => {
//     this.setState(({ open }) => ({ open: !open }))
//   }

//   handleZoomIn = () => {
//     this.setState(({ zoom }) => ({ zoom: Math.min(zoom + 1, MAX_ZOOM) }))
//   }

//   handleZoomOut = () => {
//     this.setState(({ zoom }) => ({ zoom: Math.max(zoom - 1, MIN_ZOOM) }))
//   }

//   handleOranEditForm = (item) => {
//       //alert(JSON.stringify(item, null, 2))
//       if (item.toplamMi === false) {
//         this.setState({showOranEditForm:true, selectedOranValue:item.title})
//     }
//   }

//   handleToggleTrackOpen = track => {
//     this.setState(state => {
//       const tracksById = {
//         ...state.tracksById,
//         [track.id]: {
//           ...track,
//           isOpen: !track.isOpen,
//         },
//       }
//       return {
//         tracksById,
//         tracks: Object.values(tracksById),
//       }
//     })
//   }

//   onOranEditFormChange = () => {
//       this.setState({showOranEditForm:false})
//   }

//   handleChangeSelectBox=(event)=>{
//     this.setState(oldValues => ({
//       ...oldValues,
//       [event.target.name]: event.target.value,
//     })); 

//     if (event.target.value==1) {
//       var gosterilecekData=this._convertPersonelProje(this.state.resultData[0].aylikSonuclar)
//     }else if(event.target.value==2) {
//       var gosterilecekData=this._convertProjePersonel(this.state.resultData[0].aylikSonuclar)
//     }
    
//     this.convertData(gosterilecekData)
//     this.setState({ready:true})

//   }
 
//   render() {
    

//     const { open, zoom, tracks } = this.state
//     const start = new Date(`${START_YEAR}`)
//     const end = new Date(`${START_YEAR + NUM_OF_YEARS}`)
//     if (this.state.ready === false) return (<CircularProgress />)
//     return (
//       <div className="app">
 
//       <FormControl style={{display:"block",textAlign:"right"}} > 
//         <div > <strong>Gösterim Şeklini Seçiniz</strong> 
//         <Select
//         style={{marginLeft:8}}
//           value={this.state.gosterimSekli}
//           onChange={this.handleChangeSelectBox}
//           inputProps={{
//             name: 'gosterimSekli',
//             id: 'gosterim',
//           }}
//         >
//           <MenuItem value={1}>   Personel Proje          </MenuItem>
//           <MenuItem value={2}>   Proje Personel        </MenuItem>
//         </Select>
//         </div>
//       </FormControl>


//         <Timeline
//           scale={{
//             start,
//             end,
//             zoom,
//             zoomMin: MIN_ZOOM,
//             zoomMax: MAX_ZOOM,
//             "zoomMin":<InputLabel htmlFor="gosterim">Gosterim Şeklini Seçiniz</InputLabel>
//           }}
//           isOpen={open}
//           toggleOpen={this.handleToggleOpen}
//           zoomIn={this.handleZoomIn}
//           zoomOut={this.handleZoomOut}
//           clickElement={this.handleOranEditForm}
//           clickTrackButton={track => {
//             // eslint-disable-next-line no-alert
//             alert(JSON.stringify(track))
//           }}
//           timebar={timebar}
//           tracks={tracks}
//           now={now}
//           toggleTrackOpen={this.handleToggleTrackOpen}
//           enableSticky
//           scrollToNow
//         />
//         {this.state.showOranEditForm && <OranEditForm open={this.state.showOranEditForm} onChange={this.onOranEditFormChange} value={this.state.selectedOranValue}/>}
//       </div>
//     )
//   }
// }

// export default AdamAyTimeLine