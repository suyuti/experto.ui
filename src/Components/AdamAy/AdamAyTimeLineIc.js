import React, { Component } from 'react'
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import {
   MuiPickersUtilsProvider,
   KeyboardDatePicker,
   DatePicker
} from '@material-ui/pickers';
import Tooltip from '@material-ui/core/Tooltip';
import DateFnsUtils from '@date-io/date-fns';
import { TextField, Grid, Paper, Button } from '@material-ui/core'
import trLocale from "date-fns/locale/tr";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slider from '@material-ui/core/Slider';
import Switch from '@material-ui/core/Switch';
import OranEditForm from "./OranEditForm"
import IsPaketiColor from "./IsPaketiColor" 

import { musteriService ,isPaketiService} from '../../Services';
import CircularProgress from '@material-ui/core/CircularProgress';
import { async } from 'q';

import AdamAyAyGosterimi from "./AdamAyAyGosterimi"

class AdamAyTimeLine extends Component {
   constructor(props) {
      super(props)
      var START_Date = new Date();
      START_Date.setMonth(START_Date.getMonth() - 5);//12
      var END_Date = new Date();
      END_Date.setMonth(END_Date.getMonth() + 5);//12
      // const start = new Date(`${START_YEAR}`)
      // const end = new Date(`${END_YEAR}`)

      this.state = {
         showOranEditForm: false,
         selectedOranValue: 0,
         open: false,
         ready: false,
         zoom: 2,
         tracksById: {},
         musteri: props.musteri,
         gosterimSekli: 2,//  1: projepersonel    2:personelproje
         tracks: [],//Object.values(tracksById),
         startDate: START_Date,
         endDate: END_Date,
         canvertData: [],
         openTr: null,
         data: [],
         getProjects: [],
         getPersoneller: [],
         openOranChangeModel:false,
         popupData:{},
         projeIspaketleri:{},
         openAdamAyAyGosterimiModel:false
      }



   };
 
   async componentWillMount() {
      this.setState({ ready: false })  
      await  musteriService.getProjects(this.state.musteri.id).then(result => {
         this.setState({ getProjects: result.data })
      });
      await  musteriService.getPersoneller(this.state.musteri.id).then(result => {
         this.setState({ getPersoneller: result.data })
      }) 

      await this._fetchData();
   }

   componentDidMount() {
      // this.setState({ ready: true })
   }
   _fetchData = async () => {
      this.setState({ ready: false })

      await isPaketiService.getAll(this.state.musteri.id).then(result => {
         this.setState({ projeIspaketleri: result.data })
      })

     
      await  musteriService.adamAyHesapla(this.state.musteri.id, this.state.startDate, this.state.endDate).then(result => {
         this.setState({ data: result.data })
          
         this.setState({ ready: true })
      })
      
   }
   _isPaketiProjePersonel(projeIsPaketleri, id) {
      for (let i = 0; i < projeIsPaketleri.length; i++) {
         let paket = projeIsPaketleri[i].IsPaketiId;
         if (projeIsPaketleri[i].id == id) {
            return `ip${paket}`;
         }
      }
   }
   _isPaketiProjePersonel2(projeIsPaketleri, Proje_Id, ay) {
      // for (let i = 0; i < projeIsPaketleri.length; i++) {
      //    let paket = projeIsPaketleri[i].IsPaketiId;
      //    if (projeIsPaketleri[i].ProjeId == Proje_Id && projeIsPaketleri[i].Ay == ay) {
      //       return `ip${paket}`;
      //    }
      // }
   }
   _isPaketiPersonelProje(isPaketleri, id) {
      for (let i = 0; i < isPaketleri.length; i++) {
         let paket = isPaketleri[i].IsPaketiId;
         if (isPaketleri[i].id == id) {
            return `ip${paket}`;
         }
      }
   }
   _isPaketiPersonelProje2(isPaketleri, Personel_Id) {
      // for (let i = 0; i < isPaketleri.length; i++) {
      //    let paket = isPaketleri[i].IsPaketiId;
      //    if (isPaketleri[i].PersonelId == Personel_Id) {
      //       return `ip${paket}`;
      //    }
      // }
   }


_getProjePersonelColor(IsPaketiId) {
      if (IsPaketiId) {
         var r = this.state.projeIspaketleri
            .filter(function (item) { return  item.id   ==  IsPaketiId
            })  
            return `ip${r[0].IsPaketiColor}`;
      } 
   }

   _getPersonelProjeColor(personelisPaketleri, ispaketi) {
      // if (ispaketi) {
      //    var r = this.state.data
      //       .filter(function (item) {
      //          if (item.Ay <= ispaketi.Ay && item.Yil == ispaketi.Yil && item.PersonelId == ispaketi.PersonelId) {
      //             return item
      //          }
      //          else if (item.Yil < ispaketi.Yil && item.PersonelId == ispaketi.PersonelId) {
      //             return item
      //          } 
      //       }).map(function (itemB) { return  itemB.IsPaketiId  })

      //       var count = [...new Set(r)];

      //    if (count.length > 0) {

      //       return `ip${count.length}`;

      //    } else {
      //       return ` `;
      //    }
         
      // }
   }


   _totalMountCount(arr, Ay, Yil) {
      var r = arr
         .filter(function (item) { return item.Ay == Ay && item.Yil == Yil; })
         .map(function (itemB) { return itemB.Oran; })
         .reduce(function (acc, score) { return acc + score; }, 0);
      return r
   }
 
/*
   _getMountCount(arr, Ay, Yil) {
      var r = arr
         .filter(function (item) { return item.Ay == Ay && item.Yil == Yil; })
         .map(function (itemB) { return itemB.Oran; })
         .reduce(function (acc, score) { return acc + score; }, 0);
      return r
   }*/

   _getProjectName(id) {
      if(id==undefined || !this.state)return
      var r = this.state.getProjects.filter(function (item) { return item.id == id })
      if (r.length >= 1) return r[0].ProjeAdi
      else return ""
   }
   _getPersonelName(id) {
      if(id==undefined || !this.state)return
      var r = this.state.getPersoneller.filter(function (item) { return item.id == id })
      if (r.length >= 1) return r[0].Adi + " " + r[0].Soyadi
      else return ""
   }
   _renderResultRowsHeaderYears() {
      var headerYears = [];//yıllar için başlık thsi
      var Years = this.state.endDate.getFullYear() - this.state.startDate.getFullYear() + 1 //ne kadar yıl var
      var colspan = null //yıl için birleştirmem gereken hücre sayısı
      for (let index = 0; index < Years; index++) {
         if (index == 0) { colspan = 12 - this.state.startDate.getMonth() } // yıl başta ise proje tarihinden 12 inci aya kadar birleştirir
         else if (index == Years - 1) { colspan = this.state.endDate.getMonth() + 1 } //sonda ise birinci aydan proje bitiş tarihine kadar birleşrtirir
         else {
            colspan = 12 //ortalarda ise 12 ayı birleştirir
         }
         headerYears.push(<th colSpan={colspan}>{this.state.startDate.getFullYear() + index}</th>);
      }
      return (<tr><th> </th> {headerYears} </tr>)
   }

   _renderResultRowsHeaderMounth() {
      var headerMounth = [];
      var headerYears = [];
      var mounth = (this.state.endDate.getFullYear() - this.state.startDate.getFullYear()) * 12 + ((this.state.endDate.getMonth() + 1) - (this.state.startDate.getMonth() + 1)) + 1
      for (let index = 0; index < mounth; index++) {
         headerMounth.push(<th>{((this.state.startDate.getMonth() + index) % 12) + 1}</th>);
      }
      return (<tr><th> </th> {headerMounth} </tr>)
   }

   _createProjePersonelContent() {
      var TempConvert = {}
      var contentTD = [];
      var contentTR = [];
      var countindex = 0

      //personel id grupladım tek bi dizi oluşturdum
      //var Personel_Id_listesi = [...new Set(this.state.data.map(item => item.PersonelId))];
      ///################
      var ProjeId_listesi = [...new Set(this.state.data.map(item => item.ProjeId))];

      for (let index = 0; index < ProjeId_listesi.length; index++) {
         const Proje_Id = ProjeId_listesi[index];

         //personelin iş paketlerini getirdim
         //var personelIsPaketleri = this.state.data.filter(item => { return item.PersonelId == Personel_Id })
         ///################
         var projeIsPaketleri = this.state.data.filter(item => { return item.ProjeId == Proje_Id })
         



         console.log(Proje_Id + "projeIsPaketleri")
         console.log(projeIsPaketleri)
         var mounthCount = (this.state.endDate.getFullYear() - this.state.startDate.getFullYear()) * 12 + ((this.state.endDate.getMonth() + 1) - (this.state.startDate.getMonth() + 1)) + 1
         contentTD = []
         contentTD.push(<td className={this.state.openTr == Proje_Id ? "stictTable bgStictTableGrey" : "stictTable bgStictTableWhite"} title={this._getProjectName(Proje_Id)} >
            {this._getProjectName(Proje_Id)}
         </td>);
         for (let index1 = 0; index1 < mounthCount; index1++) {

            // var TempConvertAy = ((this.state.startDate.getMonth() + index1 + 1) % 12);
            // var TempConvertYil = this.state.startDate.getFullYear() + Math.floor((this.state.startDate.getMonth() + index1) / 12);

            var TempConvertAy = ((this.state.startDate.getMonth() + index1) % 12) + 1;
            var TempConvertYil = this.state.startDate.getFullYear() + Math.floor(((this.state.startDate.getMonth() + index1 ) / 12));
          

            TempConvert = {
               id: Proje_Id,
               Yil: TempConvertYil,
               Ay: TempConvertAy,
               Oran: this._totalMountCount(projeIsPaketleri, TempConvertAy, TempConvertYil)
            }
            
            contentTD.push(<td key={countindex++} className={this.state.openTr == Proje_Id ? "bgStictTableGrey" : ""} >
               <div className={TempConvert.Oran ? this._isPaketiProjePersonel2(projeIsPaketleri, TempConvert.id, TempConvert.Ay,TempConvert.Yil) : ""}>
                  {TempConvert.Oran ? TempConvert.Oran / 10 : ""}
               </div>
            </td>);
         }
         //Gruplanacak Başliklar 
         contentTR.push(
            <tr key={countindex++}
               onClick={() => this._trClick(Proje_Id)} >
               {contentTD}
            </tr>);
         console.log("contentTR");
         console.log(contentTR);
         //convert.push("convert") 
         // convert.push(convert) 
         // var currentPersonelProjeIds = [...new Set(personelIsPaketleri.map(item => item.ProjeId))];//personel id grupladım tek bi dizi oluşturdum
         ///################
         var currentProjePersonelIds = [...new Set(projeIsPaketleri.map(item => item.PersonelId))];//personel id grupladım tek bi dizi oluşturdum

         for (let currentPersonelId = 0; currentPersonelId < currentProjePersonelIds.length; currentPersonelId++) {

            contentTD = [] /// projeleri ekledigimiz yer
            //personelin adı soyadının eklendigi yer
            contentTD.push(<td className="  text-right stictTable altCategory " title={this._getPersonelName(currentProjePersonelIds[currentPersonelId])}>  {this._getPersonelName(currentProjePersonelIds[currentPersonelId])} </td>);

            for (let index2 = 0; index2 < mounthCount; index2++) {
               // var Ay = ((this.state.startDate.getMonth() + index2 + 1) % 12)
               // var Yil = this.state.startDate.getFullYear() + Math.floor((this.state.startDate.getMonth() + index2) / 12)

               var Ay = ((this.state.startDate.getMonth() + index2) % 12) + 1;
               var Yil = this.state.startDate.getFullYear() + Math.floor(((this.state.startDate.getMonth() + index2 ) / 12));
          
               
               /*  TempConvert = personelIsPaketleri.filter(function (item) {
                    return item.Ay == Ay && item.Yil == Yil && item.ProjeId == currentPersonelProjeIds[currentProjeId];
                 })[0] || {}*/

               ///################
               TempConvert = projeIsPaketleri.filter(function (item) {
                  return item.Ay == Ay && item.Yil == Yil && item.PersonelId == currentProjePersonelIds[currentPersonelId];
               })[0] || {}

            
  
               contentTD.push(<td key={countindex++}   onClick={(e) => {  this._showPopup(e ); }} >
                  <div 
                  className={`${TempConvert.Oran && this._getProjePersonelColor( TempConvert.IsPaketiId) } ${TempConvert.readOnly&&"adamayManualUpdate"}`} 
                  data-pg={JSON.stringify(TempConvert) } >
                     {TempConvert.Oran ? TempConvert.Oran / 10 : ""}
                  </div>
               </td>);
            }
            //Gruplanacak Başliklar 
            contentTR.push(<tr key={countindex++}
               className={this.state.openTr == Proje_Id ? "altCategory" : "altCategory  none"} >
               {contentTD}
            </tr>);
         }
      }
      return contentTR
   }

   _createPersonelProjeContent() {
      var TempConvert = {}
      var contentTD = [];
      var contentTR = [];
      var countindex = 0
      //personel id grupladım tek bi dizi oluşturdum
      var Personel_Id_listesi = [...new Set(this.state.data.map(item => item.PersonelId))];

      for (let index = 0; index < Personel_Id_listesi.length; index++) {
         const Personel_Id = Personel_Id_listesi[index];

         //personelin iş paketlerini getirdim
         var personelIsPaketleri = this.state.data.filter(item => { return item.PersonelId == Personel_Id })

         console.log(Personel_Id + "personelIsPaketleri")
         console.log(personelIsPaketleri)
         var mounthCount = (this.state.endDate.getFullYear() - this.state.startDate.getFullYear()) * 12 + ((this.state.endDate.getMonth() + 1) - (this.state.startDate.getMonth() + 1)) + 1
         contentTD = []
         contentTD.push(<td className={this.state.openTr == Personel_Id ? "stictTable bgStictTableGrey" : "stictTable bgStictTableWhite"} title={this._getPersonelName(Personel_Id)} >{this._getPersonelName(Personel_Id)} </td>);
         for (let index1 = 0; index1 < mounthCount; index1++) {
            // var TempConvertAy = ((this.state.startDate.getMonth() + index1 + 1) % 12);
            // var TempConvertYil = this.state.startDate.getFullYear() + Math.floor((this.state.startDate.getMonth() + index1) / 12);

            var TempConvertAy = ((this.state.startDate.getMonth() + index1) % 12) + 1;
            var TempConvertYil = this.state.startDate.getFullYear() + Math.floor(((this.state.startDate.getMonth() + index1 ) / 12));
          

            TempConvert = {
               id: Personel_Id,
               Yil: TempConvertYil,
               Ay: TempConvertAy,
               Oran: this._totalMountCount(personelIsPaketleri, TempConvertAy, TempConvertYil) 
            }
            contentTD.push(<td key={countindex++} className={this.state.openTr == Personel_Id ? "bgStictTableGrey" : ""} >
               <div className={`${TempConvert.Oran && this._isPaketiPersonelProje(personelIsPaketleri, TempConvert)} ${
                  TempConvert.Oran>10 && "dangerAdamAy" 
               } 
               `}>
                  {TempConvert.Oran ? TempConvert.Oran / 10 : ""}
               </div>
            </td>);
         }
         //Gruplanacak Başliklar 
         contentTR.push(
            <tr key={countindex++}
               onClick={() => this._trClick(Personel_Id)} >
               {contentTD}
            </tr>);
         console.log("contentTR");
         console.log(contentTR);
         //convert.push("convert") 
         // convert.push(convert) 
         var currentPersonelProjeIds = [...new Set(personelIsPaketleri.map(item => item.ProjeId))];//personel id grupladım tek bi dizi oluşturdum

         for (let currentProjeId = 0; currentProjeId < currentPersonelProjeIds.length; currentProjeId++) {
            contentTD = [] /// projeleri ekledigimiz yer
            contentTD.push(<td className=" text-right stictTable altCategory" title={this._getProjectName(currentPersonelProjeIds[currentProjeId])} >  {this._getProjectName(currentPersonelProjeIds[currentProjeId])} </td>);
            for (let index2 = 0; index2 < mounthCount; index2++) {
               // var Ay = ((this.state.startDate.getMonth() + index2 + 1) % 12)
               // var Yil = this.state.startDate.getFullYear() + Math.floor((this.state.startDate.getMonth() + index2) / 12)

               var Ay = ((this.state.startDate.getMonth() + index2) % 12) + 1;
               var Yil = this.state.startDate.getFullYear() + Math.floor(((this.state.startDate.getMonth() + index2 ) / 12));
          
               
               TempConvert = personelIsPaketleri.filter(function (item) {
                  return item.Ay == Ay && item.Yil == Yil && item.ProjeId == currentPersonelProjeIds[currentProjeId];
               })[0] || {}
               contentTD.push(<td key={countindex++} onClick={(e) => { this._showPopup(e); }} >
                  <div  
                     data-pg={JSON.stringify(TempConvert) } 
                     className={`${TempConvert.Oran && this._getProjePersonelColor( TempConvert.IsPaketiId) } ${TempConvert.readOnly&&"adamayManualUpdate"}`}>
                     {TempConvert.Oran ? TempConvert.Oran / 10 : ""}
                  </div>
               </td>);
            }
            //Gruplanacak Başliklar 
            contentTR.push(<tr key={countindex++}
               className={this.state.openTr == Personel_Id ? "altCategory" : "altCategory  none"} >
               {contentTD}
            </tr>);
         }
      }
      return contentTR
   }
   handleChangeSelectBox = (event) => {
      /*  this.setState(oldValues => ({
           ...oldValues,
           [event.target.name]: event.target.value,
        }));
  
        if (event.target.value == 1) {
           var gosterilecekData = this._convertPersonelProje(this.state.resultData[0].aylikSonuclar)
        } else if (event.target.value == 2) {
           var gosterilecekData = this._convertProjePersonel(this.state.resultData[0].aylikSonuclar)
        }
  
        this.convertData(gosterilecekData)
        this.setState({ ready: true })
  */
      this.setState(oldValues => ({
         ...oldValues,
         [event.target.name]: event.target.value,
      }));
   }
   _handleDateChange = (inputName) => value => {
      console.log(value);
      /* this.setState((oldValues) => ({
          ...oldValues,
          [inputName]: value,
       } ));
 */ 
      this.setState({
         [inputName]: value,
      }, () => {
         this._fetchData();
      }) 
   };
   _trClick = (Personel_Id) => {
      if (Personel_Id == this.state.openTr) {  this.setState({ openTr: null }) }
      else { this.setState({ openTr: Personel_Id }) }
   }
   _showPopup = (e) => { 
       let temppopupData=JSON.parse( e.target.getAttribute('data-pg'))
         //console.log( e.target.getAttribute('data-pg')); 
         if(temppopupData!=null){
         this.setState({popupData:temppopupData })
         this.setState({openOranChangeModel:true})
      }
   }
   _closePopup = (adamay) => { 
      this.setState({popupData:{}})
      this.setState({openOranChangeModel:false})
   }
   _savePopup = ()=>{  
      musteriService.adamAyUpdate(this.state.popupData).then(result => {
            var index = this.state.data.map(function(e) { return e.id; }).indexOf(this.state.popupData.id);
            this.state.data[index]=this.state.popupData
            this._closePopup(); 
      }); 
   } 
   _handlePopupDataChange = (value) => e => {
      console.log(value); 
      var temp = e.target.checked ? 1: 0
       this.setState(() => ({
         popupData: {
             ...this.state.popupData,
             [value]: temp
         }
     })); 
   };
 
   _valuetext=(value)=> { 
   this.state.popupData.Oran=value*10
   return `${value}°C`; 
   }  

   render() { 
     // const { open, zoom, tracks,popupData } = this.state  
      return (
         <div className="app">
            <MuiPickersUtilsProvider locale={trLocale} utils={DateFnsUtils} >
               <Grid container spacing={1}> 
                  <Grid item xs={12}>
                     <Paper style={{ padding: "5px" }} >
                        <Grid container spacing={1}>
                           <Grid item className="text-center" sm={3} xs={12} >
                              <DatePicker
                                 openTo="year"
                                 views={["year", "month"]}
                                 variant="inline"
                                 format="MM/yyyy"
                                 label="Baslangıç tarihi"
                                 value={this.state.startDate}
                                 onChange={this._handleDateChange('startDate')}
                              /> 
                           </Grid>
                           <Grid item className="text-center" sm={3} xs={12} >
                              <DatePicker
                                 minDate={this.state.startDate}
                                 openTo="year"
                                 views={["year", "month"]}
                                 variant="inline"
                                 format="MM/yyyy"
                                 label="Bitiş tarihi"
                                 value={this.state.endDate}
                                 onChange={this._handleDateChange('endDate')}
                              />
                           </Grid>
                           <Grid item className="text-center" sm={3} xs={12}>
                              <Button variant="contained" color="primary" onClick={() => this._fetchData()}> Guncelle</Button>
                           </Grid>
                           <Grid item className="text-center" sm={3} xs={12}>
                              <strong>Gösterim Şeklini Seçiniz</strong>
                              <Select
                                 style={{ marginLeft: 8 }}
                                 value={this.state.gosterimSekli}
                                 onChange={this.handleChangeSelectBox}
                                 inputProps={{
                                    name: 'gosterimSekli',
                                    id: 'gosterim',
                                 }} >
                                 <MenuItem value={1}>  Proje Personel            </MenuItem>
                                 <MenuItem value={2}>  Personel Proje        </MenuItem>
                              </Select>
                           </Grid> 
                        </Grid>
                     </Paper>
                  </Grid> 
                  <Grid item xs={12} style={{ overflow: "auto", textAlign: "center" }}>
                     {this.state.ready === false ? <CircularProgress /> :
                        <Paper style={{ padding: "5px", height: "100%", width: "100%" }} >
                           <table id="customers">
                              {/* TABLO BAŞLIGI */}
                              {this._renderResultRowsHeaderYears()}
                              {this._renderResultRowsHeaderMounth()} 
                              {/* TABLO İÇERİGİ */}
                              {this.state.gosterimSekli == 1 && this._createProjePersonelContent()}
                              {this.state.gosterimSekli == 2 && this._createPersonelProjeContent()}
                           </table>
                           
                        </Paper>
                     }
                  </Grid>
                  <Grid item xs={12}>
                     {/* İŞ PAKETİ RENKLERİ */}
                     <IsPaketiColor />
                  </Grid>
               </Grid>
                {/* <OranEditForm
                  openOranChangeModel={this.state.openOranChangeModel}
                  _getProjectName={this._getProjectName}
                  _getPersonelName={this._getPersonelName}
                  popupData={this.state.popupData}
                  _valuetext={this._valuetext}
                  _handlePopupDataChange={this._handlePopupDataChange}
                  _closePopup={this._closePopup}
                  _savePopup={this._savePopup}
               />  */}
               <AdamAyAyGosterimi open={this.state.openAdamAyAyGosterimiModel}/>

               <Dialog
                  fullWidth={true}
                  maxWidth="md"
                  open={this.state.openOranChangeModel}
                  onClose={this._closePopup}
                  aria-labelledby="responsive-dialog-title">
                  <DialogTitle id="responsive-dialog-title">ADAM AY MANUEL GUNCELLEME</DialogTitle>
                  <DialogContent dividers>
                     <table id="adamayChange" > 
                        <tr> <th>Proje</th>  <td>{this._getProjectName(this.state.popupData.ProjeId)}</td>  </tr>
                        <tr> <th>Personel </th>  <td>{this._getPersonelName(this.state.popupData.PersonelId)}</td>  </tr>
                        <tr> <th>Yil</th>  <td>{this.state.popupData.Yil}</td>  </tr>
                        <tr> <th>Ay</th>  <td>{this.state.popupData.Ay}</td>  </tr>
                        <tr> <th>Oran</th>
                           <td>
                              <div className="oranRow">
                                 <div style={{ flex: 1, }}> 
                                    <Slider
                                       defaultValue={this.state.popupData.Oran / 10}
                                       getAriaValueText={this._valuetext}
                                       aria-labelledby="discrete-slider-always"
                                       step={0.1}
                                       min={0}
                                       max={1}
                                       valueLabelDisplay="on" />
                                 </div>
                              </div> 
                           </td>
                        </tr>
                        
                        <tr> <th>Sabit Oran Mı?</th>
                           <td>
                              <Switch
                                 checked={this.state.popupData.readOnly}
                                 onChange={this._handlePopupDataChange("readOnly")}
                                 value="checkedA"
                                 inputProps={{ 'aria-label': 'secondary checkbox' }}
                              />
                              {this.state.popupData.readOnly}
                           </td> 
                        </tr>
                     </table> 
                  </DialogContent> 
                  <DialogActions>
                     <Button onClick={this._closePopup}
                           variant="contained"
                           color="primary"
                           style={{ background: "#dc3545" }}
                           > IPTAL </Button>
                     <Button onClick={this._savePopup} 
                              variant="contained"
                              color="primary"
                              style={{ background: "#28a745" }}
                              autoFocus> KAYDET </Button>
                  </DialogActions>
               </Dialog> 


 

            </MuiPickersUtilsProvider>
         </div>
      )
   }
}

export default AdamAyTimeLine