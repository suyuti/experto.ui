import React, {Component} from 'react'
import { Button, Grid, List, ListItem, ListItemText, ListItemIcon, Checkbox, Paper } from '@material-ui/core'
import { musteriService } from '../../Services'

export default class AdamAyHesaplamaForm extends Component {
    state ={
        seciliProjeler: [],
        seciliPersoneller: [],
        projeler : [], // [{id:1, Adi:'proje 1'},{id:2, Adi:'proje 2'}, {id:3, Adi:'proje 3'}],
        personeller : []// [{id:1, Adi:'Personel 1'},{id:2, Adi:'proje 2'}, {id:3, Adi:'proje 3'}]
    }

    componentDidMount() {
        musteriService.getProjects(this.props.musteriId).then(result => this.setState({projeler:result.data}))
        musteriService.getPersoneller(this.props.musteriId).then(result => this.setState({personeller:result.data}))
    }

    render() {
        const {data, handleChange} = this.props
        return(
            <div>
                <Grid container>
                    <Grid item xs={6} container direction="column">
                        <h4>Projeler</h4>
                    <Paper style={{maxHeight: 400, overflow: 'auto'}}>
                        <List>
                            {this.state.projeler.map((p,i) => {
                                const labelId = `checkbox-list-label0${p.id}`
                                return (<ListItem key={p.id} dense button>
                                    <ListItemIcon>
                                        <Checkbox edge="start" id={p.id} onChange={handleChange('seciliProjeler')}/>
                                    </ListItemIcon>
                                    <ListItemText id={labelId} primary={`${p.ProjeAdi}`} />
                                </ListItem>)
                            })}
                        </List>
                        </Paper>
                    </Grid>
                    <Grid item xs={6} container direction="column">
                        <h4>Personeller</h4>
                    <Paper style={{maxHeight: 400, overflow: 'auto'}}>
                    <List>
                            {this.state.personeller.map(p => {
                                const labelId = `checkbox-list-label0${p.id}`
                                return (<ListItem key={p.id} dense button>
                                    <ListItemIcon>
                                    <Checkbox edge="start" id={p.id} onChange={handleChange('seciliPersoneller')}/>
                                    </ListItemIcon>
                                    <ListItemText id={labelId} primary={`${p.Adi} ${p.Soyadi}`} />
                                </ListItem>)
                            })}
                        </List>
                    </Paper>
                    </Grid>
                </Grid>
                <Button variant="contained" color="primary" onClick={this.props.onHesapla}> Hesapla</Button>
            </div>
        )
    }
} 