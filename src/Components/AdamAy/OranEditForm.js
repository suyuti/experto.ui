import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slider from '@material-ui/core/Slider';
import Switch from '@material-ui/core/Switch';
import { TextField, Grid, Paper, Button } from '@material-ui/core'

class OranEditForm extends Component {
  constructor(props) {
    super(props);
  
  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  render() {
    
    let { openOranChangeModel,_getProjectName, _getPersonelName,_valuetext,_handlePopupDataChange, popupData,_closePopup,_savePopup } = this.props  

    return (
      <div>
               <Dialog
                  fullWidth={true}
                  maxWidth="md"
                  open={openOranChangeModel}
                  onClose={_closePopup}
                  aria-labelledby="responsive-dialog-title">
                  <DialogTitle id="responsive-dialog-title">ADAM AY MANUEL GUNCELLEME</DialogTitle>
                  <DialogContent dividers>
                     <table id="adamayChange" > 
                        <tr> <th>Proje</th>  <td>{_getProjectName(popupData.ProjeId)}</td>  </tr>
                        <tr> <th>Personel </th>  <td>{_getPersonelName(popupData.PersonelId)}</td>  </tr>
                        <tr> <th>Yil</th>  <td>{popupData.Yil}</td>  </tr>
                        <tr> <th>Ay</th>  <td>{popupData.Ay}</td>  </tr>
                        <tr> <th>Oran</th>
                           <td>
                              <div className="oranRow">
                                 <div style={{ flex: 1, }}> 
                                    <Slider
                                       defaultValue={popupData.Oran / 10}
                                       getAriaValueText={_valuetext}
                                       aria-labelledby="discrete-slider-always"
                                       step={0.1}
                                       min={0}
                                       max={1}
                                       valueLabelDisplay="on" />
                                 </div>
                              </div> 
                           </td>
                        </tr>
                        
                        <tr> <th>Sabit Oran Mı?</th>
                           <td>
                              <Switch
                                 checked={popupData.readOnly}
                                 onChange={_handlePopupDataChange("readOnly")}
                                 value="checkedA"
                                 inputProps={{ 'aria-label': 'secondary checkbox' }}
                              />
                              {popupData.readOnly}
                           </td> 
                        </tr>
                     </table> 
                  </DialogContent> 
                  <DialogActions>
                     <Button onClick={_closePopup}
                           variant="contained"
                           color="primary"
                           style={{ background: "#dc3545" }}
                           > IPTAL </Button>
                     <Button onClick={_savePopup} 
                              variant="contained"
                              color="primary"
                              style={{ background: "#28a745" }}
                              autoFocus> KAYDET </Button>
                  </DialogActions>
               </Dialog> 

      </div>
    );
  }
}



export default OranEditForm;