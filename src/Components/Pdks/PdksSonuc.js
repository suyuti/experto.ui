import React, { Component } from 'react';
import { ExportCSV } from '../../Components/ExportCSV'

class PdksSonuc extends Component {
    constructor(props) {
        super(props);
        
    }
 

    componentWillReceiveProps(nextProps) {
        this.setState({data:nextProps})
    }

    // shouldComponentUpdate(nextProps, nextState) {

    // }

    // componentWillUpdate(nextProps, nextState) {

    // }

    // componentDidUpdate(prevProps, prevState) {  
    // }

    // componentWillUnmount() {

    // }

    _renderResultRowsContentMounth=()=> { 
        var contentTR = [];
        var data = this.props.data; 
        data = data.map((item) => {
            var newitem = {
                AdSoyad: item.AdSoyad,
                CalistigiGun: item.CalistigiGun,
                cumartesiHakki: item.cumartesiHakki,
                ResmiTatil: item.ResmiTatil,
                YillikIzin: item.YillikIzin,
                DisGorev: item.DisGorev,
                ArgeGunu: item.ArgeGunu
            }
            return newitem
        })
        console.log(data);

 
        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            var temp=   <tr>      
                            <td>{element.AdSoyad}  </td>
                            <td>{element.CalistigiGun}  </td>
                            <td>{element.cumartesiHakki}  </td>
                            <td>{element.ResmiTatil}  </td>
                            <td>{element.YillikIzin}  </td>
                            <td>{element.DisGorev}  </td>
                            <td>{element.ArgeGunu}  </td>
                        </tr>  
            contentTR.push(temp)   
            
        } 
        contentTR.push(
            <tr>
                <td colSpan={7}>
                <ExportCSV csvData={data} fileName="PDKS_Sonuc" butonismi="SONUC TABLOSUNU İNDİR" />
                </td>
            </tr>
        )
        return contentTR 
    }

    render() {  
        return (<div style={{overflowX:"scroll"}}> 
                <table  id="customers" className="pdks">
                    <tr> 
                        <th>Ad Soyad  </th>
                        <th>Calistigi Gun  </th>
                        <th>Cumartesi Hakki  </th>
                        <th>Resmi Tatil  </th>
                        <th>Yillik Izin  </th>
                        <th>Dis Gorev  </th>
                        <th>Arge Gunu  </th>
                    </tr>
                    {this._renderResultRowsContentMounth()}  
                </table> 
               </div>   
        );
    }
}
 

export default PdksSonuc;