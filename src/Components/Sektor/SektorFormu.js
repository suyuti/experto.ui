import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';

class SektorCreatePage extends Component {
    render() {
        const { data, handleChange } = this.props;
     
        return ( 
            <div >
                <TextField
                    id="standard-name"
                    label="Sektor Adi"
                    className=''
                    value={data.SektorAdi}
                    onChange={handleChange('SektorAdi')}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </div>  
        );
    }
}


export default SektorCreatePage;
