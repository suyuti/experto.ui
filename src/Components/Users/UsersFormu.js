import React, { Component } from "react";
//import InputLabel from '@material-ui/core/InputLabel';
//import MenuItem from '@material-ui/core/MenuItem';
//import FormControl from '@material-ui/core/FormControl';
//import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
//import { egitimDurumuService } from "../../Services";



class GorevCreatePage extends Component {



    render() {
        const { data, handleChange } = this.props;

        return (
            <Grid container >
                <Grid item xs={12} sm={6} >

                    <TextField
                        id="standard-name"
                        label="User Adi"
                        className=''
                        value={data.UserName}
                        onChange={handleChange('UserName')}
                        margin="normal"
                        variant="standard"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
 
                </Grid> 
                <Grid item xs={12} sm={6} >
                    <TextField
                        id="standard-name"
                        label="User sifresi"
                        className=''
                        value={data.PasswordHash}
                        onChange={handleChange('PasswordHash')}
                        margin="normal"
                        variant="standard"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6} >
                    <TextField
                        id="FirstName"
                        label="First Name"
                        className=''
                        value={data.FirstName}
                        onChange={handleChange('FirstName')}
                        margin="normal"
                        variant="standard"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    </Grid>
                <Grid item xs={12} sm={6}  >
                    <TextField
                        id="LastName"
                        label="Last Name"
                        className=''
                        value={data.LastName}
                        onChange={handleChange('LastName')}
                        margin="normal"
                        variant="standard"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={6} >
                    <TextField
                        id="EMail"
                        label="EMail"
                        className=''
                        value={data.EMail}
                        onChange={handleChange('EMail')}
                        margin="normal"
                        variant="standard"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    </Grid>
                <Grid item xs={12} sm={6} >
                    <TextField
                        id="DisplayName"
                        label="Display Name"
                        className=''
                        value={data.DisplayName}
                        onChange={handleChange('DisplayName')}
                        margin="normal"
                        variant="standard"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        id="Dashboard"
                        label="Dashboard"
                        className=''
                        value={data.Dashboard}
                        onChange={handleChange('Dashboard')}
                        margin="normal"
                        variant="standard"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid>






            </Grid>
        );
    }
}

/*
const styles = {
    inputSoyadi: {
        minWidth: '150px',
        marginLeft: '10px'
    },
    EgitimDurumu: {

        marginLeft: '10px'
    }

};*/

export default GorevCreatePage;
