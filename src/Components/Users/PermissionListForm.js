import React from "react";
import MaterialTable from 'material-table';
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";

import { userService } from "../../Services";

import { checkPermission, PermissionData } from '../../command';
import CircularProgress from '@material-ui/core/CircularProgress';


import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';



class PersonelListAndSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            userRole:[],
            permissionReady: false,
        };
        this.UserId = this.props.User_Id;
    }



    componentDidMount() {
        this.fetchData();
    }

    fetchData =   () => {

        if (this.UserId) {
             userService.getRole().then(result => {
                if (result.status === 200) {
                    if (result.data != null) {
                        this.setState({ data: result.data })
                    }
                }
            });
              userService.getUserRole(this.UserId).then(result => {
                if (result.status === 200) {
                    if (result.data != null) {
                        this.setState({ userRole: result.data })
                        this.props.setRole(result.data); //usteti formdaki UsersPermission alanını  günceller
                    }
                }
            });
            
            this.setState({ permissionReady: true})
        }
    }

    _handleCheckboxChange = (role) => {
        const index = this.state.userRole.findIndex(item => item.id === role.id);
        if(index>-1) {
            const newArray =this.state.userRole.filter( item =>  item.id !== role.id  )
            this.setState({ 
                userRole:newArray
            })  
            this.props.setRole(newArray); //usteti formdaki UsersPermission alanını  günceller
        }else{
            this.setState({ 
                userRole:[
                    ...this.state.userRole,
                    role
                 ]
            }) 
            this.props.setRole([ ...this.state.userRole,role]); //usteti formdaki UsersPermission alanını  günceller
        }  
    }

    _checkRole =(role)=>{  
        const index = this.state.userRole.findIndex(item => item.id === role.id);
        if(index>-1) {
            return true
        }else{
            return false
        }
    } 
    render() {
        if (this.state.permissionReady === false) {
            return (<div style={{ textAlign: "center" }}><CircularProgress /> </div>)
        } 
        return (
            <div>
                <Grid container >
                    {this.state.data.map((item, i) => {
                        return (
                            <Grid item xs={12} sm={4} >
                                <FormControlLabel key={i}
                                    control={
                                        <Checkbox
                                            checked={this._checkRole(item)}
                                            onChange={e => this._handleCheckboxChange(item)}
                                            value="checkedB"
                                            color="primary"
                                        />
                                    }
                                    label={item.RoleName}
                                />
                            </Grid>
                        )
                    })}
                </Grid>
            </div>
        )
    }
}

const styles = {
    fab: {
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    }
};


export default withStyles(styles)(PersonelListAndSearch);
