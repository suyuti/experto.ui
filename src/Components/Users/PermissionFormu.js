import React, { Component } from 'react';
import Board from "react-trello"; 


class PermissionFormu extends Component {
    constructor(props) {
        super(props);
        this.state={
            data:{
                "lanes": [
                    {
                      "id": "PLANNED",
                      "title": "Bütün izinler",
                      "style": {
                        "width": 280
                      },
                      "cards": [
                        {
                          "id": "Milk",
                          "title": "Buy milk",
                          "label": "15 mins",
                          "description": "2 Gallons of milk at the Deli store"
                        }
                      ]
                    },
                    {
                        "id": "user",
                        "title": "Kullanıcının İzinleri",
                        "style": {
                          "width": 280
                        },
                        "cards": [
                        ]
                      }
                ]
            }
        }
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    onDataChange=(nextData)=>{
        this.setState({"data":nextData})
    }

    render() {
        return (
            <div style={{display:"block",width:"100%"}}> 
                <Board 
                    data={ this.state.data} 
                    draggable 
                    onDataChange={this.onDataChange}
                    style={{display: "block"}}
                
                />
                <text>{JSON.stringify( this.state.data)} </text>
            </div>
        );
    }
}


export default PermissionFormu;