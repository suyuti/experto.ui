import React, { Component } from "react";
import { TextField, MenuItem } from '@material-ui/core';
import { musteriService, personelService } from "../../Services";

class EkipCreatePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      musteriler: [],
      Ready: false,
      getMusteri: {}
    }
    this.musteriId = this.props.data.MusteriId;
  }

  componentDidMount() {
    musteriService.getAll().then(result => {
      this.setState({ musteriler: result.data, Ready: true });
      this._findMusteri();
    })
  }

  _findMusteri = () => {
    let musteri = this.state.musteriler.filter(m => {
      if (m.id == this.musteriId) {
        return m;
      }
    })
    this.setState({ getMusteri: musteri[0] });
  }
  render() {
    const { data, handleChange } = this.props;
    return (
      <div >
        {this.musteriId ? (
          <div>
            <strong style={{ fontSize: 18 }}>Firma:  </strong>
            {this.state.getMusteri.FirmaAdi}
          </div>
        ) : (
            <TextField
              fullWidth
              id="musteri-ekip-id"
              select label="Müşteri"
              value={data.MusteriId ? data.MusteriId : ""} onChange={handleChange('MusteriId')}
              margin="normal">
              {this.state.musteriler.map(m => (
                <MenuItem key={m.id} value={m.id}> {m.FirmaAdi} </MenuItem>
              ))}
            </TextField>
          )
        }
        <TextField
          id="standard-name"
          label="Ekip Adi"
          className=''
          value={data.EkipAdi}
          onChange={handleChange('EkipAdi')}
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
      </div>
    );
  }
}

export default EkipCreatePage;