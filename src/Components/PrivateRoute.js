import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { authenticationService } from '../Services';
import MainLayout from '../Layout/MainLayout'
import { checkPermission } from '../command';

import ErisimEngellendi from '../Pages/ErisimEngellendi';


export const PrivateRoute = ({ component: Component, roles, permissionKeys,...rest }) => (
    
    <Route {...rest} render={props => {
        const currentUser = authenticationService.currentUserValue;
        if (!currentUser) {
            // not logged in so redirect to login page with the return url
            //alert('current user yok')
            return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        }

        if ( !checkPermission(permissionKeys)) {
         // return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
          return  (
            <Route
              {...rest}
              render={matchProps => (
                <MainLayout>
                  <ErisimEngellendi/>
                </MainLayout>
              )}
            />
          );
        }
        
        return (
            <Route
              {...rest}
              render={matchProps => (
                <MainLayout>
                  <Component {...matchProps} />
                </MainLayout>
              )}
            />
          );


    }} />
)