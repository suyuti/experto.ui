import React, { Component } from "react";
import { Grid, TextField, InputLabel, Select, MenuItem, withStyles } from "@material-ui/core";

const styles = theme => ({
    textField: {}
});

class MusteriIletisimPanel extends Component {
    render() {
        const {classes, ...props} = this.props; 
        return (
            <div>
                <Grid container xs={12} spacing={1}>
                    <Grid item xs={6}>
                        <TextField
                            id="standard-name"
                            label="Telefon"
                            className={classes.textField}
                            margin="normal"
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            id="standard-name"
                            label="Mail"
                            className={classes.textField}
                            margin="normal"
                        />
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(MusteriIletisimPanel);
