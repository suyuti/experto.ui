import React, { Component } from "react";
import {
    Grid,
    TextField,
    InputLabel,
    Select,
    MenuItem,
    withStyles 
} from "@material-ui/core";

const styles = theme => ({
    textField: {}
});

class MusteriBilgiPanel extends Component {
    state = {
        sektorler: [],
        firmaTurleri: [],
        firmaDurumu: [{ id: 0, Name: "Aktif" }, { id: 1, Name: "Pasif" }]
    };

    componentDidMount() {
        // Get Sektorler
        var sektorler = [{ id: 0, Name: "Kimya" }, { id: 1, Name: "Makine" }];
        this.setState({ sektorler: sektorler });

        // Get Firma Turleri
        var firmaTurleri = [{ id: 0, Name: "Kobi" }, { id: 1, Name: "Sanayi" }];
        this.setState({ firmaTurkeri: firmaTurleri });
    }

    render() {
        const { classes, ...props } = this.props;
        return (
            <div>
                <Grid container xs={12} spacing={1}>
                    <Grid item xs={12}>
                        <div variant="h5" gutterBottom>
                            Musteri Bilgileri
                        </div>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            id="standard-name"
                            name='firmaAdi'
                            label="Firma Adi"
                            className={classes.textField}
                            margin="normal"
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            id="standard-name"
                            label="Ticari Unvani"
                            className={classes.textField}
                            margin="normal"
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <InputLabel htmlFor="age-simple">Sektor</InputLabel>
                        <Select
                            inputProps={{
                                name: "age",
                                id: "age-simple"
                            }}
                        >
                            {this.state.sektorler.map((s, i) => (
                                <MenuItem value={s.id}>{s.Name}</MenuItem>
                            ))}
                        </Select>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            id="standard-name"
                            label="Vergi Dairesi"
                            className={classes.textField}
                            margin="normal"
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            id="standard-name"
                            label="Vergi Numarasi"
                            className={classes.textField}
                            margin="normal"
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <InputLabel htmlFor="firma-durumu">
                            Firma Durumu
                        </InputLabel>
                        <Select
                            inputProps={{
                                name: "age",
                                id: "firma-durumu"
                            }}
                        >
                            {this.state.firmaDurumu.map((s, i) => (
                                <MenuItem value={s.id}>{s.Name}</MenuItem>
                            ))}
                        </Select>
                    </Grid>
                    <Grid item xs={3}>
                        <InputLabel htmlFor="firma-turu">Firma Turu</InputLabel>
                        <Select
                            inputProps={{
                                name: "age",
                                id: "firma-turu"
                            }}
                        >
                            {this.state.firmaTurleri.map((s, i) => (
                                <MenuItem value={s.id}>{s.Name}</MenuItem>
                            ))}
                        </Select>
                    </Grid>
                </Grid>
            </div>
        );
    }
}
export default withStyles(styles)(MusteriBilgiPanel);
