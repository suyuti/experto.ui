import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views";
import Typography from "@material-ui/core/Typography";
import { Paper, withStyles, Grid, Button, LinearProgress, TextField, Switch, FormControlLabel} from "@material-ui/core";
import PropTypes from "prop-types";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {bolgeService} from "../../Services"
import {sektorService} from "../../Services"

function TabContainer({ children, dir }) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired
};

var self;
class MusteriCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sektorler:      [],
            firmaTurleri:   [{id:"K", TurAdi: "Kobi"}, {id:"S", TurAdi:"Sanayi"}],
            firmaDurumu:    [{id:"A", TurAdi: "Aktif"}, {id:"P", TurAdi:"Pasif"}],
            bolgeler    :   []
        };
        self = this;
        console.log(props)
    }

    componentDidMount() {
        console.log(this.props.data)
        bolgeService.getAll().then(result => this.setState({bolgeler: result.data}))
        sektorService.getAll().then(result => this.setState({sektorler: result.data}))
    }

    render() {
        const {data, handleChange} = this.props
        return (
            <div>
                <Grid container spacing={3}>
                    <Grid item xs={6}><TextField fullWidth id="firma-adi" label="Firma Adı" defaultValue={data.FirmaAdi} onChange = {handleChange('FirmaAdi')}/></Grid>
                    <Grid item xs={6}><TextField fullWidth id="firma-ticari-unvan" label="Ticari Unvani" defaultValue={data.TicariUnvan} onChange = {handleChange('TicariUnvan')}/></Grid>
                    <Grid item xs={12}><TextField fullWidth id="firma-adresi" label="Firma Adresi" defaultValue={data.FirmaAdresi} onChange = {handleChange('FirmaAdresi')}/></Grid>

                    <Grid item xs={3}>
                        <TextField fullWidth id="firma-turu" style={styles.firmaTuru} select label="Firma Turu" value={data.FirmaTuru} onChange={handleChange('FirmaTuru')}
                            SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                            {this.state.firmaTurleri.map(tur => (
                            <MenuItem key={tur.id} value={tur.id}> {tur.TurAdi} </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={3}>
                        <TextField fullWidth id="firma-sektoru" style={styles.sektor} select label="Sektoru" value={data.SektorId} onChange={handleChange('SektorId')}
                            SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                                {this.state.sektorler.map(sektor => (
                                <MenuItem key={sektor.id} value={sektor.id}> {sektor.SektorAdi} </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={3}>

                        <FormControlLabel
                            control={
                                <Switch
                                    checked={ (data.AktifMi=="true"|| data.AktifMi==true )?true:false }
                                    onChange={handleChange('AktifMi')}
                                    value={data.AktifMi=="true"?"false":"true"}
                                    color="primary"
                                />

                                
                            }
                            label="Aktif Mi?"
                        />
 
                    </Grid>

                    <Grid item xs={3}>
                        <TextField fullWidth id="firma-bolge" select label="Bolge" value={data.BolgeId} onChange={handleChange('BolgeId')}
                            SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                                {this.state.bolgeler.map(bolge => (
                                <MenuItem key={bolge.id} value={bolge.id}> {bolge.BolgeAdi} </MenuItem>
                            ))}
                        </TextField>
                    </Grid>

                    <Grid item xs={12}><TextField fullWidth id="firma-fatura-adresi" label="Fatura Adresi" defaultValue={data.FaturaAdresi} onChange = {handleChange('FaturaAdresi')}/></Grid>
                    <Grid item xs={12}><TextField fullWidth id="firma-fabrika-adresi" label="Fabrika Adresi" defaultValue={data.FabrikaAdresi} onChange = {handleChange('FabrikaAdresi')}/></Grid>
                    <Grid item xs={4}><TextField fullWidth id="firma-firma-telefonu" label="Merkez Telefonu" defaultValue={data.FirmaTelefonu} onChange = {handleChange('FirmaTelefonu')}/></Grid>
                    <Grid item xs={4}><TextField fullWidth id="firma-vergi-dairesi" label="Vergi Dairesi" defaultValue={data.VergiDairesi} onChange = {handleChange('VergiDairesi')}/></Grid>
                    <Grid item xs={4}><TextField fullWidth id="firma-vergi-numarasi" label="Vergi Numarasi" defaultValue={data.VergiNumarasi} onChange = {handleChange('VergiNumarasi')}/></Grid>

                    <Grid item xs={3}><TextField fullWidth id="firma-web-sitesi" label="Web Sitesi" defaultValue={data.WebSitesi} onChange = {handleChange('WebSitesi')}/></Grid>
                    <Grid item xs={3}><TextField fullWidth id="firma-aktif-olma-tarihi" label="Aktif Olma Tarihi" defaultValue={data.AktifOlmaTarihi} onChange = {handleChange('FirmaAktifOlmaTarihi')}/></Grid>
                    <Grid item xs={3}></Grid>
                    <Grid item xs={3}></Grid>
                </Grid>
            </div>
        );
    }
}

const styles = {
    _sektor: {
        width: '150px'
    },
    _firmaTuru: {
        width: '150px'
    }
};

export default MusteriCreate;
