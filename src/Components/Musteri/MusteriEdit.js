import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views"; 
import { Paper, withStyles, Grid, Button } from "@material-ui/core";
import PropTypes from "prop-types";
import MusteriListItem from "./MusteriListItem";
import { Formik, Form, Field, ErrorMessage } from "formik";
import TextField from "@material-ui/core/TextField";
import { authenticationService, musteriService } from "../../Services";
import { Role } from "../../Helpers";


const styles = theme => ({
    fab: {
        margin: theme.spacing(1),
        top: "auto",
        right: "50px",
        bottom: "50px",
        left: "auto",
        position: "fixed"
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    }
});

 
var self;
class MusteriEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            role: null,
            musteri: {}
        };
        self = this;
    }

    componentDidMount() {
        authenticationService.currentUser.subscribe(x =>
            this.setState({
                currentUser: x,
                role: x.role
            })
        );

        musteriService.getById( this.props.match.params.id).then(m => this.setState({musteri:m}))
    }

    handleChange(e, v) {
        self.setState({ value: v });
        console.log("handleChange");
    }
    handleChangeIndex(i) {
        self.setState({ value: i });
        console.log("handleChangeIndex");
    }
    
    handleTextChange( event) {
        var musteri = self.state.musteri;
        musteri.FirmaAdi = event.target.value;
        self.setState({musteri:musteri});
    }

    render() {
        const { classes, ...props } = this.props;
        return (
            <div>
                <Paper>
                    <Formik
                        initialValues={{ FirmaAdi: "", id: "" }}
                        onSubmit={(values, { setSubmitting }) => {
                            musteriService.update(this.state.musteri).then(r => console.log(r));
                            setSubmitting(false);
                        }}
                    >
                        {({submitForm,  isSubmitting }) => (
                            <div>
                                <Tabs
                                    value={this.state.value}
                                    onChange={this.handleChange}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    variant="fullWidth"
                                >
                                    <Tab label="Musteri Bilgileri" />
                                    <Tab label="Mali Bilgiler" />
                                    <Tab label="Projeler" />
                                    <Tab label="Personeller" />
                                </Tabs>

                                <SwipeableViews
                                    axis="x"
                                    index={this.state.value}
                                    onChangeIndex={this.handleChangeIndex}
                                >
                                    <div>
                                        <Grid container>
                                            <Grid item xs={2}>
                                                <input type="hidden" value={self.state.musteri.id} name="id" />
                                                <TextField
                                                    id="standard-name"
                                                    label="Name"
                                                    className={
                                                        classes.textField
                                                    }
                                                    value={self.state.musteri.FirmaAdi}
                                                    onChange={self.handleTextChange}
                                                    margin="normal"
                                                />
                                            </Grid>
                                            <Grid item xs={2}>
                                                <TextField
                                                    id="standard-name"
                                                    label="Name"
                                                    className={
                                                        classes.textField
                                                    }
                                                    margin="normal"
                                                />
                                            </Grid>
                                            <Grid item xs={2}>
                                                <TextField
                                                    id="standard-name"
                                                    label="Name"
                                                    className={
                                                        classes.textField
                                                    }
                                                    margin="normal"
                                                />
                                            </Grid>
                                            <Grid item xs={2}>
                                                <TextField
                                                    id="standard-name"
                                                    label="Name"
                                                    className={
                                                        classes.textField
                                                    }
                                                    margin="normal"
                                                />
                                            </Grid>
                                            <Grid item xs={2}>
                                                <TextField
                                                    id="standard-name"
                                                    label="Name"
                                                    className={
                                                        classes.textField
                                                    }
                                                    margin="normal"
                                                />
                                            </Grid>
                                            <Grid item xs={2}>
                                                <TextField
                                                    id="standard-name"
                                                    label="Name"
                                                    className={
                                                        classes.textField
                                                    }
                                                    margin="normal"
                                                />
                                            </Grid>
                                        </Grid>
                                        <ErrorMessage
                                            name="email"
                                            component="div"
                                        />
                                    </div>
                                    <div>
                                        <Field
                                            type="password"
                                            name="password"
                                        />
                                        <ErrorMessage
                                            name="password"
                                            component="div"
                                        />
                                    </div>
                                    <div>dddd</div>
                                    <div>Personeller</div>
                                </SwipeableViews>
                                <Form>
                                <Button
                                        variant="contained"
                                        color="primary"
                                        type="submit"
                                        className={classes.button}
                                        onClick={submitForm}
                                    >
                                        Kaydet
                                    </Button>
                                    {this.state.role === Role.Admin && (
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        type=""
                                        className={classes.button}
                                    >
                                        Sil
                                    </Button>)}
                                </Form>
                            </div>
                        )}
                    </Formik>
                </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(MusteriEdit);
