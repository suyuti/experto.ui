import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import Button from "@material-ui/core/Button";
import {  NavLink } from "react-router-dom";

class MusteriDetailPanel extends Component {
    render() {
        const {musteri} = this.props;
        return (
            <div style={styles.root}>
                <Paper style={styles.paper}>
                    <Grid container spacing={2}>
                        <Grid item>
                            <ButtonBase style={styles.image}>
                                <img
                                    style={styles.img}
                                    alt="logo"
                                    src="https://via.placeholder.com/128"
                                />
                            </ButtonBase>
                        </Grid>
                        <Grid item xs={12} sm container>
                            <Grid
                                item
                                xs
                                container
                                direction="column"
                                spacing={2}
                            >
                                <Grid item xs>
                                    <Typography
                                        gutterBottom
                                        variant="subtitle1"
                                    >
                                        {musteri.FirmaAdi}
                                    </Typography>
                                    <Typography variant="body2" gutterBottom>
                                        firma adresi
                                    </Typography>
                                    <Typography variant="body2" gutterBottom>
                                        irtibat bilgi, telefon, kisi
                                        proje, personel sayilari, 
                                        musteri istatistikleri, aksiyonlar (adam ay hesapla, ...)
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Typography variant="subtitle1">
                                    ArGe Merkezi
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid item container>
                        <Grid item xs={4}>
                            <NavLink to={`/musteri/${musteri.id}/adamay`}
                                exact={true}
                                style={{ textDecoration: "none", color: "#584355" }}
                                activeStyle={{ background: "beige", color: "red" }} >
                                <Button variant="contained" color="primary" style={styles.button}>
                                    Adam Ay Hesapla
                                </Button>
                            </NavLink>
                        </Grid>
                        <Grid item xs={4}>
                            <Button variant="contained" color="primary" style={styles.button}>
                                Action 2
                            </Button></Grid>
                        <Grid item xs={4}>
                            <Button variant="contained" color="secondary" style={styles.button}>
                                Action 3
                            </Button>
                        </Grid>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        );
    }
}

const styles = {
    root: {
        flexGrow: 1
    },
    paper: {
        padding: 2,
        margin: "auto",
    },
    image: {
        width: 128,
        height: 128
    },
    img: {
        margin: "auto",
        display: "block",
        maxWidth: "100%",
        maxHeight: "100%"
    },
    button: {
        margin:1,
      },
      input: {
        display: 'none',
      },
};
export default MusteriDetailPanel;
