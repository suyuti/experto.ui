import React, { Component } from "react";
import Avatar from "@material-ui/core/Avatar"; 
import ShareIcon from "@material-ui/icons/Share";
import PhoneIcon from "@material-ui/icons/Phone";
import MailIcon from "@material-ui/icons/Mail";
import {
    withStyles,
    Grid,
    Paper,
    Divider,
    Button,
    Link
} from "@material-ui/core";
import { Role } from "../../Helpers";
import { Link as RouterLink } from "react-router-dom";

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    paper: {
        padding: theme.spacing(2),
        margin: "auto",
        maxWidth: "%100"
    },
    image: {
        width: 128,
        height: 128
    },
    img: {
        margin: "auto",
        display: "block",
        maxWidth: "100%",
        maxHeight: "100%"
    },
    bottomPaper: {
        width: "%100"
    },
    avatar: {
        margin: 10
    },
    bigAvatar: {
        margin: 10,
        width: 120,
        height: 120
    }
});

 
class MusteriListItem extends Component {
    render() {
        const { classes, ...props } = this.props;
        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <Grid container spacing={2}>
                        <Grid item xs={2}>
                            <Avatar
                                alt="Remy Sharp"
                                src="https://via.placeholder.com/150"
                                className={classes.bigAvatar}
                            />
                        </Grid>
                        <Grid item xs={10} container direction="column">
                            <Grid item>
                                <Link
                                    component={RouterLink}
                                    to={"/musteri/" + this.props.data.id}
                                >
                                    <div variant="h5" gutterBottom>
                                        {this.props.data.FirmaAdi}
                                    </div>{" "}
                                </Link>
                            </Grid>
                            <Grid item container>
                                <Grid item xs={2} container>
                                    <PhoneIcon />
                                    (532) 532 00 00
                                </Grid>
                                <Grid item xs={2} container>
                                    <PhoneIcon />
                                    (532) 532 00 00
                                </Grid>
                                <Grid item xs={2} container>
                                    <MailIcon />
                                    a.b@c.com
                                </Grid>
                            </Grid>
                            <Grid item>Firma kisa tanitimi</Grid>
                            <Divider />
                            <Grid item container>
                                <Grid item xs={3} container direction="column">
                                    <Grid item container>
                                        <ShareIcon />
                                        Projects
                                    </Grid>
                                    <div variant="h5" gutterBottom>
                                        11
                                    </div>{" "}
                                </Grid>
                                <Grid item xs={3} container direction="column">
                                    <Grid item container>
                                        <ShareIcon />
                                        Proje
                                    </Grid>
                                    <div variant="h5" gutterBottom>
                                        11
                                    </div>{" "}
                                </Grid>
                                <Grid item xs={3} container direction="column">
                                    <Grid item container>
                                        <ShareIcon />
                                        Personel
                                    </Grid>
                                    <div variant="h5" gutterBottom>
                                        11
                                    </div>{" "}
                                </Grid>
                                <Grid item xs={3}>
                                    <Link
                                        component={RouterLink}
                                        to={"/musteri/" + this.props.data.id + "/proje"}
                                    >
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        className={classes.button}
                                    >
                                        Projeler
                                    </Button>{" "}
                                    </Link>

                                    {this.props.role === Role.Admin && (
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            className={classes.button}
                                        >
                                            Adam Ay
                                        </Button>
                                    )}{" "}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>{" "}
            </div>
        );
    }
}

export default withStyles(styles)(MusteriListItem);
