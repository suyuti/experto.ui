import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';

class SektorCreatePage extends Component {
    render() {
        const { data, handleChange } = this.props;
     
        return ( 
            <div >
                <TextField
                    id="standard-name"
                    label="Permission Aciklama"
                    className=''
                    value={data.Aciklama}
                    onChange={handleChange('Aciklama')}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <TextField
                    id="standard-name"
                    label="Method"
                    className=''
                    value={data.Method}
                    onChange={handleChange('Method')}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <TextField
                    id="standard-name"
                    label="Path"
                    className=''
                    value={data.Path}
                    onChange={handleChange('Path')}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <TextField
                    id="standard-name"
                    label="PermissionKey"
                    className=''
                    value={data.PermissionKey}
                    onChange={handleChange('PermissionKey')}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                /> 
            </div>  
        );
    }
}


export default SektorCreatePage;
