import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';

class BolgeCreatePage extends Component {
    render() {
        const { data, handleChange } = this.props;
     
        return ( 
            <div >
                <TextField
                    id="standard-name"
                    label="Bolge Adi"
                    className=''
                    value={data.BolgeAdi}
                    onChange={handleChange('BolgeAdi')}
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </div>  
        );
    }
}

export default BolgeCreatePage;
