import React, { Component } from "react"; 
import { musteriService, personelService } from "../../Services";
import { Grid, TextField, MenuItem } from '@material-ui/core';

class PersonelProfilCreatePage extends Component {
    constructor(props) {
        super(props); 
        this.state={
            musteriler:[],
            Ready:false,
            getMusteri:{}
        }
        this.musteriId=this.props.data.musteriID;
    }
    
    componentWillMount() {
        musteriService.getAll().then(result => {this.setState({musteriler:result.data,Ready:true});this._findMusteri();})
    }
    _findMusteri = () => {
        let musteri = this.state.musteriler.filter(m => {
            if (m.id == this.musteriId) {
                // alert(JSON.stringify(m));
                return m;
            }
        })
        this.setState({ getMusteri: musteri[0] });
        console.log(this.state.getMusteri)
    } 

    render() {
        const { data, handleChange } = this.props;
     
        return ( 
                <div>

                    {this.musteriId? 
                            <div>
                            <strong style={{fontSize:18}}>Firma:  </strong>
                            {this.state.getMusteri.FirmaAdi}
                        </div>
                         :           
                        <TextField fullWidth id="proje-musteri" select label="Musteri" value={data.musteriID} onChange={handleChange('musteriID')}
                            SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                            {this.state.musteriler.map(m => (
                                <MenuItem key={m.id} value={m.id}> {m.FirmaAdi} </MenuItem>
                            ))}
                        </TextField>
                    } 

                    <TextField
                        id="standard-name"
                        label="Profil Adi"
                        className=''
                        value={data.ProfilAdi}
                        onChange={handleChange('ProfilAdi')}
                        margin="normal"
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                   
                </div> );
    }
}
/*

const styles = {
    inputSoyadi: {
        minWidth: '150px',
        marginLeft: '10px'
    },
    EgitimDurumu: {

        marginLeft: '10px'
    }

};*/

export default PersonelProfilCreatePage;