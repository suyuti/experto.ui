import React, { Component } from "react";
import {convertTimestampToDate} from '../../command' 
//import InputLabel from '@material-ui/core/InputLabel';
//import MenuItem from '@material-ui/core/MenuItem';
//import FormControl from '@material-ui/core/FormControl';
//import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import InputLabel from '@material-ui/core/InputLabel';





//import { egitimDurumuService } from "../../Services";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {

};
class IsPaketiFormu extends Component {



    render() {
        const { data, handleChange, personelProfil, projeList, proje } = this.props;
        //var deneme="2019-09-27"
        // console.error(JSON.stringify(data));
        //alert(JSON.stringify(personelProfil));
        return (
            <div>
                {
                    (proje) ?
                        proje.ProjeAdi :
                        /*
                        <TextField
                            fullWidth 
                            select
                            label="Proje Adi"
                            className=""
                            value={data.ProjeId}
                            onChange={handleChange('ProjeId')}
                            SelectProps={{
                                native: true
                            }} 
                            helperText={data.ProjeId ? "" : "Lütfen Proje seciniz"}
                            margin="normal"
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        >
                        <option value=""></option>
                            {projeList.map(item => (
                                <option key={item.Code} value={item.Code}>
                                    {item.ProjeAdi}
                                </option>
                            ))}
                        </TextField>*/
                        ""
                }<br />
                <TextField
                    label="Adi"
                    className=''
                    value={data.Adi}
                    onChange={handleChange('Adi')}
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <br />
                <TextField
                    label="Aciklama"
                    className=''
                    value={data.Aciklama}
                    onChange={handleChange('Aciklama')}
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <br />
                <TextField
                    type="date"
                    label="BaslangicTarihi"
                    className=''
                    value={convertTimestampToDate(data.BaslangicTarihi)}
                    onChange={handleChange('BaslangicTarihi')}
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <br />
                <TextField
                    type="date"
                    label="BitisTarihi"
                    className=''
                    value={convertTimestampToDate(data.BitisTarihi)}
                    onChange={handleChange('BitisTarihi')}
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <br />


                <FormControl >
                    <InputLabel htmlFor="select-multiple-chip">
                        Profil Listesi
                    </InputLabel>
                    <Select
                        style={{ minWidth: "100px" }}
                        multiple
                        value={data.CalisacakProfilListesi}
                        onChange={handleChange("CalisacakProfilListesi")}
                        input={<Input id="select-multiple-chip" />}
                        renderValue={selected => (
                            <div  >
                                {selected.map(value => <Chip key={value} label={value} />)}
                            </div>
                        )}
                        MenuProps={MenuProps}
                    >
                        {personelProfil.map(item => (
                            <MenuItem
                                key={item.Code}
                                value={item.Code}
                                style={{
                                    color:
                                        data.CalisacakProfilListesi.indexOf(item.Code) === -1 ? "" : "red",
                                }} >
                                <Checkbox checked={
                                    data.CalisacakProfilListesi.indexOf(item.Code) > -1
                                } />
                                <ListItemText primary={item.ProfilAdi} />
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </div>
        );
    }
}

const styles = {
    formControl: {
        minWidth: 120,
        maxWidth: 300,
    }
};
export default IsPaketiFormu;
