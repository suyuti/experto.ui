import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';
import { egitimDurumuService } from "../../Services";
import { Grid, MenuItem } from "@material-ui/core";


class PersonelCreatePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            EgitimDurumuları: [],
            Okullar:[],
            Bolumler:[]
        };
    }


    componentDidMount() {
        //servisten gelen egitim durumları
        egitimDurumuService.getAll().then(result => this.setState({ EgitimDurumuları: result.data }));
        console.log(this.state.EgitimDurumuları);

    }


    render() {
        const { personel, button, handleChange } = this.props;

        return (
            <div style={styles.root}>
            <Grid container spacing={2}>
            <Grid item xs={12}> 
                    <TextField
                        style={ personel.TcNo > 99999999999  ?  {border:" 2px solid red"} : {}  }
                        type="number"
                        fullWidth 
                        id="TcNo"
                        label="TC NO"
                        className=''
                        value={personel.TcNo}
                        onChange={handleChange('TcNo')}
                        margin="normal" 
                    />
                </Grid>

                <Grid item xs={6}>
                    <TextField
                        fullWidth
                        type='text'
                        id="Adi"
                        label="Personel Adi"
                        className=''
                        value={personel.Adi}
                        onChange={handleChange('Adi')}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        fullWidth
                        id="Soyadi"
                        label="Personel Soyadi"
                        value={personel.Soyadi}
                        onChange={handleChange('Soyadi')}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField fullWidth id="personel-EgitimDurumAdi"  select label="Egitim Durumu" value={personel.EgitimDurumu} onChange={handleChange('EgitimDurumu')}
                        SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                        {this.state.EgitimDurumuları.map(ed => (
                            <MenuItem key={ed.id} value={ed.id}> {ed.EgitimDurumAdi} </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item xs={4}>
                    <TextField fullWidth id="personel-okul"  select label="Okul" value={personel.Okul} onChange={handleChange('Okul')}
                        SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                        {this.state.Okullar.map(okul => (
                            <MenuItem key={okul.id} value={okul.id}> {okul.OkulAdi} </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item xs={4}>
                    <TextField fullWidth id="personel-bolum"  select label="Bolum" value={personel.Bolum} onChange={handleChange('Bolum')}
                        SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                        {this.state.Bolumler.map(bolum => (
                            <MenuItem key={bolum.id} value={bolum.id}> {bolum.OkulAdi} </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item xs={12}  style={styles.mt} >
                    {button}
                </Grid>
            </Grid>
            </div>);
    }
}


const styles = {
    root: {
        margin: '10px'
    },
    mt: { 
         
    },
    EgitimDurumu: {

        marginLeft: '10px'
    }

};

export default PersonelCreatePage;