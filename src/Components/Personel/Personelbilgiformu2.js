import React, { Component } from "react";
import { convertTimestampToDate } from '../../command'
import TextField from '@material-ui/core/TextField';

import { gorevService, ekipService, departmanService, musteriService, personelProfilService } from "../../Services";
import MomentUtils from '@date-io/moment';
import { Grid, MenuItem, Switch, FormControlLabel, Input, InputLabel, FormControl, ListItemText, Select } from "@material-ui/core";
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';


const DatePickerField = ({ value, name, ...other }) => {
  //const currentError = form.errors[field.name];
  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <KeyboardDatePicker
        clearable
        //disablePast
        name={name}
        value={value}
        format="DD/MM/YYYY"
        {...other}
        animateYearScrolling />
    </MuiPickersUtilsProvider>
  );
};

class PersonelCreatePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Gorev: [],
      Ekip: [],
      Depart: [],
      Musteri: [],
      Profil: []
    };
  }


  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.personel.MusteriId !== this.props.personel.MusteriId) {
      this.props.personel.EkipId = null;
      this.props.personel.ProfilId = [];

      if (this.props.personel.MusteriId) {
        musteriService.getPersonelProfil(this.props.personel.MusteriId).then(result => this.setState({ Profil: result.data }));
        musteriService.getEkipler(this.props.personel.MusteriId).then(result => this.setState({ Ekip: result.data }));
      }
    }
  }

  fetchData = () => {
    gorevService.getAll().then(result => this.setState({ Gorev: result.data }));
    departmanService.getAll().then(result => this.setState({ Depart: result.data }));

    if (!this.props.personel.MusteriId) {
      musteriService.getAll().then(result => {
        const sortedResult = result.data.sort((a, b) => a.FirmaAdi.localeCompare(b.FirmaAdi));
        this.setState({ Musteri: sortedResult });
      });
      personelProfilService.getAll().then(result => this.setState({ Profil: result.data }));
      ekipService.getAll().then(result => this.setState({ Ekip: result.data }));
    }
    else {
      musteriService.getById(this.props.personel.MusteriId).then(m => this.setState({ Musteri: m.data }));
      musteriService.getPersonelProfil(this.props.personel.MusteriId).then(result => this.setState({ Profil: result.data }));
      musteriService.getEkipler(this.props.personel.MusteriId).then(result => this.setState({ Ekip: result.data }));
    }
  }


  // _findMusteri = () => {
  //   let musteri = this.state.Musteri.filter(m => {
  //     if (m.id == this.props.params) {
  //       // alert(JSON.stringify(m));
  //       return m;
  //     }
  //   })
  //   this.setState({ getMusteri: musteri[0] });
  // }



  render() {
    const { personel, button, handleChange } = this.props;

    return (
      <div style={styles.root}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            {this.props.params
              ?
              (<TextField
                fullWidth
                disabled
                id="personel-firma"
                value={this.props.params ? (this.state.Musteri.length ? this.state.Musteri[0].FirmaAdi : "") : ""} />)
              : (<TextField
                fullWidth
                id="personel-firma"
                select label="Çalıştığı Firma"
                value={personel.MusteriId ? personel.MusteriId : ""}
                onChange={handleChange('MusteriId')}
                SelectProps={{ MenuProps: { className: '', }, }} margin="normal" >
                {this.state.Musteri.map(m => (<MenuItem key={m.id} value={m.id}> {m.FirmaAdi} </MenuItem>
                ))}
              </TextField>)
            }
          </Grid>
          <Grid item xs={6}>
            <TextField fullWidth id="personel-departman" select label="Departman" value={personel.DepartmanId} onChange={handleChange('DepartmanId')}
              SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
              {this.state.Depart.map(m => (
                <MenuItem key={m.id} value={m.id}> {m.DepartmanAdi} </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={6}>
            <TextField fullWidth id="personel-ekip" select label="Ekip" value={personel.EkipId ? personel.EkipId : null} onChange={handleChange('EkipId')} margin="normal">
              {!personel.MusteriId
                ? <MenuItem disabled>Firma Seçiniz</MenuItem>
                : (this.state.Ekip.length > 0
                  ? this.state.Ekip.map(e => (<MenuItem key={e.id} value={e.id}> {e.EkipAdi} </MenuItem>))
                  : <MenuItem disabled>Kayıtlı Ekip Yok</MenuItem>)
              }
            </TextField>
          </Grid>
          <Grid item xs={6}>
            <TextField fullWidth id="personel-gorev" select label="Görev" value={personel.GorevId} onChange={handleChange('GorevId')}
              SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
              {this.state.Gorev.map(g => (
                <MenuItem key={g.id} value={g.id}> {g.GorevAdi} </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={6}>
            {
              this.props.params
                ? (<FormControl style={{ minWidth: "100%", marginTop: 16 }}>
                  <InputLabel id="personel-profil-label">Personel Profil</InputLabel>
                  <Select
                    multiple
                    value={personel.ProfilId}
                    displayEmpty={true}
                    onChange={handleChange('ProfilId')}
                    input={<Input id="select-multiple-placeholder" />}
                    renderValue={selected => {
                      if (selected.length === 0) return ""
                      else return <span> {selected.length} profil seçildi</span>
                    }}>
                    {this.state.Profil.map(item => (
                      <MenuItem
                        key={item.id}
                        value={item.id}
                      // style={getStyles(item, this)}
                      >
                        <ListItemText primary={item.ProfilAdi} />
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>)
                : (<FormControl style={{ minWidth: "100%", marginTop: 16 }}>
                  <InputLabel id="personel-profil-label">Personel Profil</InputLabel>
                  <Select
                    multiple
                    displayEmpty={true}
                    value={personel.MusteriId ? personel.ProfilId : []}
                    onChange={handleChange('ProfilId')}
                    input={<Input id="select-multiple-placeholder" />}
                    renderValue={selected => {
                      if (selected.length === 0) return ""
                      else return <span> {selected.length} profil seçildi</span>
                    }} >
                    {!personel.MusteriId
                      ? <MenuItem disabled>Firma Seçiniz</MenuItem>
                      : (this.state.Profil && this.state.Profil.length
                        ? this.state.Profil.map(item => (
                          <MenuItem key={item.id} value={item.id}>
                            <ListItemText primary={item.ProfilAdi} />
                          </MenuItem>))
                        : <MenuItem disabled>Kayıtlı Profil Yok</MenuItem>)
                    }
                  </Select>
                </FormControl>)
            }
          </Grid>

          <Grid item xs={3}>
            <DatePickerField name="IseBaslamaTarihi"
              label="İşe Başlama Tarihi"
              value={personel.IseBaslamaTarihi}
              onChange={handleChange('IseBaslamaTarihi')}
            ></DatePickerField>
          </Grid>
          {personel.Durumu == 0 && <Grid item xs={3}>

            <DatePickerField name="IstenAyrilmaTarihi"
              label="İşten Ayrılma Tarihi"
              value={personel.IstenAyrilmaTarihi}
              onChange={handleChange('IstenAyrilmaTarihi')}
            ></DatePickerField>
          </Grid>}
          <Grid item xs={3}>
            <FormControlLabel
              control={
                <Switch
                  checked={!personel.Durumu || personel.Durumu == 1 ? true : false}
                  onChange={handleChange('Durumu')}
                  value={personel.Durumu == 1 ? 0 : 1}
                  color="primary"
                />
              }
              label="Çalışıyor mu?"
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}


const styles = {
  root: {
    margin: '10px'
  },
  inputSoyadi: {
    marginLeft: '10px'
  },
  left: {
    marginLeft: '10px',
    minWidth: '150px'
  }

};

export default PersonelCreatePage;