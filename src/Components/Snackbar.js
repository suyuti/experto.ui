import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import { SnackbarAction } from '../actions/snackbarAction';


class SnackbarPopUp extends Component {

    handleClick = () => {
        //setOpen(true);
    }
    handleClose = (event, reason) => {
        if (reason === 'clickaway') {//ilk tıklanmayı ipTAL ETMEK İÇİN KULLANILIR
            return;
        }
        this.props.SnackbarAction('close', "");
    }


    render() {
        const { snackBarReducer } = this.props;

        return (
            <div>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    open={snackBarReducer.status}
                    autoHideDuration={2000}
                    onClose={this.handleClose}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{snackBarReducer.message}</span>}
                    action={[

                        <IconButton
                            key="close"
                            aria-label="close"
                            color="inherit"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                    ]}
                />
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        snackBarReducer: state.snackBarReducer
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        SnackbarAction: SnackbarAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SnackbarPopUp);
