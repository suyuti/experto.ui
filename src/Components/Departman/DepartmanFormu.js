import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';

class DepartmanCreatePage extends Component {
    render() {
        const { data, handleChange } = this.props;
        return ( 
                <div >
                    <TextField
                        id="standard-name"
                        label="Departman Adı"
                        className=''
                        value={data.DepartmanAdi}
                        onChange={handleChange('DepartmanAdi')}
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />

                    <TextField
                        id="standard-name"
                        label="Musteri Id"
                        className=''
                        value={data.MusteriId}
                        onChange={handleChange('MusteriId')}
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </div>  
            );
    }
}

export default DepartmanCreatePage;
