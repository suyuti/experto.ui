import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views"; 
import { Paper, withStyles, Grid, Button, LinearProgress } from "@material-ui/core";
import PropTypes, { string } from "prop-types";
import { Formik, Form, Field } from "formik";
import { egitimDurumuService } from "../../Services";
import EgitimDurumuFormu from '../../Components/EgitimDurumu/EgitimDurumuFormu' 
import Modal from '@material-ui/core/Modal';
import {
    fieldToTextField,
    TextField,
    Switch,
} from 'formik-material-ui';



 
var self;
class PersonelCreate extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            value: 0,
            EgitimDurumu: {
                id: '',
                EgitimDurumAdi: ''
            }
        };
        console.log("ParamsId", this.props.ParamsId)

        if (props.ParamsId) {
            egitimDurumuService.getById(props.ParamsId).then(data => {
                console.log(data)

                this.setState({ Personeller: data })
            }
            );
        }


    }

    handleTabChange(e, v) {
        self.setState({ value: v });
        console.log("handleTabChange");
    }
    handleSwipeViewChange(i) {
        self.setState({ value: i });
        console.log("handleChangeIndex");
    }

    handleChange = (input) => e => {
        console.log(e.target.value, input);
        let inputValue = e.target.value;
        this.setState(() => ({
            EgitimDurumu: {
                ...this.state.EgitimDurumu,
                [input]: inputValue
            }
        }));
    };
    createButton = (e) => {
        console.log(e);
        let tempIseBaslamaTarihi = this.state.personel.IseBaslamaTarihi;
        var IseBaslamaTarihi = new Date(this.state.personel.IseBaslamaTarihi).getTime();
        console.log(tempIseBaslamaTarihi + "  ---->  " + IseBaslamaTarihi);

        let tempIstenAyrilmaTarihi = this.state.personel.IstenAyrilmaTarihi;
        var IstenAyrilmaTarihi = new Date(this.state.personel.IstenAyrilmaTarihi).getTime();
        console.log(tempIstenAyrilmaTarihi + "  ---->  " + IstenAyrilmaTarihi);



        //timestimp cevirme
        this.state.personel.IseBaslamaTarihi = IseBaslamaTarihi;

        if (!this.state.personel.Durumu) {
            this.state.personel.IstenAyrilmaTarihi = IstenAyrilmaTarihi;
        }
        this.state.personel.CreatedAt = Date.now();

        //tokenda backend den cekip kim oluşturduysa onun bilgisi alınacak şimdilik localstorage den alınıyor
        this.state.personel.CreatedBy = localStorage.getItem('currentUser').id;




        console.log(this.state.personel)
        // e.preventDefault();
        // egitimDurumuService.create(this.state.personel)
    }
    

    updateButton = (e) => {
        console.log('update button');

        //tokenda backend den cekip kim oluşturduysa onun bilgisi alınacak şimdilik localstorage den alınıyor
        this.state.personel.UpdatedAt = Date.now();
        //tokenda backend den cekip kim oluşturduysa onun bilgisi alınacak şimdilik localstorage den alınıyor
        this.state.personel.UpdatedBy = localStorage.getItem('currentUser').id;
    }




    render() {
        const { classes, ...props } = this.props;
        let button;
        if (!this.props.ParamsId) {
            button = <Button
                variant="contained"
                color="default"
                onClick={this.updateButton}
                style={styles.save_button}
            >
                UPDATE
                </Button>
        } else {
            button = <Button
                variant="contained"
                color="primary"
                onClick={this.createButton}
                style={styles.save_button}
            >
                KAYDET
                </Button>
        }


        return (
            <div>
                <Paper>
                    <Formik
                        initialValues={{ FirmaAdi: '' }}
                        render={({
                            submitForm,
                            values,
                            status,
                            errors,
                            isSubmitting
                        }) => (
                                <Form>
                                    <Tabs
                                        value={this.state.value}
                                        onChange={this.handleTabChange}
                                        indicatorColor="primary"
                                        textColor="primary"
                                        variant="fullWidth"
                                    >
                                        <Tab label='Egitim Durumu Bilgileri' /> 
                                    </Tabs>
                                    <SwipeableViews
                                        axis="x"
                                        index={this.state.value}
                                        onChangeIndex={this.handleSwipeViewChange}>
                                        <div> 
                                            {this.state.personel}
                                            <EgitimDurumuFormu
                                                handleChange={this.handleChange}
                                                data={this.state.EgitimDurumu} />
                                        </div> 
                                    </SwipeableViews>
                                    <br />

                                    {button}

                                </Form>
                            )}
                    >

                    </Formik>
                </Paper>



            </div>
        );
    }
}

const styles = {
    save_button: {
        bottom: '20px',
        left: '20px'
    }
};

export default PersonelCreate;
