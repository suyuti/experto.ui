import React, { Component } from "react";
//import InputLabel from '@material-ui/core/InputLabel';
//import MenuItem from '@material-ui/core/MenuItem';
//import FormControl from '@material-ui/core/FormControl';
//import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
//import { egitimDurumuService } from "../../Services";



class GorevCreatePage extends Component {
   
    

    render() {
        const { data, handleChange } = this.props;
     
        return ( 
                <div >
                    <TextField
                        id="standard-name"
                        label="Gorev Adi"
                        className=''
                        value={data.GorevAdi}
                        onChange={handleChange('GorevAdi')}
                        margin="normal"
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                   
                </div>  
            );
    }
}

/*
const styles = {
    inputSoyadi: {
        minWidth: '150px',
        marginLeft: '10px'
    },
    EgitimDurumu: {

        marginLeft: '10px'
    }

};*/

export default GorevCreatePage;
