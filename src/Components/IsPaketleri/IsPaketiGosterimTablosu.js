import React, { Component } from 'react'

class IsPaketiTablosu extends Component {

    constructor(props) {
        super(props)

        this.state = {
            data: this.props.isPaketleri,
            endDate: null,
            startDate: null,
            proje: this.props.proje
        }

        /*
      Aciklama: "issdsdfsdfsdfsdfdf"
Adi: "ismailgfhfh"
BaslangicTarihi: "2019-02-28T21:00:00.000Z"
BitisTarihi: "2019-07-31T21:00:00.000Z"
CalisacakProfilListesi: null
ProfilId: 7
ProjeId: 10
id: 1636
*/  




    }
    componentDidMount() {
        /*
                let enKucukTarih  = new Date(this.state.data[0].BaslangicTarihi); 
                let enBuyukTarih  =new Date(this.state.data[0].BitisTarihi); 
                
                for (let index = 0; index < this.state.data.length; index++) {
                    const BaslangicTarihi = new Date(this.state.data[index].BaslangicTarihi) ;
                    const BitisTarihi =new Date(this.state.data[index].BitisTarihi) ; 
        
                    if(+enKucukTarih > +BaslangicTarihi){ 
                        enKucukTarih=  BaslangicTarihi;
                    }
                    if(+enBuyukTarih < +BitisTarihi){
                        enBuyukTarih=  BitisTarihi;
                    } 
                }
                console.log(enBuyukTarih,enKucukTarih)
        */
        this.setState({
            endDate: new Date(this.state.proje.ProjeBitisTarihi),
            startDate: new Date(this.state.proje.ProjeBaslangicTarihi)
        });


    }
    componentWillReceiveProps({isPaketleri}) { 
        this.setState({data:isPaketleri})
    }

    _renderResultRowsHeaderYears() {
        var headerYears = [];//yıllar için başlık thsi
        var Years = this.state.endDate.getFullYear() - this.state.startDate.getFullYear() + 1 //ne kadar yıl var
        var colspan = null //yıl için birleştirmem gereken hücre sayısı
        for (let index = 0; index < Years; index++) {
            if (index == 0) { colspan = 12 - this.state.startDate.getMonth() } // yıl başta ise proje tarihinden 12 inci aya kadar birleştirir
            else if (index == Years - 1) { colspan = this.state.endDate.getMonth() } //sonda ise birinci aydan proje bitiş tarihine kadar birleşrtirir
            else {
                colspan = 12 //ortalarda ise 12 ayı birleştirir
            }
            headerYears.push(<th colSpan={colspan}>{this.state.startDate.getFullYear() + index}</th>);
        }
        return (<tr> {headerYears} </tr>)
    }

    _renderResultRowsHeaderMounth() {
        var headerMounth = [];
        var headerYears = [];
        var mounth = (this.state.endDate.getFullYear() - this.state.startDate.getFullYear()) * 12 + ((this.state.endDate.getMonth() + 1) - (this.state.startDate.getMonth() + 1))
        for (let index = 0; index < mounth; index++) {
            headerMounth.push(<th>{((this.state.startDate.getMonth() + index) % 12) + 1}</th>);
        }
        return (<tr> {headerMounth} </tr>)
    }

    _getProfilId(Ay, Yil) {
        var result = " ";
        var renk=" ";
        for (let index = 0; index < this.state.data.length; index++) {

            let item = this.state.data[index] // iş paketi
            var itemBaslangicYil = new Date(item.BaslangicTarihi).getFullYear() // iş paketi baş. yıl
            var itemBaslangicAy = new Date(item.BaslangicTarihi).getMonth() + 1 // iş paketi baş. ay
            var itemBitisYil = new Date(item.BitisTarihi).getFullYear() // iş paketi bitç
            var itemBitisAy = new Date(item.BitisTarihi).getMonth() + 1 // iş paketi bit ay

            var control = (itemBitisYil - itemBaslangicYil) * 12 + (itemBitisAy - itemBaslangicAy);

            var controlB = (Yil - itemBaslangicYil) * 12 + (Ay - itemBaslangicAy);

            if (controlB >= 0 && control >= controlB) {
                result = item.Adi;
                renk = "ip" + item.IsPaketiColor
            }
        }
        return [result,  renk];
    }

    _createIspaketi() {
        let countindex = 0;
        let contentTD = [];
        let contentTR = [];
        var mounthCount = (this.state.endDate.getFullYear() - this.state.startDate.getFullYear()) * 12 + ((this.state.endDate.getMonth() + 1) - (this.state.startDate.getMonth() + 1))
        for (let index1 = 0; index1 < mounthCount; index1++) {


            var TempConvertAy = ((this.state.startDate.getMonth() + index1) % 12) + 1;
            var TempConvertYil = this.state.startDate.getFullYear() + Math.floor(((this.state.startDate.getMonth() + index1 ) / 12));
            //if(TempConvertAy==0){TempConvertAy=1}
            let TempConvert = {
                Yil: TempConvertYil,
                Ay: TempConvertAy
            }

            let [IsPaketiAdi, color] = this._getProfilId(TempConvert.Ay, TempConvert.Yil);
            
            contentTD.push(<td key={countindex++} className={"bgStictTableGrey"} >
                <div className={color}>
                     
                    {IsPaketiAdi.toString()}
                </div>
            </td>);
        }

        contentTR.push(
            <tr key={countindex++}  >
                {contentTD}
            </tr>);

        return contentTR;
    }


    render() {

        if (this.state.endDate == null || this.state.startDate == null) {
            return (<div> </div>)
        }

        return (
            <div>
                <table id="customers">
                    {this._renderResultRowsHeaderYears()}
                    {this._renderResultRowsHeaderMounth()}
                    {/*this.renderResultRows()*/}

                    {this._createIspaketi()}

                </table>

            </div>
        )
    }
}

export default IsPaketiTablosu;