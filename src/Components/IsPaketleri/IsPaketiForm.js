import React, { Component } from 'react' 
import { TextField, Grid, MenuItem, Switch, FormControlLabel ,Input ,InputLabel,FormControl , ListItemText} from "@material-ui/core";
import { Select, Checkbox, Chip} from "@material-ui/core";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    DatePicker
} from '@material-ui/pickers';
import trLocale from "date-fns/locale/tr";
import DateFnsUtils from '@date-io/date-fns';

class IsPaketiForm extends Component {
    render() {
        const { data, handleChange } = this.props;
        return (
            <div>
                <MuiPickersUtilsProvider locale={trLocale} utils={DateFnsUtils}>
                    <Grid container direction="column">
                        <TextField
                            disabled
                            id="standard-name"
                            label="Proje Adi"
                            className=''
                            value={data.ProjeAdi}
                            onChange={handleChange('ProjeAdi')}
                            margin="normal"
                            InputLabelProps={{
                                readOnly: true,
                            }}
                        />
                        <TextField
                            id="standard-name"
                            label="Is Paketi Adi"
                            className=''
                            value={data.IsPaketiAdi}
                            onChange={handleChange('IsPaketiAdi')}
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="standard-name"
                            label="Aciklama"
                            className=''
                            value={data.Aciklama}
                            onChange={handleChange('Aciklama')}
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />

                        {/* <TextField fullWidth id="profil" select label="Çalışacak Profil" value={data.ProfilId} onChange={handleChange('ProfilId')}
                            SelectProps={{ MenuProps: { className: '', }, }} margin="normal">
                            {this.props.profiller.map(p => (
                                <MenuItem key={p.id} value={p.id}> {p.ProfilAdi} </MenuItem>
                            ))}
                        </TextField> */}


                          <FormControl style={{minWidth: "100%",marginTop: 16 }}>
                        <InputLabel id="personel-profil-label">Çalışacak Profil</InputLabel>
                            <Select
                                multiple 
                                value={data.ProfilId}
                                onChange={handleChange('ProfilId')}
                                input={<Input id="select-multiple-placeholder" />}   
                                > 

                                <MenuItem disabled value="">
                                    <em>Placeholder</em>
                                </MenuItem>
                                {this.props.profiller.map(item => ( 
                                <MenuItem
                                    key={item.id}
                                    value={item.id}
                                    // style={getStyles(item, this)}
                                >
                                    {item.ProfilAdi}
                                </MenuItem>
                                ))}
                            </Select>

                        </FormControl>   

                        <Grid item spacing={3}>
                            <DatePicker
                                openTo="year"
                                views={["year", "month"]}
                                variant="dialog"
                                format="MM/yyyy"
                                label="Baslangic Tarihi"
                                value={data.BaslangicTarihi}
                                onChange={handleChange('BaslangicTarihi')}
                            />
                            <DatePicker
                                minDate={data.BaslangicTarihi}
                                openTo="year"
                                views={["year", "month"]}
                                variant="dialog"
                                format="MM/yyyy"
                                label="Bitis Tarihi"
                                value={data.BitisTarihi}
                                onChange={handleChange('BitisTarihi')}
                            />
                        </Grid>
                    </Grid>
                </MuiPickersUtilsProvider>
            </div>
        )
    }
}

export default IsPaketiForm;