export function SnackbarAction(type, payload){
    return {
        type: type,
        payload: payload
    };
}