import React, { Component } from "react";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DashboardIcon from "@material-ui/icons/Dashboard";
import PersonIcon from '@material-ui/icons/Person';
import DomainIcon from '@material-ui/icons/Domain';
import HomeIcon from '@material-ui/icons/Home';
import CastForEducationIcon from '@material-ui/icons/CastForEducation';
import BarChartIcon from '@material-ui/icons/BarChart';
import ContactsIcon from '@material-ui/icons/Contacts';
//import SettingsIcon from "@material-ui/icons/Settings";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { NavLink } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import { authenticationService } from "../Services";
import { withRouter } from 'react-router-dom'
import { checkPermission ,PermissionData} from '../command';
import CircularProgress from '@material-ui/core/CircularProgress';

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Divider from "@material-ui/core/Divider";
//import {checkPermission,currentUserPermission} from '../command';
//import { PermissionList } from "../config";


import ListSubheader from '@material-ui/core/ListSubheader';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';


const drawerWidth = 200;
const styles = theme => ({
    drawerPaper: {
        position: "fixed",
        top: theme.spacing(8),
        whiteSpace: "nowrap",
        width: drawerWidth,
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    drawerPaperClose: {
        overflowX: "hidden",
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        width: theme.spacing(8),
        [theme.breakpoints.up("sm")]: {
            width: theme.spacing(9)
        },
        selected: {
            backgroundColor: "red"
        }
    },
    navIcon: {
        paddingLeft: 8,
    } ,
    navIcon2: {
        paddingLeft: 20,
        marginRight: 8
    }
});



class SideBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: null,
            isAdmin: false,
            currentUserPermissions: [],
            TanimlarAcik:false,
            MusteriIslemleriAcik:false
        };
        this.handleClick = this.handleClick.bind(this)
    }

    componentDidMount() {
        this.setState({
            currentUser: JSON.parse(localStorage.getItem("currentUser")),
            currentUserPermissions: JSON.parse(localStorage.getItem("currentUser")).Permissions
        });
    }

    logout() {
        authenticationService.logout();
        this.props.history.push("/login");
    }

    handleClick(islem) {
        if (islem === 'FirmaMenu') {
            this.setState({MusteriIslemleriAcik: !this.state.MusteriIslemleriAcik, TanimlarAcik: false})
        }
        else if (islem === 'TanimMenu') {
            this.setState({MusteriIslemleriAcik: false, TanimlarAcik: !this.state.TanimlarAcik})
        }
    }

    render() {
        const { open, classes } = this.props;
        const { currentUser, isAdmin, currentUserPermissions } = this.state;
        const activeButton={background:"rgb(185, 185, 185)"}

        if (!currentUserPermissions) {
            return (
                <div style={{ textAlign: "center" }}>
                    <CircularProgress />
                </div>
            )
        }

        return (
            <Drawer
                variant="permanent"
                classes={{
                    paper: classNames(
                        classes.drawerPaper,
                        !open && classes.drawerPaperClose
                    )
                }}
                open={open}
            >
                <List component="nav">
                    <NavLink to="/"
                        exact={true}
                        style={{ textDecoration: "none", color: "#584355" }}
                        activeStyle={{ background: '#cccccc69', color: "red" }}>
                        <ListItem button >
                            <ListItemIcon className={classes.navIcon}>
                                <HomeIcon />
                            </ListItemIcon>
                            <ListItemText primary="Home" />
                        </ListItem>
                    </NavLink>


                    <ListItem button onClick={() => this.handleClick('FirmaMenu')} style={this.state.MusteriIslemleriAcik ? activeButton:{}}>
                        <ListItemIcon className={classes.navIcon}>
                            <DomainIcon />
                        </ListItemIcon>
                        <ListItemText primary="Firma" />
                        {this.state.MusteriIslemleriAcik ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={this.state.MusteriIslemleriAcik} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding style={{background:"#f1f0f0"}}>
                            {checkPermission([PermissionData.getMusteri]) ?
                            <NavLink to="/musteri"
                                exact={true}
                                style={{ textDecoration: "none", color: "#584355" }}
                                activeStyle={{ background: "beige", color: "red" }}>
                                <ListItem button>
                                    <ListItemIcon className={classes.navIcon2}>
                                        <DomainIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Musteri" />
                                </ListItem>
                            </NavLink>
                            : ""}
                            {checkPermission([PermissionData.getPersonel]) ?
                                <NavLink to="/personel"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <PersonIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Personel" />
                                    </ListItem>
                                </NavLink>
                            : ""}
                            {checkPermission([PermissionData.getProje]) ?
                                <NavLink to="/proje"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <PersonIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Proje" />
                                    </ListItem>
                                </NavLink>
                            : ""}
                        </List>
                    </Collapse>

                    <ListItem button onClick={() => this.handleClick('TanimMenu')}  style={this.state.TanimlarAcik ? activeButton:{}}>
                        <ListItemIcon className={classes.navIcon}>
                            <DomainIcon />
                        </ListItemIcon>
                        <ListItemText primary="Tanimlar" />
                        {this.state.TanimlarAcik ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={this.state.TanimlarAcik} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding style={{background:"#f1f0f0"}}>
                            {checkPermission([PermissionData.getEgitimDurumu]) ?
                            <NavLink to="/egitimDurumu"
                                exact={true}
                                style={{ textDecoration: "none", color: "#584355" }}
                                activeStyle={{ background: "beige", color: "red" }}>
                                <ListItem button>
                                    <ListItemIcon className={classes.navIcon2}>
                                        <CastForEducationIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Egitim Durumu" />
                                </ListItem>
                            </NavLink>
                            : ""}
                            {checkPermission([PermissionData.getGorev]) ?
                                <NavLink to="/gorev"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <BarChartIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Görevler" />
                                    </ListItem>
                                </NavLink>
                                : ""}
                            {checkPermission([PermissionData.getPersonelProfil]) ?
                                <NavLink to="/personelProfil"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <ContactsIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="P. Profilleri" />
                                    </ListItem>
                                </NavLink>
                                : ""}
                            {checkPermission([PermissionData.getProjeTurleri]) ?
                                <NavLink to="/projeTurleri"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <BarChartIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Proje Turleri" />
                                    </ListItem>
                                </NavLink>
                                : ""}
                            {checkPermission([PermissionData.getProjeAlanlari]) ?
                                <NavLink to="/projeAlanlari"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <BarChartIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Proje Alanlari" />
                                    </ListItem>
                                </NavLink>
                                : ""}
                            {checkPermission([PermissionData.getDepartman]) ?
                                <NavLink to="/departman"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <BarChartIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Departman" />
                                    </ListItem>
                                </NavLink>
                                : ""}
                            {checkPermission([PermissionData.getEkip]) ?
                                <NavLink to="/ekip"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <BarChartIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Ekip" />
                                    </ListItem>
                                </NavLink>
                                : ""}
                            {checkPermission([PermissionData.getBolge]) ?
                                <NavLink to="/bolge"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <BarChartIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Bolge" />
                                    </ListItem>
                                </NavLink>
                                : ""}
                            {checkPermission([PermissionData.getSektor]) ?
                                <NavLink to="/sektor"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <BarChartIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Sektor" />
                                    </ListItem>
                                </NavLink>
                                : ""}
                                
                        </List>
                    </Collapse>

                    {checkPermission([PermissionData.getUsers]) ?
                                <NavLink to="/users"
                                    exact={true}
                                    style={{ textDecoration: "none", color: "#584355" }}
                                    activeStyle={{ background: "beige", color: "red" }}>
                                    <ListItem button>
                                        <ListItemIcon className={classes.navIcon2}>
                                            <BarChartIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Kullanıcılar" />
                                    </ListItem>
                                </NavLink>
                                : ""}
                    {checkPermission([]) ?
                    <NavLink to="/role"
                        exact={true}
                        style={{ textDecoration: "none", color: "#584355" }}
                        activeStyle={{ background: "beige", color: "red" }}>
                        <ListItem button>
                            <ListItemIcon className={classes.navIcon2}>
                                <BarChartIcon />
                            </ListItemIcon>
                            <ListItemText primary="Roller" />
                        </ListItem>
                    </NavLink>
                    : ""}

                    {checkPermission([]) ?
                    <NavLink to="/pdks"
                        exact={true}
                        style={{ textDecoration: "none", color: "#584355" }}
                        activeStyle={{ background: "beige", color: "red" }}>
                        <ListItem button>
                            <ListItemIcon className={classes.navIcon2}>
                                <BarChartIcon />
                            </ListItemIcon>
                            <ListItemText primary="Pdks" />
                        </ListItem>
                    </NavLink>
                    : ""}

                    <ListItem button onClick={event => this.logout()}>
                        <ListItemIcon className={classes.navIcon}>
                            <ExitToAppIcon />
                        </ListItemIcon>
                        <ListItemText primary="Logout" />
                    </ListItem>
                </List>
            </Drawer>
        );
    }
}
export default withRouter(withStyles(styles)(SideBar));
