import React, {Component, Fragment} from 'react'
import { withStyles } from "@material-ui/core/styles";
import Header from './Header'
import SideBar from './SideBar'
import classNames from "classnames";

const drawerWidth = 200;

const styles = theme => ({
    root: {
      display: "flex"
    },
    content: {
      flexGrow: 1,
      marginLeft: theme.spacing(9),
      padding: theme.spacing(3),
      marginTop: theme.spacing(7),
      overflowX: "hidden"
    },
    contentShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    }
  });

class MainLayout extends Component {
    state = {
        open:true
    }
    handleToggleDrawer =() => {

        this.setState(prevState => {
            return {open : !prevState.open};
        });
    }

    render() {
        const { classes, children } = this.props;
        return(
            <Fragment>
            <div className={classes.root}>
              <Header
                handleToggleDrawer={this.handleToggleDrawer}
              />
              <main
                className={classNames(classes.content, {
                  [classes.contentShift]: this.state.open
                })}
              >
                {children}
              </main>
            </div>
            <SideBar open={this.state.open} drawerWidth={drawerWidth} />
          </Fragment>
        )
    }
}
export default withStyles(styles)(MainLayout);