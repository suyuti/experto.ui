import React, { Component } from 'react'
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import MenuIcon from "@material-ui/icons/Menu";
import {Notifications, Message} from "@material-ui/icons";
import PersonIcon from "@material-ui/icons/Person";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from '@material-ui/core/Tooltip';
import { authenticationService } from '../Services';

require('dotenv').config()

const styles = theme => ({
  toolbarRoot: {
    paddingRight: 24,
    backgroundColor: "#fe0000"
  },
  menuButton: {
    marginLeft: 13,
    marginRight: 36
  },
  title: {
    flexGrow: 1
  }
});
class Header extends Component {
  state = {
    currentUser: null
  }

  UNSAFE_componentWillMount() {
    this.setState({ currentUser: authenticationService.currentUserValue })
  }

  render() {
    const { classes, handleToggleDrawer } = this.props;
    return (<AppBar position="fixed">
      <Toolbar disableGutters={true} classes={{ root: classes.toolbarRoot }}>
        <IconButton
          color="inherit"
          aria-label="Open drawer"
          onClick={handleToggleDrawer}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
        <div
          variant="title"
          color="inherit"
          className={classes.title}
        >
          EXPERTO YONETIM PANELI [{process.env.NODE_ENV} - {process.env.REACT_APP_API_URL}]
          </div>
        <IconButton color="inherit">
          <Badge  color="secondary">
            <Message />
          </Badge>
        </IconButton>
        <IconButton color="inherit">
          <Badge  color="secondary">
            <Notifications />
          </Badge>
        </IconButton>
        <Tooltip title={this.state.currentUser && this.state.currentUser.FirstName + ' ' + this.state.currentUser.LastName + ' (' + this.state.currentUser.UserName + ')'}>
          <IconButton color="inherit">
            <PersonIcon />
          </IconButton>
        </Tooltip>
      </Toolbar>
    </AppBar>)
  }
}
export default withStyles(styles)(Header);