import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import Snackbar from '@material-ui/core/Snackbar';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Reducers from './reducers/index';
import SimpleSnackbar from './Components/Snackbar';
require('dotenv').config({ path: `./.env.${process.env.REACT_APP_NODE_ENV}` })
const store = createStore(Reducers, applyMiddleware(ReduxThunk));


//import { configureFakeBackend } from './Fake/fake-backend';
//configureFakeBackend();
 
ReactDOM.render(
    <Provider store={store}>
        <App />
        <SimpleSnackbar/>
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
